using MagazineLoader.Application;
using MagazineLoader.Updater;
using MaterialTracking;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Platform.HMI;
using PressModule.Updater;
using RobotToTrayUnloader.Updater;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_ICT
{
    public class Startup: BaseStartup
    {
        private MagazineLoader.Application.MagazineLoaderModule mMagazineLoaderModule;
        private PressModule.Application.PressModule mPressModule;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloaderModule;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
            :base(configuration, env, typeof(Program))
        {
            mLog.Debug("> Startup");

            CreateConnections();
            InitializeApplications();

            mLog.Debug("Startup >");
        }
        public new void ConfigureServices(IServiceCollection services)
        {
            mLog.Debug("> ConfigureServices");
            try
            {
                base.ConfigureServices(services);
            }
            catch(Exception e)
            {
                mLog.Fatal($"Failed to startup: {e}");
            }

            //Modules
            services.AddSingleton(MagazineLoaderModule.Instance);
            services.AddSingleton(PressModule.Application.PressModule.Instance);
            services.AddSingleton(RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance);

            //Updaters
            services.AddSingleton<MagazineLoaderUpdater>();
            services.AddSingleton<PressModuleUpdater>();
            services.AddSingleton<RobotToTrayUnloaderUpdater>();
            services.AddSingleton<MaterialTrackingUpdater>();

            //Managers
            services.AddSingleton(ConfigManager.Application.ConfigManager.Instance);

            mLog.Debug("ConfigureServices >");
        }

        private new void CreateConnections()
        {
            mLog.Debug($"> CreateApplications");

            base.CreateConnections();

            //First register the recipe, as the ip-adress for the PLC could be changed in machine recipe
            mRecipeManager.RegisterProductRecipeStructure<ProductRecipe>();
            mRecipeManager.RegisterMachineRecipeStructure<MachineRecipe>();
        }

        private new void InitializeApplications()
        {
            base.InitializeApplications();

            mMagazineLoaderModule = MagazineLoader.Application.MagazineLoaderModule.Instance;
            mPressModule = PressModule.Application.PressModule.Instance;
            mRobotToTrayUnloaderModule = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;

            mLog.Debug($"CreateApplications >");
        }
    }
}
