﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Authorization.Application;
using Platform.HMI;

namespace Power_X1_ICT.Pages
{
    public class IndexModel : PageModel
    {
        //This model is the first model that is loaded when you connect to the server.
        //Here we start the necessary managers/updaters that are not page specific
        public IndexModel(AuthorizationManager authorizationManager, EventManagerUpdater eventManagerUpdater)
        {

        }
    }
}
