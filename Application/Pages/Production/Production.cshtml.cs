using Localization;
using log4net;
using Logging;
using LotControl;
using MagazineLoader.Updater;
using MaterialTracking;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using Platform.HMI.Model;
using Platform.HMI.Pages;
using Platform.HMI.Pages.Production;
using Platform.Lib;
using PressModule.Updater;
using RecipeManager;
using RobotToTrayUnloader.Updater;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Path = System.IO.Path;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class ProductionModel : PageModel
    {
        private readonly MaterialTrackingUpdater mMaterialTrackingUpdater;
        private readonly LotControl.Application.LotControl mLotControl;
        private readonly ILog mLog = LogClient.Get();
        private readonly MaterialTrackingApplication mMaterialTracking;
        private readonly MagazineLoader.Application.MagazineLoaderModule mMagazineLoader;
        private readonly PressModule.Application.PressModule mPressModule;
        private readonly RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader;
        private readonly IRecipeManager mRecipeManager;
        private readonly IStringLocalizer<SharedResource> mSharedLocalizer;

        public ProductionModel(MaterialTrackingUpdater materialTrackingUpdater, LotControlUpdater lotControlUpdater,
            IStringLocalizer<SharedResource> sharedLocalizer,
            LotControl.Application.LotControl lotControl, MaterialTrackingApplication materialTrackingApplication,
            MagazineLoaderUpdater loaderUpdater, PressModuleUpdater pressUpdater, RobotToTrayUnloaderUpdater robotToTrayUnloaderUpdater,
            MagazineLoader.Application.MagazineLoaderModule magazineLoaderModule, PressModule.Application.PressModule pressModule,
            RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader,
            RecipeManager.RecipeManager recipeManager)
        {
            mMaterialTrackingUpdater = materialTrackingUpdater;
            mLotControl = lotControl;
            mMaterialTracking = materialTrackingApplication;
            mRecipeManager = recipeManager;
            mSharedLocalizer = sharedLocalizer;

            mMagazineLoader = magazineLoaderModule;
            mPressModule = pressModule;
            mRobotToTrayUnloader = robotToTrayUnloader;
        }

        public JsonResult OnPostGetMaterialTrackingData()
        {
            return mMaterialTrackingUpdater.GetAllMaterialTrackingData();
        }

        public void OnPostAddLot(string input1, string input2)
        {
            mLotControl.AddPlannedLot(input1, Convert.ToInt32(input2));
        }

        public void OnPostRemoveLot(string input)
        {
            mLotControl.RemovePlannedLot(input);
        }

        public void OnPostCloseCurrentLot()
        {
            mLotControl.CloseLot();
        }

        public void OnPostForceCloseCurrentLot()
        {
            mLotControl.ForceCloseLot();
        }

        public JsonResult OnPostGetLots()
        {
            return new JsonResult(new { lots = SetupHelper.GetLots(mMaterialTracking, mLotControl, mSharedLocalizer) });
        }

        public JsonResult OnPostGetSelectedLot(string input1, string input2)
        {
            return new JsonResult(new { selectedLotModel = SetupHelper.GetSelectedLot(input1, input2, mMaterialTracking, mLotControl) });
        }

        public JsonResult OnPostGetSelectedMaterial(string input)
        {
            return new JsonResult(new { selectedMaterialModel = SetupHelper.GetSelectedMaterial(input, mMaterialTracking, mLotControl.CurrentLotId, mSharedLocalizer) });
        }

        public JsonResult OnPostGetInspection3dResult()
        {
            var Inspection3dResult = new InspectionModel();
            return new JsonResult(new { Inspection3dResult = Inspection3dResult });
        }

        public JsonResult OnPostGetStates()
        {
            var states = new List<string>
            {
                ((State)mMagazineLoader.State).ToString(),
                ((State)mPressModule.State).ToString(),
                ((State)mRobotToTrayUnloader.State).ToString()
            };

            return new JsonResult(new { states });
        }

        public JsonResult OnPostGetDrives()
        {
            var drives = new List<string>();

            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo drive in allDrives)
            {
                //drive name is C:/ and when a volumelabel is available we add it: (volumelabel)
                var driveName = drive.VolumeLabel == string.Empty ? drive.Name : drive.Name + " " + "(" + drive.VolumeLabel + ")";
                drives.Add(driveName);
            }

            return new JsonResult(new { drives });
        }

        public JsonResult OnPostGetRecipeNames()
        {
            return new JsonResult(new { recipeNames = mRecipeManager.GetProductRecipeNames() });
        }

        public JsonResult OnPostGetLogFiles()
        {
            var sourceDir = @"C:\WebService\bin\logs\";
            var allFiles = Directory.GetFiles(sourceDir, "*.xml", SearchOption.TopDirectoryOnly);

            return new JsonResult(new { logs = allFiles.Select(x => Path.GetFileName(x)) });
        }

        public void OnPostSaveLotDataItems(SaveLogDataModel input)
        {
            try
            {
                if (input.Location == string.Empty)
                    return;

                var driveName = input.Location.Split(" ")[0];
                var destinationDir = Path.Combine(driveName, DateTime.Now.ToString("yyyy-MM-dd_HHmm"));

                if (input.Logs.All)
                {
                    SaveAllLogs(destinationDir);
                }

                if (input.Lots.All)
                {
                    SaveAllLots(destinationDir);
                }

                if (input.Recipes.All)
                {
                    SaveAllRecipes(destinationDir);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Failed to save lot data for lot: " + e);
            }
        }

        private static void SaveAllLogs(string destinationDir)
        {
            var sourceDir = @"C:\WebService\bin\logs\";
            var logPath = Path.Combine(destinationDir, @"logs\");
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            var allFiles = Directory.GetFiles(sourceDir, "*.xml", SearchOption.TopDirectoryOnly);

            foreach (string sourceFilename in allFiles)
            {
                var destFilename = sourceFilename.Replace(sourceDir, logPath);
                System.IO.File.Copy(sourceFilename, destFilename, true);
            }
        }

        private static void SaveAllLots(string destinationDir)
        {

            const string folderName = "LotReports";
            CopyAllFilesFromBinFolderToDestination(destinationDir, folderName);
        }

        private static void SaveAllRecipes(string destinationDir)
        {
            const string folderName = "Recipe";
            CopyAllFilesFromBinFolderToDestination(destinationDir, folderName);
        }

        private static void CopyAllFilesFromBinFolderToDestination(string destinationDir, string folderName)
        {
            var sourceDir = Path.Combine(Directory.GetCurrentDirectory(), "bin", folderName);
            var path = Path.Combine(destinationDir, folderName);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var allFiles = Directory.GetFiles(sourceDir, "*.xml", SearchOption.TopDirectoryOnly);

            foreach (string sourceFilename in allFiles)
            {
                var destFilename = sourceFilename.Replace(sourceDir, path);
                System.IO.File.Copy(sourceFilename, destFilename, true);
            }
        }
    }
}
