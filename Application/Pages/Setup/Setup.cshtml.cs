using LineControl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Diagnostics;
using System;
using Platform.Lib;
using Authorization.Lib;
using Microsoft.AspNetCore.Identity;
using Platform.HMI.Model;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class SetupModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> mSignInManager;

        private readonly LineControl.Application.LineControl mLineControl;
        private readonly LotControl.Application.LotControl mLotControl;

        public DryRunSettingsModel DryRunSettingsModel { get; set; } = new();

        public bool NoAoi { get; set; }

        public bool SimulationEnabled { get; set; }

        public SetupModel(LineControlUpdater lineControlUpdater, SignInManager<ApplicationUser> signInManager,
            LineControl.Application.LineControl lineControl, LotControl.Application.LotControl lotControl)
        {
            mLotControl = lotControl;
            mLineControl = lineControl;
            mSignInManager = signInManager;
        }

        public void OnPostChangeDryRunEnabled(DryRunSettingsModel model)
        {
            model.Save();
        }

        public void OnPostClearMaterial()
        {
            mLineControl.ClearAllMaterial();
        }

        public void OnPostReInitPLC()
        {
            mLineControl.ReInitPLC();
        }

        public void OnPostLockCovers()
        {
            mLineControl.Lock();
        }

        public void OnPostUnlockCovers()
        {
            mLineControl.Unlock();
        }

        public JsonResult OnPostAreCoversLocked()
        {
            return new JsonResult(mLineControl.CoversLocked);
        }

        public JsonResult OnPostGetDryRunEnabled()
        {
            return new JsonResult(mLineControl.DryRunEnabled);

        }

        public JsonResult OnPostGetStateStoppedOrError()
        {
            return new JsonResult(mLineControl.State == (int)State.Stopped || mLineControl.State == (int)State.Error);
        }

        public void OnPostRestartWindows()
        {
            StartShutDown("-f -r -t 5");
        }

        public void OnPostStopChrome()
        {
            Process[] chromeInstances = Process.GetProcessesByName("chrome");

            foreach (Process p in chromeInstances)
                p.Kill();
        }

        public JsonResult OnPostGetLotControlSettings()
        {
            return new JsonResult(new { Mode = mLotControl.Mode.ToString(), MaxLeadFrames = mLotControl.MaxLeadframes });
        }

        public void OnPostSaveLotControlValues(string[] input)
        {
            mLotControl.SetLotControlMode(input[0], Convert.ToInt32(input[1]));
        }

        private static void StartShutDown(string param)
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = "cmd";
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Arguments = "/C shutdown " + param;
            Process.Start(proc);
        }
    }
}
