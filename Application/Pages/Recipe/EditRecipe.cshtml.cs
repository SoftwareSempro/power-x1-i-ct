using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using RecipeManager;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Pages
{
    public class EditRecipeModel : PageModel
    {
        private IRecipeManager mRecipeManager;
        public string RecipeName { get; set; } = string.Empty;

        public EditRecipeDataModel Recipe { get; set; } = new();

        public EditRecipeModel(IRecipeManager recipeManager)
        {
            mRecipeManager = recipeManager;
        }

        public void OnGet(string recipe)
        {
            if (recipe != null)
            {
                RecipeName = recipe;
                Recipe = new EditRecipeDataModel(mRecipeManager.GetRecipe(RecipeName) as ProductRecipe);
            }
        }

        public LocalRedirectResult OnPostSaveRecipe(EditRecipeDataModel input)
        {
            ProductRecipe recipe = input.ConvertToProductRecipe();
            var author = User?.Identity?.Name ?? "N/A";
            mRecipeManager.SaveRecipe(recipe, author);
            return LocalRedirect("/Recipe/Recipe");
        }

        public LocalRedirectResult OnPostCloseRecipe()
        {
            return LocalRedirect("/Recipe/Recipe");
        }       
    }
}
