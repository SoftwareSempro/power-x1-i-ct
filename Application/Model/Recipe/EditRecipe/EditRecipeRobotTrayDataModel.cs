﻿using System;
using System.Collections.Generic;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    public class EditRecipeRobotTrayDataModel
    {
        public string Count { get; set; }
        public string Tray1 { get; set; } = "Disabled";
        public string Tray2 { get; set; } = "Disabled";      
        public string Tray3 { get; set; } = "Disabled";
        public string Tray4 { get; set; } = "Disabled";
        public string Tray5 { get; set; } = "Disabled";
        public string Tray6 { get; set; } = "Disabled";

        public string DefaultVelocityPercentage { get; set; }

        public EditRecipeRobotTrayDataModel(Robot robot)
        {         
            Count = robot.BaseCount.ToString("F0");

            if (robot.Locations.Count >= 4)
            {
                Tray1 = robot.Locations[0].ToString();
                Tray2 = robot.Locations[1].ToString();
                Tray3 = robot.Locations[2].ToString();
                Tray4 = robot.Locations[3].ToString();
            }

            if (robot.Locations.Count >= 5)
            {
                Tray5 = robot.Locations[4].ToString();
            }

            if (robot.Locations.Count >= 6)
            {
                Tray6 = robot.Locations[5].ToString();
            }
            DefaultVelocityPercentage = robot.DefaultVelocityPercentage.ToString();
        }

        public EditRecipeRobotTrayDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Robot ConvertToRobotRecipe(Robot robot)
        {
            var output = new Robot();
            output.BaseCount = Convert.ToInt32(Count);
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray1));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray2));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray3));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray4));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray5));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray6));
            output.DefaultVelocityPercentage = Convert.ToInt32(DefaultVelocityPercentage);

            output.Tray1TrayOffsetX = robot.Tray1TrayOffsetX;
            output.Tray1TrayOffsetY = robot.Tray1TrayOffsetY;
            output.Tray1TrayOffsetRZ = robot.Tray1TrayOffsetRZ;

            output.Tray2TrayOffsetX = robot.Tray2TrayOffsetX;
            output.Tray2TrayOffsetY = robot.Tray2TrayOffsetY;
            output.Tray2TrayOffsetRZ = robot.Tray2TrayOffsetRZ;

            output.Tray3TrayOffsetX = robot.Tray3TrayOffsetX;
            output.Tray3TrayOffsetY = robot.Tray3TrayOffsetY;
            output.Tray3TrayOffsetRZ = robot.Tray3TrayOffsetRZ;

            output.Tray4TrayOffsetX = robot.Tray4TrayOffsetX;
            output.Tray4TrayOffsetY = robot.Tray4TrayOffsetY;
            output.Tray4TrayOffsetRZ = robot.Tray4TrayOffsetRZ;

            output.Tray5TrayOffsetX = robot.Tray5TrayOffsetX;
            output.Tray5TrayOffsetY = robot.Tray5TrayOffsetY;
            output.Tray5TrayOffsetRZ = robot.Tray5TrayOffsetRZ;

            output.Tray6TrayOffsetX = robot.Tray6TrayOffsetX;
            output.Tray6TrayOffsetY = robot.Tray6TrayOffsetY;
            output.Tray6TrayOffsetRZ = robot.Tray6TrayOffsetRZ;

            return output;
        }
    }
}
