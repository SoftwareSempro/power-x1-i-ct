﻿using Microsoft.AspNetCore.Mvc;
using System;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class EditRecipeTrimDataModel
    {
        public string TopPosition { get; set; }
        public string SensorPosition { get; set; }
        public string ClosePosition { get; set; }
        public string Velocity { get; set; }
        public string RemovePosition { get; set; }
        public string MaintenancePosition { get; set; }       

        public EditRecipeTrimDataModel(Trim trim)
        {
            TopPosition = trim.TopPosition.ToString("F2");
            SensorPosition = trim.SensorPosition.ToString("F2");
            ClosePosition = trim.ClosePosition.ToString("F2");
            Velocity = trim.Velocity.ToString("F0");
            RemovePosition = trim.RemovePosition.ToString("F2");
            MaintenancePosition = trim.MaintenancePosition.ToString("F2");
        }

        public EditRecipeTrimDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Trim ConvertToTrimRecipe()
        {
            var output = new Trim();

            output.TopPosition = Convert.ToDouble(TopPosition);
            output.SensorPosition = Convert.ToDouble(SensorPosition);
            output.ClosePosition = Convert.ToDouble(ClosePosition);
            output.Velocity = Convert.ToInt32(Velocity);
            output.RemovePosition = Convert.ToDouble(RemovePosition);
            output.MaintenancePosition = Convert.ToDouble(MaintenancePosition);

            return output;
        }
    }
}