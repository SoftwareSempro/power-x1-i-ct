﻿using System;
using System.Collections.Generic;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    public class EditRecipeRobotPlaceDataModel
    {
        private Place mPlaceRecipe;

        public EditRecipeRobotPlaceDataModel(Place place)
        {
            mPlaceRecipe = place;
        }

        public EditRecipeRobotPlaceDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Place ConvertToRobotRecipe()
        {
            var output = mPlaceRecipe;

            return output;
        }
    }
}
