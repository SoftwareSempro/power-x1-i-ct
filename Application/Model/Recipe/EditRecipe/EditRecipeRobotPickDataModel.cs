﻿using System;
using System.Collections.Generic;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    public class EditRecipeRobotPickDataModel
    {
        private Pick mPickRecipe;

        public EditRecipeRobotPickDataModel(Pick pick)
        {
            mPickRecipe = pick;
        }

        public EditRecipeRobotPickDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Pick ConvertToRobotRecipe()
        {
            var output = mPickRecipe;

            return output;
        }
    }
}
