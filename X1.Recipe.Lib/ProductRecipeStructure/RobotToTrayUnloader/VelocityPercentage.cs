﻿namespace X1.Recipe.Lib.Product
{
   public class VelocityPercentage
    {
        public int ApproachVelocityPercentage { get; set; }
        public int ClearVelocityPercentage { get; set; }
    }
}
