﻿namespace X1.Recipe.Lib.Product
{
    public class Place
    {      
        public double PlaceOffsetZ { get; set; }
        public VelocityPercentage VelocityPercentage { get; set; } = new();
    }
}
