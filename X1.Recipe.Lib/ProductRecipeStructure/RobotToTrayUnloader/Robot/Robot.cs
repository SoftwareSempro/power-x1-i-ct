﻿namespace X1.Recipe.Lib.Product

{
    public class Robot
    {
        public int BaseCount { get; set; }
        public List<OutfeedLocationType> Locations { get; set; } = new();
        public int DefaultVelocityPercentage { get; set; }

        public double Tray1TrayOffsetX { get; set; }
        public double Tray1TrayOffsetY { get; set; }
        public double Tray1TrayOffsetRZ { get; set; }

        public double Tray2TrayOffsetX { get; set; }
        public double Tray2TrayOffsetY { get; set; }
        public double Tray2TrayOffsetRZ { get; set; }

        public double Tray3TrayOffsetX { get; set; }
        public double Tray3TrayOffsetY { get; set; }
        public double Tray3TrayOffsetRZ { get; set; }

        public double Tray4TrayOffsetX { get; set; }
        public double Tray4TrayOffsetY { get; set; }
        public double Tray4TrayOffsetRZ { get; set; }

        public double Tray5TrayOffsetX { get; set; }
        public double Tray5TrayOffsetY { get; set; }
        public double Tray5TrayOffsetRZ { get; set; }

        public double Tray6TrayOffsetX { get; set; }
        public double Tray6TrayOffsetY { get; set; }
        public double Tray6TrayOffsetRZ { get; set; }
    }
}
