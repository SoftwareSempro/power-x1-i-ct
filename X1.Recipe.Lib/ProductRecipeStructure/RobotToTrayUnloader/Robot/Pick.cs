﻿namespace X1.Recipe.Lib.Product
{
   public class Pick
    {
        public Position Position { get; set; } = new();
        public double PickOffsetZ { get; set; }
        public VelocityPercentage VelocityPercentage { get; set; } = new();
    }
}
