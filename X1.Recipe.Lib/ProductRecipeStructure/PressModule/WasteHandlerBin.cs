﻿namespace X1.Recipe.Lib
{ 
   public class WasteHandlerBin
    {
        public int AlmostFullCount { get; set; }
        public int MaximumWasteCount { get; set; }
    }
}
