﻿namespace X1.Recipe.Lib.Recipe.Machine
{
    public class ProvideMaterial
    {
        public double MagazineLiftStartPositionY { get; set; }
        public double MagazineLiftStartPositionZ { get; set; }
        public int VelocityY { get; set; }
        public int VelocityZ { get; set; }
    }
}
