﻿using MachineManager.Lib.Recipe.Machine;
using Recipe.Lib;
using System.Xml.Serialization;

namespace X1.Recipe.Lib.Machine
{
    public class MachineRecipe: IMachineRecipe
    {
        public DateTime DateOfRevision { get; set; }

        [XmlIgnore]
        public int Revision { get; set; }

        //public MagazineLoader MagazineLoader { get; set; } = new();
        public PressModule PressModule { get; set; } = new();
        public RobotTrayUnloader RobotTrayUnloader {get; set; } = new();
        public LotControl LotControl { get; set; } = new();
        public PLCSettings PLCSettings { get; set; } = new();
        public FTPSettings FTPSettings { get; set; } = new();
        public MagazineLoader.Lib.Recipe.Machine.MagazineLoader MagazineLoader { get; set; } = new();
        public SignalTower.Lib.Recipe.Machine.SignalTowerSettings SignalTowerSettings { get; set; } = new();
        public Platform.Lib.Recipe.Machine.DryRunSettings DryRunSettings { get; set; } = new();
    }
}
