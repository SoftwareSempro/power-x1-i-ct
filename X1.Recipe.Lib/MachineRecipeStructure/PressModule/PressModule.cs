﻿namespace X1.Recipe.Lib.Machine
{
    public class PressModule
    {
        public Trimming Trimming { get; set; } = new();
        public Forming forming { get; set; } = new();
    }
}
