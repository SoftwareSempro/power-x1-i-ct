﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.Main;
using PressModule.PLCInterfaces.Infeed;
using PressModule.PLCInterfaces.Trimming;
using PressModule.PLCInterfaces.Forming;
using PressModule.PLCInterfaces.IndexHandler;
using PressModule.PLCInterfaces.Dambar;
using PressModule.PLCInterfaces.WasteHandler;
using PressModule.PLCInterfaces.MaterialTracking;
using PressModule.PLCInterfaces.Trim;
using PressModule.PLCInterfaces.Form;
using PressModule.PLCInterfaces.Indexer;
using PressModule.PLCInterfaces.VacuumCleaner;
using PressModule.PLCInterfaces.Resources.Dambar;
using PressModule.PLCInterfaces.Resources.WasteBin;

namespace PressModule.PLCInterfaces
{
    public class PressModule
    {
        [Name("pressModuleMainProcessInterface")]
        public pressModuleMainProcessInterface pressModuleMainProcessInterface { get; set; }

        [Name("pressModuleInfeedInterface")]
        public PressModuleInfeedInterface PressModuleInfeedInterface { get; set; }

        [Name("pressModuleTrimmingInterface")]
        public PressModuleTrimmingInterface PressModuleTrimmingInterface { get; set; }

        [Name("pressModuleFormingInterface")]
        public PressModuleFormingInterface PressModuleFormingInterface { get; set; }

        [Name("pressModuleIndexHandlerInterface")]
        public PressModuleIndexHandlerInterface PressModuleIndexHandlerInterface { get; set; }

        [Name("pressModuleDambarCheckInterface")]
        public PressModuleDambarInterface PressModuleDambarInterface { get; set; }

        [Name("pressModuleWasteHandlerInterface")]
        public PressModuleWasteHandlerInterface PressModuleWasteHandlerInterface { get; set; }

        [Name("pressModuleTrimInterface")]
        public PressModuleTrimInterface PressModuleTrimInterface { get; set; }

        [Name("pressModuleFormInterface")]
        public PressModuleFormInterface PressModuleFormInterface { get; set; }

        [Name("pressModuleIndexerInterface")]
        public PressModuleIndexerInterface PressModuleIndexerInterface { get; set; }

        [Name("pressModuleDambarInterface")]
        public PressModuleDambarCheckInterface PressModuleDambarCheckInterface { get; set; }
        [Name("pressModuleWasteBinInterface")]
        public PressModuleWasteBinInterface PressModuleWasteBinInterface { get; set; }

        [Name("pressModuleVacuumCleanerInterface")]
        public PressModuleVacuumCleanerInterface PressModuleVacuumCleanerInterface { get; set; }

        public PressModuleMaterialTracking PressModuleMaterialTracking { get; set; }
    }
}
