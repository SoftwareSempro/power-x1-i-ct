﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Resources.Dambar
{
    public class ToPlc
    {
        [Name("dambarCheckCommand")]
        public bool DambarCheckCommand { get; set; }
    }
}
