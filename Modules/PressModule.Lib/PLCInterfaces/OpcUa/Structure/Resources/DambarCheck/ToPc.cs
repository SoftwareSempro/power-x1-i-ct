﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Resources.Dambar
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("dambarCmdResult")]
        public bool DambarCheckCommandResult { get; set; }
    }
}
