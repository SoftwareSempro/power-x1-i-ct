﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Resources.Dambar
{
   public class PressModuleDambarCheckInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
