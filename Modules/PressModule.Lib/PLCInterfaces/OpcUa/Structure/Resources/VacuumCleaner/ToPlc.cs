﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.VacuumCleaner
{
    public class ToPlc
    {
        [Name("vacuumCleanerOnCommand")]
        public bool VacuumCleanerOnCommand { get; set; }

        [Name("vacuumCleanerOffCommand")]
        public bool VacuumCleanerOffCommand { get; set; }
    }
}