﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.VacuumCleaner
{
   public class PressModuleVacuumCleanerInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
