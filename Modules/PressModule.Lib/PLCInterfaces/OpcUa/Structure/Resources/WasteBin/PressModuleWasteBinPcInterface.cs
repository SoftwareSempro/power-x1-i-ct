﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Resources.WasteBin
{
   public class PressModuleWasteBinInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
