﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Resources.WasteBin
{
    public class ToPc
    {        
        [Name("wasteBinPresent")]
        public bool WasteBinPresent { get; set; }
    }
}
