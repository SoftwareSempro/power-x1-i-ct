﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Form
{
    public class ToPlc
    {
        [Name("goToCommand")]
        public bool GoToCommand { get; set; }

        [Name("setPointZ")]
        public double SetPointZ { get; set; }

        [Name("velocity")]
        public short Velocity { get; set; }
    }
}
