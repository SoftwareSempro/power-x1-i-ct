﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Form
{
   public class PressModuleFormInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
