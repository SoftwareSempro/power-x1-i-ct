﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trim
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("positionZ")]
        public double PositionZ { get; set; }

        [Name("toolLocked")]
        public bool ToolLocked { get; set; }
        
    }
}
