﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trim
{
   public class PressModuleTrimInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
