﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
   public class PressModuleIndexerInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
