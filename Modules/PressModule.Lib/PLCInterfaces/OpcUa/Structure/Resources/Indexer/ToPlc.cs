﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
    public class ToPlc
    {
        [Name("infeedCommand")]
        public bool InfeedCommand { get; set; }

        [Name("outfeedCommand")]
        public bool OutfeedCommand { get; set; }

        [Name("pinUpCommand")]
        public bool PinUpCommand { get; set; }

        [Name("pinDownCommand")]
        public bool PinDownCommand { get; set; }

        [Name("vacuumOnCommand")]
        public bool VacuumOnCommand { get; set; }

        [Name("vacuumOffCommand")]
        public bool VacuumOffCommand { get; set; }

        [Name("vacuumChannelSettings")]
        public ChannelSettings VacuumChannelSettings { get; set; }

        [Name("activateBlowOffCommand")]
        public bool ActivateBlowOffCommand { get; set; }

        [Name("deactivateBlowOffCommand")]
        public bool DeactivateBlowOffCommand { get; set; }

        [Name("blowOffChannelSettings")]
        public ChannelSettings BlowOffChannelSettings { get; set; }

    }
}
