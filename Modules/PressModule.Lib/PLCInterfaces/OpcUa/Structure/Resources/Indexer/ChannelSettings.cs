﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
    public class ChannelSettings
    {
        [Name("[0]")]
        public bool Channel0 { get; set; }
        [Name("[1]")]
        public bool Channel1 { get; set; }
        [Name("[2]")]
        public bool Channel2 { get; set; }
        [Name("[3]")]
        public bool Channel3 { get; set; }
        [Name("[4]")]
        public bool Channel4 { get; set; }
        [Name("[5]")]
        public bool Channel5 { get; set; }
    }
}