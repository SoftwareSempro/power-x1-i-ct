﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
        [Name("isOnInfeed")]
        public bool IsOnInfeed { get; set; }
        [Name("isOnOutfeed")]
        public bool IsOnOutfeed { get; set; }
        [Name("isUp")]
        public bool IsUp { get; set; }
        [Name("isDown")]
        public bool IsDown { get; set; }
        [Name("isVacuum1On")]
        public bool IsVacuum1On { get; set; }
        [Name("isVacuum2On")]
        public bool IsVacuum2On { get; set; }
        [Name("isVacuum3On")]
        public bool IsVacuum3On { get; set; }
        [Name("isVacuum4On")]
        public bool IsVacuum4On { get; set; }
        [Name("isVacuum5On")]
        public bool IsVacuum5On { get; set; }
        [Name("isVacuum6On")]
        public bool IsVacuum6On { get; set; }
    }
}
