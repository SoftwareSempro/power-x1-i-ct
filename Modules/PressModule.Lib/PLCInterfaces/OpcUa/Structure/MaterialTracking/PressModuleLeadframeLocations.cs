﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace PressModule.PLCInterfaces.MaterialTracking
{
    public class Press2ModuleLeadframeLocations
    {
        [Name("[0]")]
        public LeadframeLocation LeadframeLocation0 { get; set; }
        [Name("[1]")]
        public LeadframeLocation LeadframeLocation1 { get; set; }
    }
}