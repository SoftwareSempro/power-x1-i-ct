﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace PressModule.PLCInterfaces.MaterialTracking
{
    public class Press2ModuleIndexerLeadframeLocations
    {
        [Name("[0]")]
        public LeadframeLocation LeadframeLocation0 { get; set; }
    }
}