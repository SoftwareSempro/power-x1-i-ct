﻿
using MachineManager.Lib;

namespace PressModule.PLCInterfaces.MaterialTracking
{
    public class PressModuleMaterialTracking
    {
        [Name("press2ModuleLeadframeLocations")]
        public Press2ModuleLeadframeLocations Press2ModuleLeadframeLocations { get; set; }

        [Name("press2ModuleIndexerLeadframeLocations")]
        public Press2ModuleIndexerLeadframeLocations Press2ModuleIndexerLeadframeLocations { get; set; }

        [Name("press2ModuleDeviceLocations")]
        public Press2ModuleDeviceLocations Press2ModuleDeviceLocations { get; set; }

        [Name("press2ModuleIndexerDeviceLocations")]
        public Press2ModuleIndexerDeviceLocations Press2ModuleIndexerDeviceLocations { get; set; }

        [Name("press2ModuleIndexerWasteLocationHasMaterial")]
        public bool Press2ModuleIndexerWasteLocationHasMaterial { get; set; }

    }
}
