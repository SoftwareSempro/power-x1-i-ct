using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Infeed
{
  public class ToPlc
  {
        [Name("RetryCommand")]
        public bool RetryCommand { get; set; }
        [Name("RemoveMaterialCommand")]
        public bool RemoveMaterialCommand { get; set; }
  }
}
