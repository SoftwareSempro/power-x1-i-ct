﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Infeed
{ 
   public class PressModuleInfeedInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
