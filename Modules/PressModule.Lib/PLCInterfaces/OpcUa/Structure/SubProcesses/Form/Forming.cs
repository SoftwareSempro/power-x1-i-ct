﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
   public class Forming
    {
        [Name("topPosition")]
        public float TopPosition { get; set; }
        [Name("sensorPosition")]
        public float SensorPosition { get; set; }
        [Name("closePosition")]
        public float ClosePosition { get; set; }
        [Name("safePositionZ")]
        public double SafePositionZ { get; set; }
        [Name("removePosition")]
        public float RemovePosition { get; set; }
        [Name("maintenancePosition")]
        public float MaintenacePosition { get; set; }
        [Name("velocity")]
        public short Velocity { get; set; }
    }
}
