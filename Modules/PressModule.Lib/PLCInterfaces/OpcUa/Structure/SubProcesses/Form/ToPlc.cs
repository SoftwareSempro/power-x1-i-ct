﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
   public class ToPlc
    {
        [Name("settings")]
        public Forming Forming { get; set; }
    }
}
