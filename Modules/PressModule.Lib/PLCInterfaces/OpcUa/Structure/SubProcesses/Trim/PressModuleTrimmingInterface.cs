﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trimming
{
    public class PressModuleTrimmingInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }

    }
}
