﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trimming
{
   public class ToPlc
    {
        [Name("settings")]
        public Trimming Trimming { get; set; }
    }
}
