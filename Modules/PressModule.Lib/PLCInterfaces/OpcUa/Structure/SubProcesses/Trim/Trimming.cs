﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trimming
{
   public class Trimming
    {
        [Name("topPosition")]
        public float TopPosition { get; set; }
        [Name("sensorPosition")]
        public float SensorPosition { get; set; }
        [Name("closePosition")]
        public float ClosePosition { get; set; }
        [Name("safePositionZ")]
        public float SafePositionZ { get; set; }
        [Name("removePosition")]
        public float RemovePosition { get; set; }
        [Name("maintenancePosition")]
        public float MaintenacePosition { get; set; }
        [Name("velocity")]
        public short Velocity { get; set; }
        
    }
}
