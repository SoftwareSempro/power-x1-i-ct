﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Dambar
{ 
   public class PressModuleDambarInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
