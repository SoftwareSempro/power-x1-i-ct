﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Dambar
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
