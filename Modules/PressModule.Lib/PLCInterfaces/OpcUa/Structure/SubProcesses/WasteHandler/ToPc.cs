﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.WasteHandler
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("count")]
        public int Count { get; set; }
    }
}
