using Assistants.Lib;
using MachineManager.Lib;

namespace PressModule.PLCInterfaces.IndexHandler
{
    public class ToPlc
    {
        [Name("defaultAssistCommands")]
        public DefaultAssistCommands DefaultAssistCommands { get; set; }
    }
}
