﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.IndexHandler
{ 
   public class PressModuleIndexHandlerInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }


        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
