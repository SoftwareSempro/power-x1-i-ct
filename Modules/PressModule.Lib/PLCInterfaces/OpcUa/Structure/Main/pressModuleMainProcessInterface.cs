﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Main
{
   public class pressModuleMainProcessInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
