﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Main
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
        [Name("state")]
        public int State { get; set; }
        [Name("isRunningProduction")]
        public bool IsRunningProduction { get; set; }
        [Name("hasError")]
        public bool HasError { get; set; }
        [Name("inSimulation")]
        public bool InSimulation { get; set; }
    }
}
