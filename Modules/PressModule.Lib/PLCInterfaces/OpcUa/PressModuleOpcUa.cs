﻿using MachineManager.Lib;
using MachineManager.OpcUa;
using System;
using System.Linq.Expressions;

namespace PressModule.PLCInterfaces.OpcUa
{
   public class PressModuleOpcUa
    {
        public static string GetPath<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return OpcUaPathCreator<ServerInterface>.GetPath(expression);
        }

        public static TagModel GetTagModel<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return new TagModel(ServerInterface.ModuleId, GetPath(expression));
        }
    }
}
