﻿using log4net;
using Logging;
using LotControl.Application;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application
{
    public class MaterialTracking: SubscriptionBase
    {
        public event EventHandler<int> InfeedHasMaterialChanged;
        public event EventHandler<int> TrimmingHasMaterialChanged;
        public event EventHandler<int> WasteBinHasMaterialChanged;
        public event EventHandler<int> DambarHasMaterialChanged;
        public event EventHandler<int> FormingHasMaterialChanged;

        public event EventHandler<int> IndexerPosition0HasMaterialChanged;
        public event EventHandler<int> IndexerPosition1HasMaterialChanged;
        public event EventHandler<int> IndexerPosition2HasMaterialChanged;
        public event EventHandler<int> IndexerPosition3HasMaterialChanged;
        public event EventHandler<int> IndexerPosition4HasMaterialChanged;
        public event EventHandler<bool> IndexerPosition1HasWasteChanged;

        MaterialTrackingApplication mMaterialTracking;
        LotControl.Application.LotControl mLotControl;
        private ILog mLog = LogClient.Get();

        private bool mInfeedHasMaterial;
        private bool mTrimmingHasMaterial;
        private bool mDambarHasMaterial;
        private bool mWasteBinHasMaterial;
        private bool mFormingHasMaterial;

        private bool mIndexerPosition0HasMaterial;
        private bool mIndexerPosition1HasMaterial;
        private bool mIndexerPosition2HasMaterial;
        private bool mIndexerPosition3HasMaterial;
        private bool mIndexerPosition4HasMaterial;
        private bool mIndexerPosition1HasWaste;

        private int mTrimmingProcessState;
        private int mFormingProcessState;
        private int mInspectedDambarProcessState;

        private int mInspectionDambarResult;

        private readonly TagModel mInfeedHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation0.HasMaterial);
        private readonly TagModel mTrimmingHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation1.HasMaterial);
        private readonly TagModel mWasteBinHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation0.HasMaterial);
        private readonly TagModel mDambarHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mFormingHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation2.HasMaterial);

        private readonly TagModel mIndexPosition0HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerLeadframeLocations.LeadframeLocation0.HasMaterial);
        private readonly TagModel mIndexPosition1HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation0.HasMaterial);
        private readonly TagModel mIndexPosition1HasWaste = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerWasteLocationHasMaterial);
        private readonly TagModel mIndexPosition2HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mIndexPosition3HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mIndexPosition4HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation3.HasMaterial);

        private readonly TagModel mTrimmingProcessStateTagModel = PressModuleOpcUa.GetTagModel((x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation1.Leadframe.Devices[0].ProcessedState));
        private readonly TagModel mFormingProcessStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation2.Device.ProcessedState);
        private readonly TagModel mInspectedDambarProcessStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation1.Device.ProcessedState);

        private readonly TagModel mDambarInspectionResultTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation1.Device.DambarInspectionResult);
      
        public bool InfeedHasMaterial
        {
            get => mInfeedHasMaterial;
            set
            {
                if (mInfeedHasMaterial != value)
                {
                    mInfeedHasMaterial = value;

                    if (mInfeedHasMaterial)
                        InfeedHasMaterialChanged?.Invoke(this, GetPressPositionId((int)DeviceLocation.Infeed));
                    else
                        InfeedHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool TrimmingHasMaterial
        {
            get => mTrimmingHasMaterial;
            set
            {
                if (mTrimmingHasMaterial != value)
                {
                    mTrimmingHasMaterial = value;
                    if (mTrimmingHasMaterial)
                        TrimmingHasMaterialChanged?.Invoke(this, GetPressPositionId((int)DeviceLocation.Trim));
                    else
                        TrimmingHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool WasteBinHasMaterial
        {
            get => mWasteBinHasMaterial;
            set
            {
                if (mWasteBinHasMaterial != value)
                {
                    mWasteBinHasMaterial = value;
                    if (mWasteBinHasMaterial)
                        WasteBinHasMaterialChanged?.Invoke(this, GetPressPositionId((int)DeviceLocation.WasteBin));
                    else
                        WasteBinHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool DambarHasMaterial
        {
            get => mDambarHasMaterial;
            set
            {
                if (mDambarHasMaterial != value)
                {
                    mDambarHasMaterial = value;
                    if (mDambarHasMaterial)
                        DambarHasMaterialChanged?.Invoke(this, GetPressPositionId((int)DeviceLocation.Dambar));
                    else
                        DambarHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool FormingHasMaterial
        {
            get => mFormingHasMaterial;
            set
            {
                if (mFormingHasMaterial != value)
                {
                    mFormingHasMaterial = value;
                    if (mFormingHasMaterial)
                        FormingHasMaterialChanged?.Invoke(this, GetPressPositionId((int)DeviceLocation.Form));
                    else
                        FormingHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition0HasMaterial
        {
            get => mIndexerPosition0HasMaterial;
            set
            {
                if (mIndexerPosition0HasMaterial != value)
                {
                    mIndexerPosition0HasMaterial = value;
                    if (mIndexerPosition0HasMaterial)
                        IndexerPosition0HasMaterialChanged?.Invoke(this, GetIndexerPositionId(0));
                    else
                        IndexerPosition0HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition1HasMaterial
        {
            get => mIndexerPosition1HasMaterial;
            set
            {
                if (mIndexerPosition1HasMaterial != value)
                {
                    mIndexerPosition1HasMaterial = value;
                    if (mIndexerPosition1HasMaterial)
                        IndexerPosition1HasMaterialChanged?.Invoke(this, GetIndexerPositionId(1));
                    else
                        IndexerPosition1HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }        
        
        public bool IndexerPosition1HasWaste
        {
            get => mIndexerPosition1HasWaste;
            set
            {
                if (mIndexerPosition1HasWaste != value)
                {
                    mIndexerPosition1HasWaste = value;
                    IndexerPosition1HasWasteChanged?.Invoke(this, mIndexerPosition1HasWaste);
                }
            }
        }

        public bool IndexerPosition2HasMaterial
        {
            get => mIndexerPosition2HasMaterial;
            set
            {
                if (mIndexerPosition2HasMaterial != value)
                {
                    mIndexerPosition2HasMaterial = value;
                    if (mIndexerPosition2HasMaterial)
                        IndexerPosition2HasMaterialChanged?.Invoke(this, GetIndexerPositionId(2));
                    else
                        IndexerPosition2HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition3HasMaterial
        {
            get => mIndexerPosition3HasMaterial;
            set
            {
                if (mIndexerPosition3HasMaterial != value)
                {
                    mIndexerPosition3HasMaterial = value;
                    if (mIndexerPosition3HasMaterial)
                        IndexerPosition3HasMaterialChanged?.Invoke(this, GetIndexerPositionId(3));
                    else
                        IndexerPosition3HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition4HasMaterial
        {
            get => mIndexerPosition4HasMaterial;
            set
            {
                if (mIndexerPosition4HasMaterial != value)
                {
                    mIndexerPosition4HasMaterial = value;
                    if (mIndexerPosition4HasMaterial)
                        IndexerPosition4HasMaterialChanged?.Invoke(this, GetIndexerPositionId(4));
                    else
                        IndexerPosition4HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }
        //public int TrimmingProcessState
        //{
        //    get => mTrimmingProcessState;
        //    set
        //    {
        //        if (mTrimmingProcessState != value)
        //        {
        //            mTrimmingProcessState = value;

        //            if (value == (int)DeviceProcessState.Trimmed)
        //            {
        //                var id = GetPressPositionDeviceId((int)DeviceLocation.Trim);

        //                if (id != 0)
        //                {
        //                    mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mTrimmingProcessState);
        //                }
        //            }
        //        }
        //    }
        //}

        public int TrimmingProcessState
        {
            get => mTrimmingProcessState;
            set
            {
                if (mTrimmingProcessState != value)
                {
                    mTrimmingProcessState = value;

                    if (mTrimmingProcessState != 0)
                    {
                        var id = GetPressPositionId(5);
                        mLog.Debug($"TrimmingProcessState id: {id}, result: {mTrimmingProcessState}");
                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, DeviceProcessState.Trimmed);
                        }
                    }
                }
            }
        }

        //public int FormingProcessState
        //{
        //    get => mFormingProcessState;
        //    set
        //    {
        //        if (mFormingProcessState != value)
        //        {
        //            mFormingProcessState = value;

        //            if (value == (int)DeviceProcessState.Formed)
        //            {
        //                var id = GetPressPositionId((int)DeviceLocation.Form);

        //                if (id != 0)
        //                {
        //                    mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mFormingProcessState);
        //                }
        //            }
        //        }
        //    }
        //}

        public int FormingProcessState
        {
            get => mFormingProcessState;
            set
            {
                if (mFormingProcessState != value)
                {
                    mFormingProcessState = value;

                    if (value == (int)DeviceProcessState.Formed)
                    {
                        var id = GetPressPositionId((int)DeviceLocation.Form);
                        mLog.Debug($"FormingProcessState id: {id}, result: {mFormingProcessState}");

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mFormingProcessState);
                        }
                    }
                }
            }
        }

        public int InspectedDambarProcessState
        {
            get => mInspectedDambarProcessState;
            set
            {
                if (mInspectedDambarProcessState != value)
                {
                    mInspectedDambarProcessState = value;

                    if (value == (int)DeviceProcessState.InspectedDambar)
                    {
                        var id = GetPressPositionId((int)DeviceLocation.Dambar);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mInspectedDambarProcessState);
                        }
                    }
                }
            }
        }

        public int InspectionDambarResult
        {
            get => mInspectionDambarResult;
            set
            {
                if (mInspectionDambarResult != value)
                {
                    mInspectionDambarResult = value;

                    if (value != (int)ValidationResult.None)
                    {
                        var id = GetPressPositionId((int)DeviceLocation.Dambar);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, DeviceProcessTypeResult.InspectionDambarResult, (InspectionResult) mInspectionDambarResult);
                        }
                    }
                }
            }
        }


        public MaterialTracking()
        {
            mMaterialTracking = MaterialTrackingApplication.Instance;
            mLotControl = LotControl.Application.LotControl.Instance;

            TagModelPathDictionary = new()
            {
                {mInfeedHasMaterialTagModel , nameof(InfeedHasMaterial) },
                {mTrimmingHasMaterialTagModel , nameof(TrimmingHasMaterial) },
                {mWasteBinHasMaterialTagModel , nameof(WasteBinHasMaterial) },
                {mDambarHasMaterialTagModel , nameof(DambarHasMaterial) },
                {mFormingHasMaterialTagModel , nameof(FormingHasMaterial) },
                {mIndexPosition0HasMaterial  , nameof(IndexerPosition0HasMaterial) },
                {mIndexPosition1HasMaterial  , nameof(IndexerPosition1HasMaterial) },
                {mIndexPosition2HasMaterial  , nameof(IndexerPosition2HasMaterial) },
                {mIndexPosition3HasMaterial  , nameof(IndexerPosition3HasMaterial) },
                {mIndexPosition4HasMaterial  , nameof(IndexerPosition4HasMaterial) },
                {mIndexPosition1HasWaste, nameof(IndexerPosition1HasWaste) },
                {mTrimmingProcessStateTagModel, nameof(TrimmingProcessState) },
                {mFormingProcessStateTagModel, nameof(FormingProcessState) },
                {mInspectedDambarProcessStateTagModel, nameof(InspectedDambarProcessState) },
                {mDambarInspectionResultTagModel, nameof(InspectionDambarResult) }
            };
        }
        public int GetIndexerPositionId(int position)
        {
            var output = 0;
            try
            {
                TagModel tagModel = position switch
                {
                    0 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerLeadframeLocations.LeadframeLocation0.Leadframe.Id),
                    1 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation0.Device.Id),
                    2 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation1.Device.Id),
                    3 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation2.Device.Id),
                    4 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleIndexerDeviceLocations.DeviceLocation3.Device.Id),
                    _ => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation0.Device.Id),
                };
                output = (int)mPlcService.ReadValue(tagModel);
            }
            catch (Exception)
            {
                mLog.Error("Failed to get device id for indexer position: " + position);
            }
            return output;
        }

        public int GetPressPositionId(int position)
        {
            var output = 0;
            try
            {
                TagModel tagModel = position switch
                {
                    0 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation0.Leadframe.Id),
                    1 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation1.Leadframe.Id),
                    2 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation0.Device.Id),
                    3 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation1.Device.Id),
                    4 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation2.Device.Id),

                    5 => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleLeadframeLocations.LeadframeLocation1.Leadframe.Devices[0].Id),
                    _ => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.Press2ModuleDeviceLocations.DeviceLocation0.Device.Id),
                };

                output = (int)mPlcService.ReadValue(tagModel);
            }
            catch(Exception)
            {
                mLog.Error("Failed to get device id for position: " + position);
            }
            return output;
        }      
    }
}
