﻿using MachineManager.Lib;
using Platform.Application;
using PressModule.Lib;
using PressModule.PLCInterfaces.OpcUa;

namespace PressModule.Application
{
    public class PressModule: MainProcess
    {       
        private static PressModule mPressmodule;
        
        private readonly TagModel mPressModuleStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.Status);
        private readonly TagModel mPressModuleStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.State);
        private readonly TagModel mPressModuleIsRunningProductionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.IsRunningProduction);
        private readonly TagModel mPressModuleHasErrorTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.HasError);
        private readonly TagModel mPressModuleInSimulationTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.InSimulation);
                
        private readonly TagModel mInfeedCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.InfeedCommand);
        private readonly TagModel mTrimCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.TrimCommand);
        private readonly TagModel mFormCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.FormCommand);
        private readonly TagModel mIndexPickCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.IndexPickCommand);
        private readonly TagModel mIndexPlaceCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.IndexPlaceCommand);
        private readonly TagModel mDambarCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.DambarCommand);
        private readonly TagModel mRemoveTrimToolTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.RemoveTrimToolCommand);
        private readonly TagModel mTrimMaintenanceTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.TrimMaintenanceCommand);
        private readonly TagModel mRemoveFormToolTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.RemoveFormToolCommand);
        private readonly TagModel mFormMaintenanceTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.FormMaintenanceCommand);       
        private readonly TagModel mClearMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.ClearMaterial);
               
        public static PressModule Instance => mPressmodule ?? (mPressmodule = new PressModule());
        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();
        public MaterialTracking MaterialTracking { get; } = new MaterialTracking();
        public EventHandler EventHandler { get; } = new();

        public PressModule()
            : base(nameof(PressModule), PressModuleEvents.PressModuleInSimulation.ID)
        {
            mInitializeCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.InitializeCommand);
            mStartCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.StartCommand);
            mStopCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.StopCommand);

            mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.Status);
            mStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.State);
            mHasErrorTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.HasError);
            mIsRunningProductionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.IsRunningProduction);
            mInSimulationTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPc.InSimulation);

            mEnableSimulationTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.EnableSimulation);
            mVelocityPercentageTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.pressModuleMainProcessInterface.ToPlc.VelocityPercentage);

                        //Resources
            AddTagModelDictionary(() => Resources.Trim, Resources.Trim.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.Form, Resources.Form.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.Indexer, Resources.Indexer.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.VacuumCleaner, Resources.VacuumCleaner.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.DambarCheck, Resources.DambarCheck.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.WasteBin, Resources.WasteBin.TagModelPathDictionary);

            AddTagModelDictionary(GetNameOf(() => SubProcesses.Infeed), SubProcesses.Infeed.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.IndexHandler), SubProcesses.IndexHandler.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.Trimming), SubProcesses.Trimming.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.Dambar), SubProcesses.Dambar.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.Forming), SubProcesses.Forming.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.WasteHandler), SubProcesses.WasteHandler.TagModelPathDictionary);

            AddTagModelDictionary(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(EventHandler), EventHandler.TagModelPathDictionary);

            AddSubscription();

            mEventManager.RegisterEvents(PressModuleEvents.Events);
                     
        }

        protected override void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }        
               
        public async void ClearMaterial()
        {
            mLog.Debug($"Clear material: PressModule");
            await mPLCService.WriteValueAsync(mClearMaterial, true);
        }

        public void ExecuteTrim()
        {
            mLog.Debug($"ExecuteTrim: PressModule");
            mPLCService.WriteValue(mTrimCommandTagModel, true);
        }

        public void ExecuteForm()
        {
            mLog.Debug($"ExecuteForm: PressModule");
            mPLCService.WriteValue(mFormCommandTagModel, true);
        }
        public void ExecuteIndexerPick()
        {
            mLog.Debug($"ExecuteIndexerPick: PressModule");
            mPLCService.WriteValue(mIndexPickCommandTagModel, true);
        }
        public void ExecuteIndexerPlace()
        {
            mLog.Debug($"ExecuteIndexerPlace: PressModule");
            mPLCService.WriteValue(mIndexPlaceCommandTagModel, true);
        }    
        public void ExecuteRemoveTrimTool()
        {
            mLog.Debug($"ExecuteRemoveTrimTool: PressModule");
            mPlcService.WriteValue(mRemoveTrimToolTagModel, true);
        }

        public void ExecuteTrimMaintenance()
        {
            mLog.Debug($"ExecuteGoToTrimMaintenance: PressModule");
            mPlcService.WriteValue(mTrimMaintenanceTagModel, true);
        }

        public void ExecuteRemoveFormTool()
        {
            mLog.Debug($"ExecuteRemoveFormTool: PressModule");
            mPlcService.WriteValue(mRemoveFormToolTagModel, true);
        }

        public void ExecuteFormMaintenance()
        {
            mLog.Debug($"ExecuteGoToFormMaintenance: PressModule");
            mPlcService.WriteValue(mFormMaintenanceTagModel, true);
        }
        public void ExecuteDambar()
        {
            mLog.Debug($"ExecuteDambar: PressModule");
            mPLCService.WriteValue(mDambarCommandTagModel, true);
        }        
    }
}
