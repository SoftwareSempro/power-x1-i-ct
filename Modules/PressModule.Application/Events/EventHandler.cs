﻿using EventHandler.Application;
using PressModule.Lib;

namespace PressModule.Application
{
    public class EventHandler : EventHandlerSubScriptionBase<PressModuleEvents>
    {
        #region SubProcesses
        //Trimming
        public bool TrimProcessFailedToClaimSafeZone { set => SetEventPropertyValue(value); }
        public bool TrimToolFailedToReachSensorPosition { set => SetEventPropertyValue(value); }
        public bool TrimToolFailedToReachClosePosition { set => SetEventPropertyValue(value); }
        public bool TrimToolFailedToReachOpenPosition { set => SetEventPropertyValue(value); }
        public bool TrimToolFailedToReachSafePosition { set => SetEventPropertyValue(value); }
        public bool TrimToolNotLocked { set => SetEventPropertyValue(value); }
        public bool TrimToolNotLockedError { set => SetEventPropertyValue(value); }
        public bool TrimWasteBinNotPresentWarning { set => SetEventPropertyValue(value); }
        public bool TrimWasteBinNotPresentError { set => SetEventPropertyValue(value); }

        //Dambar check
        public bool DambarCheckNotOk { set => SetEventPropertyValue(value); }

        //Forming
        public bool FormProcessFailedToClaimSafeZone { set => SetEventPropertyValue(value); }
        public bool FormToolFailedToReachSensorPosition { set => SetEventPropertyValue(value); }
        public bool FormToolFailedToReachClosePosition { set => SetEventPropertyValue(value); }
        public bool FormToolFailedToReachOpenPosition { set => SetEventPropertyValue(value); }
        public bool FormToolFailedToReachSafePosition { set => SetEventPropertyValue(value); }
        public bool FormToolNotLocked { set => SetEventPropertyValue(value); }
        public bool FormToolNotLockedError { set => SetEventPropertyValue(value); }
        public bool FormWasteBinNotPresentWarning { set => SetEventPropertyValue(value); }
        public bool FormWasteBinNotPresentError { set => SetEventPropertyValue(value); }

        //Index handler
        public bool IndexerFailedToReachInfeedPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachOutfeedPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachUpPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachDownPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachPickPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachPlacePosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToPickUpProduct { set => SetEventPropertyValue(value); }
        public bool IndexerLostProductDuringIndexing { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToPlaceProduct { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachPinUpPosition { set => SetEventPropertyValue(value); }
        public bool IndexerFailedToReachPinDownPosition { set => SetEventPropertyValue(value); }
        public bool IndexerProcessFailedToClaimSafeZone { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpProductAtInfeed { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpProductAtTrim { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpWaste { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpProductAtWasteTable { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpProductAtDambar { set => SetEventPropertyValue(value); }
        public bool IndexHandlerIndexerFailedToPickUpProductAtForm { set => SetEventPropertyValue(value); }
        public bool IndexerLostWasteDuringIndexing { set => SetEventPropertyValue(value); }

        //Waste handler
        public bool LeadframeWasteBinAlmostFull { set => SetEventPropertyValue(value); }
        public bool LeadframeWasteBinFull { set => SetEventPropertyValue(value); }
        public bool LeadframeWasteBinNotPresent { set => SetEventPropertyValue(value); }
        public bool LeadframeWasteBinNotPresentError { set => SetEventPropertyValue(value); }

        #endregion
        
        #region Resources
        //Trim
        public bool TrimDriveError { set => SetEventPropertyValue(value); }
        public bool TrimCommunicationError { set => SetEventPropertyValue(value); }
        public bool TrimPositionError { set => SetEventPropertyValue(value); }
        public bool TrimNotSafeToMoveInSafeZone { set => SetEventPropertyValue(value); }

        //Form
        public bool FormDriveError { set => SetEventPropertyValue(value); }
        public bool FormCommunicationError { set => SetEventPropertyValue(value); }
        public bool FormPositionError { set => SetEventPropertyValue(value); }
        public bool FormNotSafeToMoveInSafeZone { set => SetEventPropertyValue(value); }
        
        #endregion

        #region Safety
        public bool TrimCoverOpen { set => SetEventPropertyValue(value); }
        public bool TrimCoverLockError { set => SetEventPropertyValue(value); }
        public bool FormCoverOpen { set => SetEventPropertyValue(value); }
        public bool FormCoverLockError { set => SetEventPropertyValue(value); }
        public bool PressModuleEmergencyStopActive { set => SetEventPropertyValue(value); }
        public bool PressModuleNotallCoversLocked { set => SetEventPropertyValue(value); }
        #endregion
        public EventHandler()
           : base()
        {
            RegisterTagModelsInDictionary();
        }       
    }
}
