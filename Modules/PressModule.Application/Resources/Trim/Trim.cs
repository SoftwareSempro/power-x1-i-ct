﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class Trim : Resource
    {        
        private bool mToolLocked;
        private double mPositionZ;
     
        public Action PositionChanged;
        public Action SensorStatusChanged;

        private static new TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPc.Status);
        public TagModel PositionZTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPc.PositionZ);
        public TagModel ToolLockedTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPc.ToolLocked);
        public TagModel GoToTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPlc.GoToCommand);
        public TagModel SetPointZTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPlc.SetPointZ);
        public TagModel VelocityTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimInterface.ToPlc.Velocity);

        public double PositionZ
        {
            get => mPositionZ;
            set
            {
                if (mPositionZ != value)
                {
                    mPositionZ = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        public bool ToolLocked
        {
            get => mToolLocked;
            private set
            {
                if (mToolLocked != value)
                {
                    mToolLocked = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public Trim()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },
                { PositionZTagModel, nameof(PositionZ) },
                { ToolLockedTagModel, nameof(ToolLocked) },
          };
        }


        public async void GoTo(double inputZ, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(SetPointZTagModel, Convert.ToSingle(inputZ));
            await PlcService.Instance.WriteValueAsync(VelocityTagModel, Convert.ToInt16(velocity));
            await PlcService.Instance.WriteValueAsync(GoToTagModel, true);
        }
    }
}
