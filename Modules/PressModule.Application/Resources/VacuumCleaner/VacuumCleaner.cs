﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class VacuumCleaner : Resource
    {       
        private static new TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleVacuumCleanerInterface.ToPc.Status);
        public TagModel VacuumCleanerOnCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleVacuumCleanerInterface.ToPlc.VacuumCleanerOnCommand);
        public TagModel VacuumCleanerOffCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleVacuumCleanerInterface.ToPlc.VacuumCleanerOffCommand);

        public VacuumCleaner()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },                
          };
        }

        public async void On()
        {
            await PlcService.Instance.WriteValueAsync(VacuumCleanerOnCommandTagModel, true);
        }

        public async void Off()
        {
            await PlcService.Instance.WriteValueAsync(VacuumCleanerOffCommandTagModel, true);
        }
    }
}
