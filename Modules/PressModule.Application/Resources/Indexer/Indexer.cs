﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
    public class Indexer : Resource
    {       
        private bool mIsOnInfeed;
        private bool mIsOnOutfeed;
        private bool mIsDown;
        private bool mIsUp;
        private bool mIsVacuum1On;
        private bool mIsVacuum2On;
        private bool mIsVacuum3On;
        private bool mIsVacuum4On;
        private bool mIsVacuum5On;
        private bool mIsVacuum6On;
                
        public Action SensorStatusChanged;

        private static new TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.Status);
        public static TagModel IsOnInfeedTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsOnInfeed);
        public static TagModel IsOnOutfeedTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsOnOutfeed);
        public static TagModel IsUpTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsUp);
        public static TagModel IsDownTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsDown);
        public static TagModel IsVacuum1OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum1On);
        public static TagModel IsVacuum2OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum2On);
        public static TagModel IsVacuum3OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum3On);
        public static TagModel IsVacuum4OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum4On);
        public static TagModel IsVacuum5OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum5On);
        public static TagModel IsVacuum6OnTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPc.IsVacuum6On);
                      
        public static TagModel InfeedCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.InfeedCommand);
        public static TagModel OutfeedCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.OutfeedCommand);
        public static TagModel PinUpCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.PinUpCommand);
        public static TagModel PinDownCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.PinDownCommand);
        public static TagModel VacuumOnCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumOnCommand);
        public static TagModel VacuumOffCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumOffCommand);
        public static TagModel VacuumChannel1TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel0);
        public static TagModel VacuumChannel2TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel1);
        public static TagModel VacuumChannel3TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel2);
        public static TagModel VacuumChannel4TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel3);
        public static TagModel VacuumChannel5TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel4);
        public static TagModel VacuumChannel6TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.VacuumChannelSettings.Channel5);
              
        public static TagModel ActivateBlowOffCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.ActivateBlowOffCommand);
        public static TagModel DeactivateBlowOffCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.DeactivateBlowOffCommand);
        public static TagModel BlowOffChannel1TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel0);
        public static TagModel BlowOffChannel2TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel1);
        public static TagModel BlowOffChannel3TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel2);
        public static TagModel BlowOffChannel4TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel3);
        public static TagModel BlowOffChannel5TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel4);
        public static TagModel BlowOffChannel6TagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerInterface.ToPlc.BlowOffChannelSettings.Channel5);


        public bool IsOnInfeed
        {
            get => mIsOnInfeed;
            private set
            {
                if (mIsOnInfeed != value)
                {
                    mIsOnInfeed = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsOnOutfeed
        {
            get => mIsOnOutfeed;
            private set
            {
                if (mIsOnOutfeed != value)
                {
                    mIsOnOutfeed = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsUp
        {
            get => mIsUp;
            private set
            {
                if (mIsUp != value)
                {
                    mIsUp = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsDown
        {
            get => mIsDown;
            private set
            {
                if (mIsDown != value)
                {
                    mIsDown = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum1On
        {
            get => mIsVacuum1On;
            private set
            {
                if (mIsVacuum1On != value)
                {
                    mIsVacuum1On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum2On
        {
            get => mIsVacuum2On;
            private set
            {
                if (mIsVacuum2On != value)
                {
                    mIsVacuum2On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum3On
        {
            get => mIsVacuum3On;
            private set
            {
                if (mIsVacuum3On != value)
                {
                    mIsVacuum3On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum4On
        {
            get => mIsVacuum4On;
            private set
            {
                if (mIsVacuum4On != value)
                {
                    mIsVacuum4On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum5On
        {
            get => mIsVacuum5On;
            private set
            {
                if (mIsVacuum5On != value)
                {
                    mIsVacuum5On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsVacuum6On
        {
            get => mIsVacuum6On;
            private set
            {
                if (mIsVacuum6On != value)
                {
                    mIsVacuum6On = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public Indexer()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },
                { IsOnInfeedTagModel, nameof(IsOnInfeed) },
                { IsOnOutfeedTagModel, nameof(IsOnOutfeed) },
                { IsUpTagModel , nameof(IsUp) },
                { IsDownTagModel , nameof(IsDown) },
                { IsVacuum1OnTagModel , nameof(IsVacuum1On) },
                { IsVacuum2OnTagModel , nameof(IsVacuum2On) },
                { IsVacuum3OnTagModel , nameof(IsVacuum3On) },
                { IsVacuum4OnTagModel  , nameof(IsVacuum4On) },
                { IsVacuum5OnTagModel  , nameof(IsVacuum5On) },
                { IsVacuum6OnTagModel  , nameof(IsVacuum6On) },
          };
        }

        public async void ToLeft()
        {
            await PlcService.Instance.WriteValueAsync(InfeedCommandTagModel, true);
        }

        public async void ToRight()
        {
            await PlcService.Instance.WriteValueAsync(OutfeedCommandTagModel, true);
        }

        public async void PinDown()
        {
            await PlcService.Instance.WriteValueAsync(PinDownCommandTagModel, true);
        }

        public async void PinUp()
        {
            await PlcService.Instance.WriteValueAsync(PinUpCommandTagModel, true);
        }

        public async void VacuumOn(bool[] vacuumSettings)
        {
            await PlcService.Instance.WriteValueAsync(VacuumChannel1TagModel, vacuumSettings[0]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel2TagModel, vacuumSettings[1]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel3TagModel, vacuumSettings[2]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel4TagModel, vacuumSettings[3]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel5TagModel, vacuumSettings[4]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel6TagModel, vacuumSettings[5]);

            await PlcService.Instance.WriteValueAsync(VacuumOnCommandTagModel, true);
        }

        public async void VacuumOff(bool[] vacuumSettings)
        {
            await PlcService.Instance.WriteValueAsync(VacuumChannel1TagModel, vacuumSettings[0]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel2TagModel, vacuumSettings[1]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel3TagModel, vacuumSettings[2]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel4TagModel, vacuumSettings[3]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel5TagModel, vacuumSettings[4]);
            await PlcService.Instance.WriteValueAsync(VacuumChannel6TagModel, vacuumSettings[5]);

            await PlcService.Instance.WriteValueAsync(VacuumOffCommandTagModel, true);
        }

        public async void ActivateVacuumBlowOff(bool[] vacuumSettings)
        {
            await PlcService.Instance.WriteValueAsync(BlowOffChannel1TagModel, vacuumSettings[0]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel2TagModel, vacuumSettings[1]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel3TagModel, vacuumSettings[2]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel4TagModel, vacuumSettings[3]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel5TagModel, vacuumSettings[4]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel6TagModel, vacuumSettings[5]);
            await PlcService.Instance.WriteValueAsync(ActivateBlowOffCommandTagModel, true);
        }

        public async void DeactivateVacuumBlowOff(bool[] vacuumSettings)
        {
            await PlcService.Instance.WriteValueAsync(BlowOffChannel1TagModel, vacuumSettings[0]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel2TagModel, vacuumSettings[1]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel3TagModel, vacuumSettings[2]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel4TagModel, vacuumSettings[3]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel5TagModel, vacuumSettings[4]);
            await PlcService.Instance.WriteValueAsync(BlowOffChannel6TagModel, vacuumSettings[5]);
            await PlcService.Instance.WriteValueAsync(DeactivateBlowOffCommandTagModel, true);
        }
    }
}
