﻿namespace PressModule.Application.Resources
{
   public class Resources
    {
        public Trim Trim { get; } = new Trim();
        public Form Form { get; } = new Form();
        public Indexer Indexer { get; } = new Indexer();
        public DambarCheck DambarCheck { get; } = new DambarCheck();
        public WasteBin WasteBin { get; } = new WasteBin();
        public VacuumCleaner VacuumCleaner { get; } = new VacuumCleaner();
    }
}
