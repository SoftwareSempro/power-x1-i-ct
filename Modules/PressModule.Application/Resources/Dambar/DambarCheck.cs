﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
    public class DambarCheck : Resource    {
        
        private bool mDambarCheckCommandResult;                
        public Action SensorStatusChanged;
        public Action ResultsChanged;

        private static new TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarCheckInterface.ToPc.Status);
        public static TagModel DambarCheckCommandResultTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarCheckInterface.ToPc.DambarCheckCommandResult);
        public static TagModel DambarCheckCommandTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarCheckInterface.ToPlc.DambarCheckCommand);
          
        public bool DambarCheckCommandResult
        {
            get => mDambarCheckCommandResult;
            private set
            {
                if (mDambarCheckCommandResult != value)
                {
                    mDambarCheckCommandResult = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public DambarCheck()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },
                { DambarCheckCommandResultTagModel, nameof(DambarCheckCommandResult) },                
          };
        }

        public static async void DoDambarCheck()
        {;
            await PlcService.Instance.WriteValueAsync(DambarCheckCommandTagModel, true);
        }
    }
}