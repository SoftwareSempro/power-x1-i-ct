﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
    public class Form : Resource
    {
        private double mPositionZ;
        private bool mToolLocked;        

        public Action PositionChanged;
        public Action SensorStatusChanged;

        private static new TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPc.Status);
        public static TagModel PositionZTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPc.PositionZ);
        public static TagModel ToolLockedTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPc.ToolLocked);
        public static TagModel GoToTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPlc.GoToCommand);
        public static TagModel SetPointZTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPlc.SetPointZ);
        public static TagModel VelocityTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormInterface.ToPlc.Velocity);


        public double PositionZ
        {
            get => mPositionZ;
            set
            {
                if (mPositionZ != value)
                {
                    mPositionZ = value;
                    PositionChanged?.Invoke();
                }
            }
        }
        public bool ToolLocked
        {
            get => mToolLocked;
            set
            {
                if (mToolLocked != value)
                {
                    mToolLocked = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public Form()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },
                { PositionZTagModel, nameof(PositionZ) },
                { ToolLockedTagModel, nameof(ToolLocked) },
          };
        }

        public async void GoTo(double inputZ, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(SetPointZTagModel, Convert.ToSingle(inputZ));
            await PlcService.Instance.WriteValueAsync(VelocityTagModel, Convert.ToInt16(velocity));
            await PlcService.Instance.WriteValueAsync(GoToTagModel, true);
        }
    }
}
