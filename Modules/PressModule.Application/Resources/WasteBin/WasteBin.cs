﻿using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
    public class WasteBin : Resource
    {
        private bool mBinPresent;
        public Action SensorStatusChanged;

        public TagModel mBinPresentTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteBinInterface.ToPc.WasteBinPresent);

        public bool BinPresent
        {
            get => mBinPresent;
            set
            {
                if (mBinPresent != value)
                {
                    mBinPresent = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public WasteBin()
        {
            TagModelPathDictionary = new()
           {                
                { mBinPresentTagModel, nameof(BinPresent) }              
          };
        }
    }
}
