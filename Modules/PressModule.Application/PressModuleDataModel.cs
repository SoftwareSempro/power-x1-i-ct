﻿using System;

namespace PressModule.Application
{
   public class PressModuleDataModel
    {
        private int mVelocityPercentage;

        public event EventHandler<PressModuleDataModel> DataModelChanged;
        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                mVelocityPercentage = value;
                DataModelChanged?.Invoke(this, this);
            }
        }
    }
}
