﻿namespace PressModule.Application.SubProcesses
{
    public class SubProcesses
    {
        public Infeed.Infeed Infeed { get; } = new ();
        public Trimming Trimming { get; } = new ();
        public Forming Forming { get; } = new();
        public IndexHandler IndexHandler { get; } = new();
        public Dambar Dambar { get; } = new();
        public WasteHandler WasteHandler { get; } = new();
    }
}
