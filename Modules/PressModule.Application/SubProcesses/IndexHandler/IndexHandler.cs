﻿using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.SubProcesses
{
    public class IndexHandler : SubProcess
    {
        private readonly TagModel mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexHandlerInterface.ToPc.Status);        
        private static readonly TagModel mRemoveMaterialCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexHandlerInterface.ToPlc.DefaultAssistCommands.RemoveMaterialCommand);
       
        private readonly MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private readonly LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;
        private readonly ILog mLog = LogClient.Get();       
       
        public IndexHandler()
        {
            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)}
            };
        }
        private async void ExecuteRemoveMaterialCommand(int removedId)
        {
            mLog.Info($"ExecuteRemoveMaterialCommand: {removedId}");
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
            mMaterialTracking.UpdateLeadFrame(removedId, mLotControl.CurrentLotId, LeadframeProcessState.Removed);
            mMaterialTracking.UpdateDevice(removedId, mLotControl.CurrentLotId, DeviceProcessState.Removed);
        }

        public void ExecuteRemoveDeviceCommand(int removedId)
        {
            ExecuteRemoveMaterialCommand(removedId);
            mMaterialTracking.UpdateDevice(removedId, mLotControl.CurrentLotId, DeviceProcessState.Removed);
        }

        public void ExecuteRemoveLeadframeCommand(int removedId)
        {
            ExecuteRemoveMaterialCommand(removedId);
            mMaterialTracking.UpdateLeadFrame(removedId, mLotControl.CurrentLotId, LeadframeProcessState.Removed);
        }

        public async void ExecuteRemoveWasteCommand()
        {
            mLog.Info($"ExecuteRemoveWasteCommand");
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
        }
    }
}
