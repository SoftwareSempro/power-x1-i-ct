﻿using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.SubProcesses
{
    public class Dambar : SubProcess
    {
        private readonly TagModel mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarInterface.ToPc.Status);
        private static readonly TagModel mRetryCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarInterface.ToPlc.DefaultAssistCommands.RetryCommand);
        private static readonly TagModel mRemoveMaterialCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarInterface.ToPlc.DefaultAssistCommands.RemoveMaterialCommand);


        private MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;


        public Dambar()
        {
            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)}
            };
        }

        public static async void RetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryCommandTagModel, true);
        }

        public async void RemoveMaterialCommand(int removedDeviceId)
        {
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
            mMaterialTracking.UpdateDevice(removedDeviceId, mLotControl.CurrentLotId, DeviceProcessState.Removed);
        }
    }
}
