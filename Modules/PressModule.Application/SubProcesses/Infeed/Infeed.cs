﻿using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.SubProcesses.Infeed
{
    public class Infeed : SubscriptionBase
    {

        private readonly TagModel mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleInfeedInterface.ToPc.Status);
        private static readonly TagModel mRetryCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleInfeedInterface.ToPlc.RetryCommand);
        private static readonly TagModel mRemoveMaterialCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleInfeedInterface.ToPlc.RemoveMaterialCommand);
        private int mStatus;

        private MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;

        public event EventHandler<int> StatusChanged;
        public int Status
        {
            get => mStatus;
                private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public Infeed()
        {
            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)}
            };
        }

        public static async void RetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryCommandTagModel, true);
        }

        public async void RemoveMaterialCommand(int removedDeviceId)
        {
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
            mMaterialTracking.UpdateDevice(removedDeviceId, mLotControl.CurrentLotId, DeviceProcessState.Removed);
        }
    }
}
