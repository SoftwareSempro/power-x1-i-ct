﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses
{
    public class Forming : SubProcess
    {
             

        private readonly TagModel mFormingStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPc.Status);

        private readonly TagModel mTopPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.TopPosition);
        private readonly TagModel mSensorPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.SensorPosition);
        private readonly TagModel mClosePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.ClosePosition);
        private readonly TagModel mSafePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.SafePositionZ);
        private readonly TagModel mRemovePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.RemovePosition);
        private readonly TagModel mMaintenancePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.MaintenacePosition);
        private readonly TagModel mVelocityTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingInterface.ToPlc.Forming.Velocity);
        private FormingDataModel mSettings;        

        public FormingDataModel Settings => mSettings;
        public event EventHandler<FormingDataModel> SettingsChanged;

        public double TopPosition
        {
            get => (float)mSettings.TopPosition;
            set
            {
                if (mSettings.TopPosition != value)
                {
                    mSettings.TopPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double SensorPosition
        {
            get => (float)mSettings.SensorPosition;
            set
            {
                if (mSettings.SensorPosition != value)
                {
                    mSettings.SensorPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double ClosePosition
        {
            get => (float)mSettings.ClosePosition;
            set
            {
                if (mSettings.ClosePosition != value)
                {
                    mSettings.ClosePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double SafePosition
        {
            get => (float)mSettings.SafePosition;
            set
            {
                if (mSettings.SafePosition != value)
                {
                    mSettings.SafePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double RemovePosition
        {
            get => (float)mSettings.RemovePosition;
            set
            {
                if (mSettings.RemovePosition != value)
                {
                    mSettings.RemovePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MaintenancePosition
        {
            get => (float)mSettings.MaintenancePosition;
            set
            {
                if (mSettings.MaintenancePosition != value)
                {
                    mSettings.MaintenancePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int Velocity
        {
            get => (short)mSettings.Velocity;
            set
            {
                if (mSettings.Velocity != value)
                {
                    mSettings.Velocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public void UpdateSettings(FormingDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(settings.TopPosition, settings.SensorPosition, settings.ClosePosition, settings.RemovePosition, settings.MaintenancePosition, settings.Velocity);
            UpdateMachineRecipeSettingsOnPLC(settings.SafePosition);
        }

        public async void UpdateMachineRecipeSettingsOnPLC(double safePosition)
        {
            await PlcService.Instance.WriteValueAsync(mSafePositionTagModel, (float)safePosition);
        }

        public async void UpdateProductRecipeSettingsOnPLC(double topPosition, double sensorPosition, double closePosition, double removePosition, double maintenancePosition, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(mTopPositionTagModel, (float)topPosition);
            await PlcService.Instance.WriteValueAsync(mSensorPositionTagModel, (float)sensorPosition);
            await PlcService.Instance.WriteValueAsync(mClosePositionTagModel, (float)closePosition);            
            await PlcService.Instance.WriteValueAsync(mRemovePositionTagModel, (float)removePosition);
            await PlcService.Instance.WriteValueAsync(mMaintenancePositionTagModel, (float)maintenancePosition);
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, (short)velocity);
        }
        public Forming()
        {
            mSettings = new FormingDataModel();

            TagModelPathDictionary = new()
            {
                 { mFormingStatusTagModel, nameof(Status) },
                 { mTopPositionTagModel, nameof(TopPosition) },
                 { mSensorPositionTagModel, nameof(SensorPosition) },
                 { mClosePositionTagModel, nameof(ClosePosition) },
                 { mSafePositionTagModel, nameof(SafePosition) },
                 { mRemovePositionTagModel, nameof(RemovePosition) },
                 { mMaintenancePositionTagModel, nameof(MaintenancePosition) },
                 { mVelocityTagModel, nameof(Velocity) }
             };                        
        }        
    }
}
