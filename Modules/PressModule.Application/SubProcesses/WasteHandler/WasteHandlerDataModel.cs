﻿namespace PressModule.Application.SubProcesses
{
   public class WasteHandlerDataModel
    {
        public int AlmostFullCount { get; set; }
        public int MaximumWasteCount { get; set; }
        public bool ResetCount { get; set; }
    }
}
