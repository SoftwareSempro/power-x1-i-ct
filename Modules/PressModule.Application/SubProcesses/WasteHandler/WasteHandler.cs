﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses
{
   public class WasteHandler: SubProcess
    {
        private PlcService mPLCService = PlcService.Instance;

        private readonly TagModel mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteHandlerInterface.ToPc.Status);
        private readonly TagModel mCountTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteHandlerInterface.ToPc.Count);

        private readonly TagModel mAlmostFullCountTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteHandlerInterface.ToPlc.WasteHandler.AlmostMaximumWasteCount);
        private readonly TagModel mMaximumWasteCountTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteHandlerInterface.ToPlc.WasteHandler.MaximumWasteCount);
        private readonly TagModel mResetCounterTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleWasteHandlerInterface.ToPlc.ResetCounter);
        private WasteHandlerDataModel mSettings;

        private int mCount;
        public event EventHandler<WasteHandlerDataModel> SettingsChanged;
        public event EventHandler<int> CountChanged;

        public WasteHandlerDataModel Settings => mSettings;        

        public int WasteHandlerCount
        {
            get => mCount;
            set
            {
                if (mCount != value)
                {
                    mCount = value;
                    CountChanged?.Invoke(this, mCount);
                }
            }
        }

        public int AlmostMaximumWasteCount
        {
            get => (short)mSettings.AlmostFullCount;
            set
            {
                if (mSettings.AlmostFullCount != value)
                {
                    mSettings.AlmostFullCount = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int MaximumWasteCount
        {
            get => (short)mSettings.MaximumWasteCount;
            set
            {
                if (mSettings.MaximumWasteCount != value)
                {
                    mSettings.MaximumWasteCount = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }                

        public void UpdateSettings(WasteHandlerDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(settings.AlmostFullCount, settings.MaximumWasteCount);
        }
        public async void UpdateProductRecipeSettingsOnPLC(int almostFullCount, int maximumWasteCount)
        {
            await PlcService.Instance.WriteValueAsync(mAlmostFullCountTagModel, (short)almostFullCount);
            await PlcService.Instance.WriteValueAsync(mMaximumWasteCountTagModel, (short)maximumWasteCount);
            
        }

        public WasteHandler()
        {
            mSettings = new WasteHandlerDataModel();

            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status) },
                { mCountTagModel, nameof(WasteHandlerCount) },
                { mAlmostFullCountTagModel, nameof(AlmostMaximumWasteCount) },
                { mMaximumWasteCountTagModel, nameof(MaximumWasteCount) }                              
            };          
        }

        public void ResetWasteHandlerCount()
        {
            mPLCService.WriteValue(mResetCounterTagModel, true);
        }
    }
}
