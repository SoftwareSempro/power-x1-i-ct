﻿using System.Collections.Generic;
using System.IO;
using EventHandler.Lib;
using Platform.Lib;

namespace RobotTrayUnloader.Lib
{
    public class RobotTrayUnloaderEvents : BaseEvents<RobotTrayUnloaderEvents>
    {
        private const string cModuleName = "RobotTrayUnloader";
        private const string cInspectionProcessName = "Inspection";
        private const string cRobotProcessName = "Robot";
        private const string cSafetyName = "Safety";

        public static Event RobotToTrayUnloaderInSimulation => new(521, "Robot to tray unloader is in simulation", EventType.Info, cModuleName, cModuleName, $"Events/{cModuleName}/{nameof(RobotToTrayUnloaderInSimulation)}");
       
        #region Subprocesses
        //Inspection
        private const string cInspectionPLCEvents = "instInspectionEvents";
        private const string cScanningstageFailedToReachInfeedInstance = "instScanningstageFailedToReachInfeed";
        private const string cScanningstageFailedToReachOutfeedInstance = "instScanningstageFailedToReachOutfeed";

        public static Event ScanningstageFailedToReachInfeed = new(500, "Scanningstage failed to reach infeed", EventType.Error, cInspectionProcessName, cModuleName, cInspectionPLCEvents, cScanningstageFailedToReachInfeedInstance, $"Events/{cModuleName}/{nameof(ScanningstageFailedToReachInfeed)}");
        public static Event ScanningstageFailedToReachOutfeed = new(501, "Scanningstage failed to reach outfeed", EventType.Error, cInspectionProcessName, cModuleName, cInspectionPLCEvents, cScanningstageFailedToReachOutfeedInstance, $"Events/{cModuleName}/{nameof(ScanningstageFailedToReachOutfeed)}");


        //Robot
        private const string cRobotProcessPLCEvents = "instRobotEvents";
        private const string cRobotFailedToReachPickPositionInstance = "instRobotFailedToReachPickPosition";
        private const string cRobotFailedToReachPlacePositionInstance = "instRobotFailedToReachPlacePosition";
        private const string cRobotFailedToReachHomePositionInstance = "instRobotFailedToReachHomePosition";
        private const string cNoEmptyPocketFoundAssistInstance = "instNoEmptyPocketFound";
        private const string cRobotFailedToPickUpProductInstance = "instRobotFailedToPickUpProduct";
        private const string cRobotLostProductDuringMovementInstance = "instRobotLostProductDuringMovement";
        private const string cRobotFailedToPlaceProductInstance = "instRobotFailedToPlaceProduct";
        private const string cRobotModuleEmergenceStopActiveInstance = "instRobotModuleEmergenceStopActive";

        public static Event RobotFailedToReachPickPosition = new(503, "Robot failed to reach pick position", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotFailedToReachPickPositionInstance, $"Events/{cModuleName}/{nameof(RobotFailedToReachPickPosition)}");
        public static Event RobotFailedToReachPlacePosition = new(504, "Robot failed to reach place position", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotFailedToReachPlacePositionInstance, $"Events/{cModuleName}/{nameof(RobotFailedToReachPlacePosition)}");
        public static Event RobotFailedToReachHomePosition = new(505, "Robot failed to reach home position", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotFailedToReachHomePositionInstance, $"Events/{cModuleName}/{nameof(RobotFailedToReachHomePosition)}");
        public static Event NoEmptyPocketFoundAssist = new(506, "No empty pocket found", EventType.Assist, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cNoEmptyPocketFoundAssistInstance, $"Events/{cModuleName}/NoEmptyPocketFoundAssist");
        public static Event RobotFailedToPickUpProduct = new(507, "Robot failed to pick up product", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotFailedToPickUpProductInstance, $"Events/{cModuleName}/{nameof(RobotFailedToPickUpProduct)}");
        public static Event RobotLostProductDuringMovement = new(508, "Robot lost product during movement", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotLostProductDuringMovementInstance, $"Events/{cModuleName}/{nameof(RobotLostProductDuringMovement)}");
        public static Event RobotFailedToPlaceProduct = new(509, "Robot failed to place product", EventType.Error, cRobotProcessName, cModuleName, cRobotProcessPLCEvents, cRobotFailedToPlaceProductInstance, $"Events/{cModuleName}/{nameof(RobotFailedToPlaceProduct)}");
        public static Event RobotModuleEmergenceStopActive = new(510, "Robot module emergency stop active", EventType.Error, "General", cModuleName, cRobotProcessPLCEvents, cRobotModuleEmergenceStopActiveInstance, $"Events/{cModuleName}/{nameof(RobotModuleEmergenceStopActive)}");
        #endregion

        #region Resources
        //ScannnigStage
        private const string cScanningStagePLCEvents = "instScanningStageEvents";
        private const string cScanningStageDriveError = "instDriveError";
        private const string cScanningStageCommunicationError = "instCommunicationError";
        private const string cScanningStagePositionError = "instPositionError";

        public static Event ScanningStageDriveError = new(511, "Scanning stage failed to move: drive error", EventType.Error, cInspectionProcessName, cModuleName, cScanningStagePLCEvents, cScanningStageDriveError, $"Events/{cModuleName}/{nameof(ScanningStageDriveError)}");
        public static Event ScanningStageCommunicationError = new(512, "Scanning stage failed to move: communication error", EventType.Error, cInspectionProcessName, cModuleName, cScanningStagePLCEvents, cScanningStageCommunicationError, $"Events/{cModuleName}/{nameof(ScanningStageCommunicationError)}");
        public static Event ScanningStagePositionError = new(513, "Scanning stage failed to move: position error", EventType.Error, cInspectionProcessName, cModuleName, cScanningStagePLCEvents, cScanningStagePositionError, $"Events/{cModuleName}/{nameof(ScanningStagePositionError)}");

        // Robot
        private const string cRobotPLCEvents = "instRobotTrayUnloaderRobotEvents";
        private const string cRobotHasError = "instRobotHasError";

        public static Event RobotHasError = new(514, "The robot program is in state error", EventType.Error, cRobotProcessName, cModuleName, cRobotPLCEvents, cRobotHasError, $"Events/{cModuleName}/{RobotHasError}");

        //Safety
        private const string cSafetyPLCEvents = "instRobotTrayUnloaderSafetyEvents";
        private const string cRobotTrayUnloaderFrontLeftCoverOpen = "robotTrayUnloaderFrontLeftCoverOpen";
        private const string cRobotTrayUnloaderFrontRightCoverOpen = "robotTrayUnloaderFrontRightCoverOpen";
        private const string cRobotTrayUnloaderRearLeftCoverOpen = "robotTrayUnloaderRearLeftCoverOpen";
        private const string cRobotTrayUnloaderRearRightCoverOpen = "robotTrayUnloaderRearRightCoverOpen";

        public static Event RobotTrayUnloaderFrontLeftCoverOpenInfo = new(515, "Robot tray unloader front left cover is open", EventType.Info, cSafetyName, cModuleName, cSafetyPLCEvents, cRobotTrayUnloaderFrontLeftCoverOpen, $"Events/{cModuleName}/{nameof(RobotTrayUnloaderFrontLeftCoverOpenInfo)}");
        public static Event RobotTrayUnloaderFrontRightCoverOpenInfo = new(516, "Robot tray unloader front right cover is open", EventType.Info, cSafetyName, cModuleName, cSafetyPLCEvents, cRobotTrayUnloaderFrontRightCoverOpen, $"Events/{cModuleName}/{nameof(RobotTrayUnloaderFrontRightCoverOpenInfo)}");
        public static Event RobotTrayUnloaderRearLeftCoverOpenInfo = new(517, "Robot tray unloader rear left cover is open", EventType.Info, cSafetyName, cModuleName, cSafetyPLCEvents, cRobotTrayUnloaderRearLeftCoverOpen, $"Events/{cModuleName}/{nameof(RobotTrayUnloaderRearLeftCoverOpenInfo)}");
        public static Event RobotTrayUnloaderRearRightCoverOpenInfo = new(518, "Robot tray unloader rear right cover is open", EventType.Info, cSafetyName, cModuleName, cSafetyPLCEvents, cRobotTrayUnloaderRearRightCoverOpen, $"Events/{cModuleName}/{nameof(RobotTrayUnloaderRearRightCoverOpenInfo)}");
        #endregion
    }
}
