﻿using MachineManager.Lib;
using MachineManager.OpcUa;
using System;
using System.Linq.Expressions;

namespace RobotToTrayUnloader.PLCInterfaces.OpcUa
{
    public class RobotToTrayUnloaderOpcUa
    {
        public static string GetPath<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return OpcUaPathCreator<ServerInterface>.GetPath(expression);
        }

        public static TagModel GetTagModel<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return new TagModel(ServerInterface.ModuleId, GetPath(expression));
        }
    }
}
