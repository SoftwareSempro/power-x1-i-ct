﻿
using MachineManager;

namespace RobotToTrayUnloader.PLCInterfaces.OpcUa
{
    public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceFor(nameof(RobotToTrayUnloader));
        public RobotToTrayUnloader RobotToTrayUnloader { get; set; }
    }
}
