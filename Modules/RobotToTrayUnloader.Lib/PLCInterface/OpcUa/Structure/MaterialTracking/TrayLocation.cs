﻿using MachineManager.Lib;

namespace RobotTrayUnloader.PLCInterface
{
    public class TrayLocation
    {
        [Name("[1]")]
        public MaterialTracking.Lib.TrayLocation Tray1 { get; set; }

        [Name("[2]")]
        public MaterialTracking.Lib.TrayLocation Tray2 { get; set; }

        [Name("[3]")]
        public MaterialTracking.Lib.TrayLocation Tray3 { get; set; }

        [Name("[4]")]
        public MaterialTracking.Lib.TrayLocation Tray4 { get; set; }

        [Name("[5]")]
        public MaterialTracking.Lib.TrayLocation Tray5 { get; set; }

        [Name("[6]")]
        public MaterialTracking.Lib.TrayLocation Tray6 { get; set; }


    }
}
