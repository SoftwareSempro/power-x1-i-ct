﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.Robot;
using PressModule.PLCInterfaces.ScanningStage;
using RobotToTrayUnloader.PLCInterface.CameraController;
using RobotToTrayUnloader.PLCInterface.Inspection;
using RobotToTrayUnloader.PLCInterface.Main;
using RobotToTrayUnloader.PLCInterface.Robot;
using RobotTrayUnloader.PLCInterface;
using RobotTrayUnloader.PLCInterfaces.TeachPlacePositions;

namespace RobotToTrayUnloader.PLCInterfaces.OpcUa
{
    public class RobotToTrayUnloader
    {
        [Name("robotToTrayUnloaderInterface")]
        public RobotToTrayUnloaderInterface RobotToTrayUnloaderInterface { get; set; }

        [Name("inspectionProcessInterface")]
        public InspectionProcessInterface InspectionProcessInterface { get; set; }

        [Name("robotProcessInterface")]
        public RobotProcessInterface RobotProcessInterface { get; set; }

        [Name("robotTrayUnloaderScanningStageInterface")]
        public RobotTrayUnloaderScanningStageInterface RobotTrayUnloaderScanningStageInterface { get; set; }

        [Name("robotTrayUnloaderRobotInterface")]
        public RobotTrayUnloaderRobotInterface RobotTrayUnloaderRobotInterface { get; set; }

        [Name("robotToTrayUnloader3dCameraControllerInterface")]
        public RobotTrayUnloader3dCameraControllerInterface RobotTrayUnloader3dCameraControllerInterface { get; set; }

        [Name("robotTrayUnloaderTeachPlacePositionsInterface")]
        public RobotTrayUnloaderTeachPlacePositionsInterface RobotTrayUnloaderTeachPlacePositionsInterface { get; set; }

        [Name("robotToTrayUnloaderMaterialTracking")]
        public RobotToTrayUnloaderMaterialTracking RobotToTrayUnloaderMaterialTracking { get; set; }

    }
}
