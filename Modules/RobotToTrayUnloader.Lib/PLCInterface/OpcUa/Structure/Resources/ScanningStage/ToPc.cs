﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.ScanningStage
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("actualPositionX")]
        public double ActualPositionX { get; set; }

        [Name("productAtScanningStage")]
        public bool ProductAtScanningStage { get; set; }
    }
}
