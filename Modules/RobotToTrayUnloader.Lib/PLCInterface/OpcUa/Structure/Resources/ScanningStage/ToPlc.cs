﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.ScanningStage
{
    public class ToPlc
    {

        [Name("goToCommand")]
        public bool GoToCommand { get; set; }

        [Name("setPointX")]
        public double SetPointX { get; set; }

        [Name("velocity")]
        public int Velocity { get; set; }
    }
}
