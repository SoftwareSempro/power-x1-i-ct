﻿using MachineManager.Lib;
using System;
using System.Collections.Generic;
namespace PressModule.PLCInterfaces.ScanningStage
{
    public class RobotTrayUnloaderScanningStageInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
