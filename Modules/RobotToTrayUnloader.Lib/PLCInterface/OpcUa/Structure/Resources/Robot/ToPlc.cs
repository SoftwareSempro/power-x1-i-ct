﻿using MachineManager.Lib;
using RobotToTrayUnloader.Lib.PLCInterface.OpcUa.Structure.Resources;

namespace PressModule.PLCInterfaces.Robot
{
    public class ToPlc
    {

        [Name("goToCommand")]
        public bool GoToCommand { get; set; }

        [Name("setpoint")]
        public Pose Setpoint { get; set; }

        [Name("velocity")]
        public int Velocity { get; set; }
        [Name("vacuumOnCommand")]
        public bool VacuumOnCommand { get; set; }
        [Name("vacuumOffCommand")]
        public bool VacuumOffCommand { get; set; }

        [Name("activateBlowOffCommand")]
        public bool ActivateBlowOffCommand { get; set; }
        [Name("deactivateBlowOffCommand")]
        public bool DeactivateBlowOffCommand { get; set; }

        
    }
}
