﻿using MachineManager.Lib;
using RobotToTrayUnloader.Lib.PLCInterface.OpcUa.Structure.Resources;

namespace PressModule.PLCInterfaces.Robot
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("actualPosition")]
        public Pose ActualPosition { get; set; }

        [Name("isVacuumOn")]
        public bool IsVacuumOn { get; set; }

        [Name("isPause")]
        public bool IsPause { get; set; }
    }
}
