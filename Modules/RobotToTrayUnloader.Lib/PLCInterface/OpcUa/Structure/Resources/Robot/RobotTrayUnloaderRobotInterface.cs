﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Robot
{
    public class RobotTrayUnloaderRobotInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
