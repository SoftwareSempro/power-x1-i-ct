﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.Lib.PLCInterface.OpcUa.Structure.Resources
{
    public class Pose
    {
        [Name("X")]
        public double X { get; set; }
        [Name("Y")]
        public double Y { get; set; }
        [Name("Z")]
        public double Z { get; set; }
        [Name("R")]
        public double R { get; set; }
    }
}
