using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.CameraController
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("result")]
        public int Result { get; set; }

        [Name("controllerReady")]
        public bool ControllerReady { get; set; }

        [Name("controllerBusy")]
        public bool ControllerBusy { get; set; }

        [Name("controllerIsRunning")]
        public bool ControllerIsRunning { get; set; }

        [Name("commandNumber")]
        public int CommandNumber { get; set; }
    }
}
