using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.CameraController
{
  public class RobotTrayUnloader3dCameraControllerInterface
    {
      [Name("toPc")]
      public ToPc ToPc { get; set; }

  }
}
