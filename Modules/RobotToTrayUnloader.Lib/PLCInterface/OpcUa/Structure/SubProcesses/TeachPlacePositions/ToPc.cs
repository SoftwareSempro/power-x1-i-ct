using MachineManager.Lib;

namespace RobotTrayUnloader.PLCInterfaces.TeachPlacePositions
{
  public class ToPc
  {
      [Name("status")]
      public int Status { get; set; }
  }
}
