using MachineManager.Lib;
using RobotToTrayUnloader.PLCInterface.Robot;

namespace RobotTrayUnloader.PLCInterfaces.TeachPlacePositions
{
  public class ToPlc
  {
        [Name("trayNumber")]
        public short TrayNumber { get; set; }
        [Name("column")]
        public short Column { get; set; }
        [Name("row")]
        public short Row { get; set; }

        [Name("placeMaterialCommand")]
        public bool PlaceMaterialCommand { get; set; }
        [Name("removeMaterialCommand")]
        public bool RemoveMaterialCommand { get; set; }
        [Name("pickCommand")]
        public bool PickCommand { get; set; }
        [Name("toPlaceCommand")]
        public bool ToPlaceCommand { get; set; }
        [Name("toHomeCommand")]
        public bool ToHomeCommand { get; set; }
  }
}
