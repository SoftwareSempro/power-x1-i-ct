using MachineManager.Lib;

namespace RobotTrayUnloader.PLCInterfaces.TeachPlacePositions
{
    public class RobotTrayUnloaderTeachPlacePositionsInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
