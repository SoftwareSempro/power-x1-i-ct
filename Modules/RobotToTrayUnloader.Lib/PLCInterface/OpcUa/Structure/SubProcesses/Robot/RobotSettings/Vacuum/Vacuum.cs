﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class Vacuum
    {
        [Name("productPresentVacuumDelay")]
        public int ProductPresentVacuumDelay { get; set; }

        [Name("productNotPresentVacuumDelay")]
        public int ProductNotPresentVacuumDelay { get; set; }
    }
}
