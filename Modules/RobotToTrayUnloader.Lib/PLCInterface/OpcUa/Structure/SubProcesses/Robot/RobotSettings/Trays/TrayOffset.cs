﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class TrayOffset
    {
        public double X { get; set; }
        public double Y { get; set; }
        
        [Name("rZ")]
        public double RZ { get; set; }
    }
}
