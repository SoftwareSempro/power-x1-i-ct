﻿namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class Position
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double R { get; set; }

    }
}
