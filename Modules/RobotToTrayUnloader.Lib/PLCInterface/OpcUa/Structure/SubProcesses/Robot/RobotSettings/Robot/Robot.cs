﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class Robot
    {
        [Name("pick")]
        public Pick Pick { get; set; }

        [Name("place")]
        public Place Place { get; set; }

        [Name("home")]
        public Home Home { get; set; }

        [Name("baseCount")]
        public int BaseCount { get; set; }

        [Name("defaultVelocityPercentage")]
        public int DefaultVelocityPercentage { get; set; }
    }
}
