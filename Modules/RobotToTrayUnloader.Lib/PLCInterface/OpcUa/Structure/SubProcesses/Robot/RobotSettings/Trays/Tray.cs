﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class Tray
    {
        [Name("trayType")]
        public int TrayType { get; set; }

        [Name("offset")]
        public TrayOffset TrayOffset { get; set; }
    }
}
