﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class Home
    {
        [Name("position")]
        public Position Position { get; set; }
    }
}
