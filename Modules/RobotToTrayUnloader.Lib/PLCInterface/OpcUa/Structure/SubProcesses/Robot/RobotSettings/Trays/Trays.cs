﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class Trays
    {
        [Name("[1]")]
        public Tray Tray1 { get; set; }

        [Name("[2]")]
        public Tray Tray2 { get; set; }

        [Name("[3]")]
        public Tray Tray3 { get; set; }

        [Name("[4]")]
        public Tray Tray4 { get; set; }

        [Name("[5]")]
        public Tray Tray5 { get; set; }

        [Name("[6]")]
        public Tray Tray6 { get; set; }
    }
}
