﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class Pick
    {
        [Name("position")]
        public Position Position { get; set; }

        [Name("pickOffsetZ")]
        public double PickOffSetZ {get; set;}

        [Name("velocityPercentage")]
        public VelocityPercentage VelocityPercentage { get; set; }
    }
}
