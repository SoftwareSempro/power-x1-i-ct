﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
