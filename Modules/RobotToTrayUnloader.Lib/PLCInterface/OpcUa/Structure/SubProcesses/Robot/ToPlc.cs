﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class ToPlc
    {
        [Name("settings")]
        public RobotSettings RobotSettings { get; set; }

        [Name("retryCommand")]
        public bool RetryCommand { get; set; }
    }
}
