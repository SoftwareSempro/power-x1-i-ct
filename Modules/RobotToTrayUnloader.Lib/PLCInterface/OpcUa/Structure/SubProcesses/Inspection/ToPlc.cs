﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class ToPlc
    {
        [Name("settings")]
        public Inspection Inspection { get; set; }

    }
}
