﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
