﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class Inspection
    {        
            [Name("infeedPosition")]
            public double InfeedPosition { get; set; }

            [Name("outfeedPosition")]
            public double OutfeedPosition { get; set; }

            [Name("toInspectionVelocityPositionOffset")]
            public double ToInspectionVelocityPositionOffset { get; set; }

            [Name("startInspectionPosition")]
            public double StartInspectionPosition { get; set; }

            [Name("stopInspectionPosition")]
            public double StopInspectionPosition { get; set; }

            [Name("velocity")]
            public int Velocity { get; set; }

            [Name("inspectionVelocity")]
            public int InspectionVelocity { get; set; }

            [Name("noAOI")]
            public bool NoAoi { get; set; }

            [Name("commandNumber")]
            public int CommandNumber { get; set; }

    }
}
