﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class InspectionProcessInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
