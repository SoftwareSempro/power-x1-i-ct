using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class NoEmptyPocketFoundAssist : PageModel
    {
        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;

        public void OnPostRetry()
        {
            mRobotToTrayUnloader.SubProcesses.RobotProcess.ExecuteRetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
        }
    }
}
