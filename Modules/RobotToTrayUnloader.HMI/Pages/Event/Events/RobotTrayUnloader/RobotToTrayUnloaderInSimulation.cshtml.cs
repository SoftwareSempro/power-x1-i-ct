using Microsoft.AspNetCore.Mvc.RazorPages;
using Platform.Lib;

namespace RobotTrayUnloader.HMI.Pages.Event.Events
{
    public class RobotToTrayUnloaderInSimulation : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl;
        private readonly RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader;        

        public RobotToTrayUnloaderInSimulation(LineControl.Application.LineControl lineControl, RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader)
        {
            mLineControl = lineControl;
            mRobotToTrayUnloader = robotToTrayUnloader;
        }

        public void OnPostDisableSimulation()
        {
            mRobotToTrayUnloader.DisableSimulation();
        }

        public void OnPostStop()
        {
            if (mLineControl.State != (int)State.Stopped)
            {
                mLineControl.Stop();
                mRobotToTrayUnloader.Stop();
            }
            else if (mRobotToTrayUnloader.State != (int)State.Stopped)
            {
                mRobotToTrayUnloader.Stop();
            }
            else
            {
                mLineControl.Unlock();
            }
        }
    }
}
