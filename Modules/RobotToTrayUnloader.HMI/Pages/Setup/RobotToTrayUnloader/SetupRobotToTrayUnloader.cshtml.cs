using Authorization.Lib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Platform.HMI.Resources;
using Platform.Lib;
using Power_X1_iCT.Model;
using RecipeManager;
using RobotToTrayUnloader.Updater;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]
    [Authorize(Policy = SetupPolicy.Name)]
    public class SetupRobotToTrayUnloaderModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly RobotToTrayUnloaderUpdater mUpdater;

        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader;

        public RobotToTrayUnloaderSettingsModel RobotToTrayUnloaderSettingsModel { get; set; }
        public InspectionSettingsModel InspectionSettingsModel { get; set; } = new();
        public bool SimulationEnabled { get; set; }
        public bool IsRunningProduction { get; set; }
        public RobotSettingsModel RobotSettingsModel { get; set; } = new();
        public RobotPickSettingsModel RobotPickSettingsModel { get; set; } = new();
        public RobotPlaceSettingsModel RobotPlaceSettingsModel { get; set; } = new();
        public RobotHomeSettingsModel RobotHomeSettingsModel { get; set; } = new();

        public SetupRobotToTrayUnloaderModel(RobotToTrayUnloaderUpdater robotToTrayUnloaderUpdater, ILogger<SetupRobotToTrayUnloaderModel> logger,
            RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader, IRecipeManager recipeManager)
        {
            mLog = logger;
            mUpdater = robotToTrayUnloaderUpdater;
            mRobotToTrayUnloader = robotToTrayUnloader;
            RobotToTrayUnloaderSettingsModel = new RobotToTrayUnloaderSettingsModel(mRobotToTrayUnloader);
            SimulationEnabled = mRobotToTrayUnloader.InSimulation;
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((State)mRobotToTrayUnloader.State).ToString() });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mRobotToTrayUnloader.InSimulation);
        }
        public JsonResult OnPostCanProcessCommands()
        {
            var canProcess = !mRobotToTrayUnloader.IsRunningProduction &&
                mRobotToTrayUnloader.State == (int)State.Running &&
                mRobotToTrayUnloader.Status != (int)Status.Busy;
            return new JsonResult(canProcess);
        }

        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mRobotToTrayUnloader.Initialize();
        }

        public JsonResult OnPostCanStart()
        {
            return new JsonResult(mRobotToTrayUnloader.CanStart());
        }

        public void OnPostStart()
        {
            mLog.LogInformation("OnStart");
            mRobotToTrayUnloader.Start();
        }
        public JsonResult OnPostCanStop()
        {
            return new JsonResult(mRobotToTrayUnloader.CanStop());
        }

        public void OnPostStop()
        {
            mLog.LogInformation("OnStop");
            mRobotToTrayUnloader.Stop();
        }

        public void OnPostInspectionCommand()
        {
            mLog.LogInformation("OnInspection");
            mRobotToTrayUnloader.Inspect();
        }

        public void OnPostToInfeedCommand()
        {
            mLog.LogInformation("OnInfeed");
            mRobotToTrayUnloader.InspectToInfeed();
        }

        public void OnPostRobotPickCommand()
        {
            mLog.LogInformation("OnPick");
            mRobotToTrayUnloader.Pick();
        }
        public void OnPostRobotPlaceCommand()
        {
            mLog.LogInformation("OnPick");
            mRobotToTrayUnloader.Place();
        }
        public void OnPostRobotHomeCommand()
        {
            mLog.LogInformation("OnHome");
            mRobotToTrayUnloader.Home();
        }

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadInspectionSettingsToPLC(InspectionSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveInspectionSettings(InspectionSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshInspectionSettingsFromRecipe(InspectionSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadRobotSettingsToPLC(RobotSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveRobotSettings(RobotSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshRobotSettingsFromRecipe(RobotSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadRobotPickSettingsToPLC(RobotPickSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveRobotPickSettings(RobotPickSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshRobotPickSettingsFromRecipe(RobotPickSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadRobotPlaceSettingsToPLC(RobotPlaceSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveRobotPlaceSettings(RobotPlaceSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshRobotPlaceSettingsFromRecipe(RobotPlaceSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadRobotHomeSettingsToPLC(RobotHomeSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveRobotHomeSettings(RobotHomeSettingsModel model) => this.CallActionIfAuthorized(() => model.Save());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshRobotHomeSettingsFromRecipe(RobotHomeSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        #region Resources

        #region 3dCameraController

        public JsonResult OnPostGet3dCameraControllerSensorValues()
        {
            bool[] sensorStatusses = new bool[3];
            sensorStatusses[0] = mRobotToTrayUnloader.Resources.Camera3dController.ControllerReady;
            sensorStatusses[1] = mRobotToTrayUnloader.Resources.Camera3dController.ControllerBusy;
            sensorStatusses[2] = mRobotToTrayUnloader.Resources.Camera3dController.ControllerIsRunning;

            return new JsonResult(sensorStatusses);
        }
        public JsonResult OnPostGet3dCameraControllerResults()
        {
            int[] results = new int[2];
            results[0] = mRobotToTrayUnloader.Resources.Camera3dController.Result;
            results[1] = mRobotToTrayUnloader.Resources.Camera3dController.CommandNumber;

            return new JsonResult(results);
        }

        #endregion

        #region ScanningStage
        public void OnPostScanningStageGoToX(string input1, string input2)
        {
            mRobotToTrayUnloader.Resources.ScanningStage.GoTo(Convert.ToDouble(input1), Convert.ToInt32(input2));
        }

        public JsonResult OnPostGetScanningStageActualPositionX()
        {
            return new JsonResult(mRobotToTrayUnloader.Resources.ScanningStage.ActualPositionX);
        }

        public JsonResult OnPostGetProductAtScanningStage()
        {
            bool status = new bool();
            status = mRobotToTrayUnloader.Resources.ScanningStage.ProductAtScanningStage;

            return new JsonResult(status);
        }
        #endregion

        #region Robot
        public void OnPostRobotGoTo(string input1, string input2, string input3, string input4, string input5)
        {
            mRobotToTrayUnloader.Resources.Robot.GoTo(Convert.ToDouble(input1), Convert.ToDouble(input2), Convert.ToDouble(input3), Convert.ToDouble(input4), Convert.ToInt32(input5));
        }

        public void OnPostVacuumOffCommand()
        {
            mRobotToTrayUnloader.Resources.Robot.VacuumOffCommand();
        }

        public void OnPostVacuumOnCommand()
        {
            mRobotToTrayUnloader.Resources.Robot.VacuumOnCommand();
        }

        public void OnPostActivateBlowOff()
        {
            mRobotToTrayUnloader.Resources.Robot.ActivateBlowOff();
        }

        public void OnPostDeactivateBlowOff()
        {
            mRobotToTrayUnloader.Resources.Robot.DeactivateBlowOff();
        }

        public JsonResult OnPostGetRobotActualPosition()
        {
            List<double> positions = new List<double>()
            {
                mRobotToTrayUnloader.Resources.Robot.ActualPositionX,
                mRobotToTrayUnloader.Resources.Robot.ActualPositionY,
                mRobotToTrayUnloader.Resources.Robot.ActualPositionZ,
                mRobotToTrayUnloader.Resources.Robot.ActualPositionR
            };

            return new JsonResult(positions);
        }

        public JsonResult OnPostGetRobotHasVacuum()  
        {
            return new JsonResult(mRobotToTrayUnloader.Resources.Robot.IsVacuumOn);
        }

        public JsonResult OnPostGetRobotSensorValues()  
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mRobotToTrayUnloader.Resources.Robot.IsVacuumOn;
            sensorStatusses[1] = mRobotToTrayUnloader.Resources.Robot.IsPause;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #endregion

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mRobotToTrayUnloader.DisableSimulation();
            }
            else
            {
                mRobotToTrayUnloader.EnableSimulation();
            }
        }
        public void OnPostSetVelocityPercentage(int input)
        {
            RobotToTrayUnloaderSettingsModel.SetVelocityPercentage(input, mRobotToTrayUnloader);
        }
    }
}
