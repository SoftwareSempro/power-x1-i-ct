using Assistants.Lib;
using LineControl;
using Localization;
using log4net;
using Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using Platform.HMI.Assistants;
using Platform.HMI.Pages.Assistants;
using Platform.HMI.Pages.Assistants.HelperClasses;
using Platform.HMI.Updaters;
using Platform.Lib;
using Power_X1_iCT.Model;
using RecipeManager;
using RobotToTrayUnloader.Updater;
using RobotTrayUnloader.Pages.Assistants.TeachPlacePositions.Pages;

namespace RobotTrayUnloader.Pages.Assistants
{
    [BindProperties]
    public class TeachPlacePositionsModel : PageModel
    {
        private AssistantUpdater mUpdater;
        private LineControl.Application.LineControl mLineControl;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mModule;
        private IRecipeManager mRecipeManager;
        private IStringLocalizer<TeachPlacePositionsModel> mLocalizer;
        private IStringLocalizer<BaseAssistantModel> mTeachPlacePositionsLocalizer;
        private ILog mLog = LogClient.Get();
        private readonly AssistantModelHelper mAssistantHelper;

        public RobotSettingsModel RobotSettingsModel { get; set; } = new();

        public string Title => "Teach robot place positions";

        public List<IAssistantPage> Pages => GetAssistantPages();

        public BaseAssistantModel PageModel => mAssistantHelper.PageModel;

        public TeachPlacePositionsModel(IStringLocalizer<TeachPlacePositionsModel> localizer,
            IStringLocalizer<BaseAssistantModel> baseLocalizer,
            IStringLocalizer<SharedResource> sharedLocalizer,
            AssistantUpdater updater,
            LineControlUpdater lineUpdater,
            RobotToTrayUnloaderUpdater moduleUpdater,
            RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader, LineControl.Application.LineControl lineControl,
            IRecipeManager recipeManager)
        {
            mLocalizer = localizer;
            mTeachPlacePositionsLocalizer = baseLocalizer;
            mLineControl = lineControl;
            mModule = robotToTrayUnloader;
            mUpdater = updater;
            mRecipeManager = recipeManager;
            mAssistantHelper = new AssistantModelHelper(sharedLocalizer, updater);
        }

        private List<IAssistantPage> GetAssistantPages()
        {
            var output = new List<IAssistantPage>
            {
                new StartTeachPlacePositions(mTeachPlacePositionsLocalizer),
                new ProductionFinished(mTeachPlacePositionsLocalizer),
                new PlaceMaterial(mTeachPlacePositionsLocalizer, mLineControl, mModule),
                new CloseCovers(mTeachPlacePositionsLocalizer, mLineControl),
                new TeachPlacePositions.Pages.TeachPlacePositions(mTeachPlacePositionsLocalizer, mLineControl, mModule),
                new RemoveMaterial(mTeachPlacePositionsLocalizer, mLineControl, mModule),
            };

            return output;
        }

        public JsonResult OnPostNextCommand(int currentPageIndex)
        {
            return mAssistantHelper.OnPostNextCommand(currentPageIndex, GetAssistantPages());
        }

        public JsonResult OnPostCanNextCommand(int currentPageIndex)
        {
            return mAssistantHelper.OnPostCanNextCommand(currentPageIndex, GetAssistantPages());
        }

        public JsonResult OnPostCanPreviousCommand(int currentPageIndex)
        {
            return mAssistantHelper.OnPostCanPreviousCommand(currentPageIndex, GetAssistantPages());
        }
        public JsonResult OnPostPreviousCommand(int currentPageIndex)
        {
            return mAssistantHelper.OnPostPreviousCommand(currentPageIndex, GetAssistantPages());
        }

        public void OnPostOnEnterCommand(int currentPageIndex)
        {
            mAssistantHelper.OnPostOnEnterCommand(currentPageIndex, GetAssistantPages());
        }

        public JsonResult OnPostRestartCommand()
        {
            //Reset the PLC to the init state
            mLineControl.ReInitPLC();

            return mAssistantHelper.OnPostRestartCommand(GetAssistantPages());
        }

        public IActionResult OnPostStopCommand()
        {
            mLog.Info("OnPostStopCommand");
            mModule.Stop();
            //When leave -> always stop de equipment command
            mLineControl.Stop();

            return StatusCode(200);
        }

        public IActionResult OnPostGetPartialView(int currentPageIndex)
        {
            mLog.Debug($"OnPostGetPartialView: {currentPageIndex}");

            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());
            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case PlaceMaterial page:
                            return Partial("Pages/_PlaceMaterialContentPartial", this);
                        case TeachPlacePositions.Pages.TeachPlacePositions page:
                            return Partial("Pages/_TeachPlacePositionsContentPartial", this);
                        case RemoveMaterial page:
                            return Partial("Pages/_RemoveMaterialContentPartial", this);
                        default:
                            return Partial(AssistantModelHelper.DefaultContentPartial, this);
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }

            return Page();
        }

        public IActionResult OnPostMaterialPlacedCommand(int currentPageIndex)
        {
            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());
            mUpdater.UpdatePage(PageModel.CurrentPage);
            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case PlaceMaterial page:
                            page.PlaceMaterialOnInfeed();
                            break;
                        default:
                            return NotFound();
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }
            mUpdater.UnSubscribe();

            return Page();
        }

        public IActionResult OnPostMaterialRemovedCommand(int currentPageIndex)
        {
            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());
            mUpdater.UpdatePage(PageModel.CurrentPage);
            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case RemoveMaterial page:
                            page.RemoveMaterialFromInfeed();
                            break;
                        default:
                            return NotFound();
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }
            mUpdater.UnSubscribe();

            return Page();
        }

        public IActionResult OnPostToTrayCommand(int currentPageIndex, int trayNumber, int row, int column)
        {
            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());

            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case TeachPlacePositions.Pages.TeachPlacePositions page:
                            page.ToTray(trayNumber, row, column);
                            break;
                        default:
                            return NotFound();
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }

            return Page();
        }

        public IActionResult OnPostPickupCommand(int currentPageIndex)
        {
            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());
            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case TeachPlacePositions.Pages.TeachPlacePositions page:
                            page.Pickup();
                            break;
                        default:
                            return NotFound();
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }

            return Page();
        }

        public IActionResult OnPostToHomeCommand(int currentPageIndex)
        {
            mAssistantHelper.SetPageModel(currentPageIndex, GetAssistantPages());
            try
            {
                if (Pages.Count > currentPageIndex)
                {
                    switch (PageModel.CurrentPage)
                    {
                        case TeachPlacePositions.Pages.TeachPlacePositions page:
                            page.ToHome();
                            break;
                        default:
                            return NotFound();
                    }
                }
                else
                {
                    mLog.Error("Page is not available");
                    return NotFound();
                }

            }
            catch (Exception e)
            {
                mLog.Error("On enter failed: " + e);
            }

            return Page();
        }

        public IActionResult OnPostInitialize()
        {
            mModule.Initialize();

            return StatusCode(200);
        }

        public JsonResult OnPostCanStart()
        {
            return new JsonResult(mModule.CanStart());
        }

        public JsonResult OnPostCanStop()
        {
            return new JsonResult(mModule.CanStop());
        }

        public IActionResult OnPostStop()
        {
            mModule.Stop();

            return StatusCode(200);
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((State)mModule.State).ToString() });
        }

        public JsonResult OnPostGetStatus()
        {
            return new JsonResult(new { status = ((Status)mModule.SubProcesses.RobotProcess.Status).ToString() });
        }

        public void OnPostDownloadRobotSettingsToPLC(RobotSettingsModel model) => model.Download();
        public void OnPostSaveRobotSettings(RobotSettingsModel model) => model.Save(User?.Identity?.Name ?? "N/A");
        public IActionResult OnPostRefreshRobotSettingsFromRecipe(RobotSettingsModel model, int revision)
        {
            model.RefreshFromRecipe(revision, User?.Identity?.Name ?? "N/A");
            return new JsonResult(new { model });
        }
        public IActionResult OnPostGetCurrentRecipeRevision()
        {
            var revision = mRecipeManager.GetCurrentRecipe().Revision;

            return new JsonResult(new { revision });
        }
    }
}
