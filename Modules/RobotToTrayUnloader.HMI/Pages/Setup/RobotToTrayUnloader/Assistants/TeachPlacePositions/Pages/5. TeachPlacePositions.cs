﻿using Assistants.Lib;
using iText.StyledXmlParser.Jsoup.Nodes;
using Microsoft.Extensions.Localization;
using Platform.HMI.Assistants;
using Platform.HMI.Pages.Assistants.HelperClasses;
using Platform.Lib;
using RecipeManager;
using System.Diagnostics;

namespace RobotTrayUnloader.Pages.Assistants.TeachPlacePositions.Pages
{
    public class TeachPlacePositions: BaseAssistantPage<BaseAssistantModel>
    {
        private LineControl.Application.LineControl mLineControl;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mModule;

        public override string MessageText => mLocalizer.GetString("Teach the place positions");
        public override string PreviousCaption => mLocalizer.GetString(AssistCaption.Cancel.ToString());
        public override string NextCaption => mLocalizer.GetString(AssistCaption.Next.ToString());

        public TeachPlacePositions(IStringLocalizer<BaseAssistantModel> localizer, LineControl.Application.LineControl lineControl, RobotToTrayUnloader.Application.RobotToTrayUnloader module)
            : base(localizer)
        {
            mLineControl = lineControl;
            mModule = module;
        }

        public override bool CanNext()
        {
            return (mModule.Status == (int)Status.Ready);// || Debugger.IsAttached;
        }

        public override void OnEnter()
        {
            base.OnEnter();

            if (!Debugger.IsAttached)
            {
                mLineControl.Lock();
                mModule.Initialize();
            }
        }

        public override void OnLeave()
        {
            base.OnLeave();
        }

        internal void ToTray(int trayNumber, int row, int column)
        {
            mLog.Debug($"ToTray: {trayNumber}, {row}, {column}");

            var dataModel = new Application.SubProcesses.TeachPlacePositionsDataModel
            {
                TrayNumber = (short)trayNumber,
                Row = (short)row,
                Column = (short)column
            };
            Application.SubProcesses.TeachPlacePositions.ToPlaceCommand(dataModel);
        }

        internal void ToHome()
        {
            Application.SubProcesses.TeachPlacePositions.ToHomeCommand();
        }

        internal void Pickup()
        {
            Application.SubProcesses.TeachPlacePositions.PickCommand();
        }
    }
}
