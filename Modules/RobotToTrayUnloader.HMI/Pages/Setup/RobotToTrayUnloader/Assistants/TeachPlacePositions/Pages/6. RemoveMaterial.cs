﻿using Assistants.Lib;
using Microsoft.Extensions.Localization;
using Platform.HMI.Assistants;
using Platform.HMI.Pages.Assistants.HelperClasses;
using System.Diagnostics;

namespace RobotTrayUnloader.Pages.Assistants.TeachPlacePositions.Pages
{
    public class RemoveMaterial: BaseAssistantPage<BaseAssistantModel>
    {
        private LineControl.Application.LineControl mLineControl;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mModule;
        public override string MessageText => mLocalizer.GetString("Remove material from the robot or infeed location");
        public override string PreviousCaption => mLocalizer.GetString(AssistCaption.Previous.ToString());
        public override string NextCaption => mLocalizer.GetString(AssistCaption.Finish.ToString());

        public RemoveMaterial(IStringLocalizer<BaseAssistantModel> localizer, LineControl.Application.LineControl lineControl, RobotToTrayUnloader.Application.RobotToTrayUnloader module)
            : base(localizer)
        {
            mLineControl = lineControl;
            mModule = module;
        }

        public override bool CanNext()
        {
            return (!mModule.MaterialTracking.InfeedHasMaterial && !mModule.MaterialTracking.RobotHasMaterial);
        }

        public override void OnEnter()
        {
            base.OnEnter();

            mModule.Stop();
            mLineControl.Unlock();
        }

        public void RemoveMaterialFromInfeed()
        {
            Application.SubProcesses.TeachPlacePositions.RemoveMaterialCommand();

            if (Debugger.IsAttached)
            {
                CanNextUpdated?.Invoke(this, true);
            }
        }

    }
}
