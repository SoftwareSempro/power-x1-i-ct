﻿using Assistants.Lib;
using Microsoft.Extensions.Localization;
using Platform.HMI.Assistants;
using Platform.HMI.Pages.Assistants.HelperClasses;
using System.Diagnostics;

namespace RobotTrayUnloader.Pages.Assistants.TeachPlacePositions.Pages
{
    public class PlaceMaterial: BaseAssistantPage<BaseAssistantModel>
    {
        private LineControl.Application.LineControl mLineControl;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mModule;
        public override string MessageText => mLocalizer.GetString("Place material on robot pick position");
        public override string PreviousCaption => mLocalizer.GetString(AssistCaption.Cancel.ToString());
        public override string NextCaption => mLocalizer.GetString(AssistCaption.Yes.ToString());

        public PlaceMaterial(IStringLocalizer<BaseAssistantModel> localizer, LineControl.Application.LineControl lineControl, RobotToTrayUnloader.Application.RobotToTrayUnloader module)
            : base(localizer)
        {
            mLineControl = lineControl;
            mModule = module;
        }

        public override bool CanNext()
        {
            return mModule.MaterialTracking.InfeedHasMaterial;
        }

        public override void OnEnter()
        {
            mLog.Debug($"PlaceMaterial: OnEnter");

            base.OnEnter();

            mLineControl.Unlock();
        }

        public override void OnLeave()
        {
            mLog.Debug($"PlaceMaterial: OnLeave");
            base.OnLeave();
        }

        public void PlaceMaterialOnInfeed()
        {
            mLog.Debug("> PlaceMaterialOnInfeed");

            if (mModule.MaterialTracking.InfeedHasMaterial)
            {
                //When it already has meterial
                CanNextUpdated?.Invoke(this, true);
                return;
            }

            Application.SubProcesses.TeachPlacePositions.PlaceMaterialCommand();
            if (Debugger.IsAttached)
            {
                CanNextUpdated?.Invoke(this, true);
            }
            mLog.Debug("PlaceMaterialOnInfeed >");
        }
    }
}
