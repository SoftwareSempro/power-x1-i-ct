﻿using Assistants.Lib;
using Microsoft.Extensions.Localization;
using Platform.HMI.Assistants;
using Platform.HMI.Pages.Assistants.HelperClasses;

namespace RobotTrayUnloader.Pages.Assistants.TeachPlacePositions.Pages
{
    public class StartTeachPlacePositions: BaseAssistantPage<BaseAssistantModel>
    {
        public override string MessageText => mLocalizer.GetString("Are you sure you want to teach the robot place positions?");
        public override string ImageSource => "/_content/Platform.HMI/img/help.png";
        public override string PreviousCaption => mLocalizer.GetString(AssistCaption.Cancel.ToString());
        public override string NextCaption => mLocalizer.GetString(AssistCaption.Yes.ToString());

        public StartTeachPlacePositions(IStringLocalizer<BaseAssistantModel> localizer)
            : base(localizer)
        { }            
    }
}
