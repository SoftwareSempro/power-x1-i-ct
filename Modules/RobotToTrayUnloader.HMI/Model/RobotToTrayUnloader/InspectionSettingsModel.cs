﻿using Recipe.Lib;
using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;
using RobotToTrayUnloader.PLCInterface.Inspection;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Model
{
    public class InspectionSettingsModel
    {
        private RobotToTrayUnloader.Application.SubProcesses.Inspection mInspection = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Inspection;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double InfeedPosition { get; set; }
        public double OutfeedPosition { get; set; }
        public double ToInspectionVelocityPositionOffset { get; set; }
        public double StartInspectionPosition { get; set; }
        public double StopInspectionPosition { get; set; }
        public int Velocity { get; set; }
        public int InspectionVelocity { get; set; }
        public bool NoAOI { get; set; }
        public int InspectionCommandNumber { get; set; }

        public InspectionSettingsModel()
        {
            Inspection_SettingsChanged(this, mInspection.Settings);
            mInspection.SettingsChanged += Inspection_SettingsChanged;
        }

        private void Inspection_SettingsChanged(object sender, InspectionDataModel e)
        {
            InfeedPosition = e.InfeedPosition;
            OutfeedPosition = e.OutfeedPosition;
            ToInspectionVelocityPositionOffset = e.ToInspectionVelocityPositionOffset;
            StartInspectionPosition = e.StartInspectionPosition;
            StopInspectionPosition = e.StopInspectionPosition;
            Velocity = e.Velocity;
            InspectionVelocity = e.InspectionVelocity;
            NoAOI = e.NoAOI;
            InspectionCommandNumber = e.InspectionCommandNumber;
        }
        public void Save(string author)
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.Inspection.InfeedPosition = InfeedPosition;
            productRecipe.Inspection.OutfeedPosition = OutfeedPosition;
            productRecipe.Inspection.ToInspectionVelocityPositionOffset = ToInspectionVelocityPositionOffset;
            productRecipe.Inspection.StartInspectionPosition = StartInspectionPosition;
            productRecipe.Inspection.StopInspectionPosition = StopInspectionPosition;
            productRecipe.Inspection.Velocity = Velocity;
            productRecipe.Inspection.InspectionVelocity = InspectionVelocity;
            productRecipe.Inspection.NoAOI = NoAOI;
            productRecipe.Inspection.InspectionCommandNumber = InspectionCommandNumber;
            mRecipeManager.SaveRecipe(productRecipe, author);

            Download();

        }
        public void Download()
        {
            var dataModel = new RobotToTrayUnloader.Application.SubProcesses.InspectionDataModel();
            dataModel.InfeedPosition = InfeedPosition;
            dataModel.OutfeedPosition = OutfeedPosition;
            dataModel.ToInspectionVelocityPositionOffset = ToInspectionVelocityPositionOffset;
            dataModel.StartInspectionPosition = StartInspectionPosition;
            dataModel.StopInspectionPosition = StopInspectionPosition;
            dataModel.Velocity = Velocity;
            dataModel.InspectionVelocity = InspectionVelocity;
            dataModel.NoAOI = NoAOI;
            dataModel.InspectionCommandNumber= InspectionCommandNumber;

            mInspection.UpdateSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            IProductRecipe productRecipe = mRecipeManager.GetCurrentRecipe();
            var subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.Product.Inspection>(productRecipe);

            InfeedPosition = subRecipe.InfeedPosition;
            OutfeedPosition = subRecipe.OutfeedPosition;
            ToInspectionVelocityPositionOffset = subRecipe.ToInspectionVelocityPositionOffset;
            StartInspectionPosition = subRecipe.StartInspectionPosition;
            StopInspectionPosition = subRecipe.StopInspectionPosition;
            Velocity = subRecipe.Velocity;
            InspectionVelocity = subRecipe.InspectionVelocity;
            NoAOI = subRecipe.NoAOI;
            InspectionCommandNumber = subRecipe.InspectionCommandNumber;

            Download();
        }
    }
}