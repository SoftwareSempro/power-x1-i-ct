﻿using Recipe.Lib;
using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class RobotPlaceSettingsModel
    {
        private RobotToTrayUnloader.Application.SubProcesses.RobotProcess mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.RobotProcess;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public double PlaceOffsetZ { get; set; }        
        public int PlaceApproachVelocityPercentage { get; set; }
        public int PlaceClearVelocityPercentage { get; set; }        

        public RobotPlaceSettingsModel()
        {
            RobotPlace_SettingChanged(this, mRobot.SettingsPlace);
            mRobot.SettingsPlaceChanged += RobotPlace_SettingChanged;
        }

        public void RobotPlace_SettingChanged(object send, RobotPlaceDataModel e)
        {     
            PlaceOffsetZ = e.PlaceOffsetZ;            
            PlaceApproachVelocityPercentage = e.PlaceVelocityPercentage.ApproachVelocityPercentage;
            PlaceClearVelocityPercentage = e.PlaceVelocityPercentage.ClearVelocityPercentage;
        }

        public void Save(string author)
        {

            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.RobotProcess.Place.PlaceOffsetZ = PlaceOffsetZ;
            productRecipe.RobotProcess.Place.VelocityPercentage.ApproachVelocityPercentage = PlaceApproachVelocityPercentage;
            productRecipe.RobotProcess.Place.VelocityPercentage.ClearVelocityPercentage = PlaceClearVelocityPercentage;
            mRecipeManager.SaveRecipe(productRecipe, author);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotPlaceDataModel();
            dataModel.PlaceOffsetZ = PlaceOffsetZ;            
            dataModel.PlaceVelocityPercentage.ApproachVelocityPercentage = PlaceApproachVelocityPercentage;
            dataModel.PlaceVelocityPercentage.ClearVelocityPercentage = PlaceClearVelocityPercentage;

            mRobot.UpdateRobotPlaceSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            IProductRecipe productRecipe = mRecipeManager.GetCurrentRecipe();
            var subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.RobotProcess>(productRecipe);

            PlaceOffsetZ = subRecipe.Place.PlaceOffsetZ;
            PlaceApproachVelocityPercentage = subRecipe.Place.VelocityPercentage.ApproachVelocityPercentage;
            PlaceClearVelocityPercentage = subRecipe.Place.VelocityPercentage.ClearVelocityPercentage;

            Download();
        }
    }
}
