﻿using Recipe.Lib;
using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Model
{
    public class RobotPickSettingsModel
    {
        private RobotToTrayUnloader.Application.SubProcesses.RobotProcess mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.RobotProcess;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double PickPositionX { get; set; }
        public double PickPositionY { get; set; }
        public double PickPositionZ { get; set; }
        public double PickPositionR { get; set; }
        public double PickOffsetZ { get; set; }
        public int PickApproachVelocityPercentage { get; set; }
        public int PickClearVelocityPercentage { get; set; }
        

        public RobotPickSettingsModel()
        {
            RobotPick_settingsChanged(this, mRobot.SettingsPick);
            mRobot.SettingsPickChanged += RobotPick_settingsChanged;
        }

        private void RobotPick_settingsChanged(object sender, RobotPickDataModel e)
        {
            PickPositionX = e.PickPosition.X;
            PickPositionY = e.PickPosition.Y;
            PickPositionZ = e.PickPosition.Z;
            PickPositionR = e.PickPosition.R;
            PickOffsetZ = e.PickOffsetZ;
            PickApproachVelocityPercentage = e.PickVelocityPercentage.ApproachVelocityPercentage;
            PickClearVelocityPercentage = e.PickVelocityPercentage.ClearVelocityPercentage;
        }

        public void Save(string author)
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.RobotProcess.Pick.Position.X = PickPositionX;
            productRecipe.RobotProcess.Pick.Position.Y = PickPositionY;
            productRecipe.RobotProcess.Pick.Position.Z = PickPositionZ;
            productRecipe.RobotProcess.Pick.Position.R = PickPositionR;            
            productRecipe.RobotProcess.Pick.PickOffsetZ = PickOffsetZ;
            productRecipe.RobotProcess.Pick.VelocityPercentage.ApproachVelocityPercentage = PickApproachVelocityPercentage;
            productRecipe.RobotProcess.Pick.VelocityPercentage.ClearVelocityPercentage = PickClearVelocityPercentage;
            mRecipeManager.SaveRecipe(productRecipe, author);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotPickDataModel();
            dataModel.PickPosition.X = PickPositionX;
            dataModel.PickPosition.Y = PickPositionY;
            dataModel.PickPosition.Z = PickPositionZ;
            dataModel.PickPosition.R = PickPositionR;
            dataModel.PickOffsetZ = PickOffsetZ;
            dataModel.PickVelocityPercentage.ApproachVelocityPercentage = PickApproachVelocityPercentage;
            dataModel.PickVelocityPercentage.ClearVelocityPercentage = PickClearVelocityPercentage;           

            mRobot.UpdateRobotPickSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            IProductRecipe productRecipe = mRecipeManager.GetCurrentRecipe();
            var subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.RobotProcess>(productRecipe);

            PickPositionX = subRecipe.Pick.Position.X;
            PickPositionY = subRecipe.Pick.Position.X;
            PickPositionZ = subRecipe.Pick.Position.X;
            PickPositionR = subRecipe.Pick.Position.X;
            PickOffsetZ = subRecipe.Pick.Position.X;
            PickApproachVelocityPercentage = subRecipe.Pick.VelocityPercentage.ApproachVelocityPercentage;
            PickClearVelocityPercentage = subRecipe.Pick.VelocityPercentage.ClearVelocityPercentage;

            Download();
        }

    }
}
