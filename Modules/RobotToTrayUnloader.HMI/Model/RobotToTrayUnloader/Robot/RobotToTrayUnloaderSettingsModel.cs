﻿using RobotTrayUnloader.Application;

namespace Power_X1_iCT.Model
{
    public class RobotToTrayUnloaderSettingsModel
    {
        public int VelocityPercentage { get; set; }

        public RobotToTrayUnloaderSettingsModel(RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader)
        {
            VelocityPercentage = robotToTrayUnloader.Settings.VelocityPercentage;
        }

        public RobotToTrayUnloaderSettingsModel()
        {
            
        }

        public void SetVelocityPercentage(int percentage, RobotToTrayUnloader.Application.RobotToTrayUnloader robotToTrayUnloader)
        {
            robotToTrayUnloader.SetVelocityPercentage(percentage);
        }
    }
}
