﻿using iText.StyledXmlParser.Jsoup.Nodes;
using Recipe.Lib;
using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class RobotSettingsModel
    {
        private RobotToTrayUnloader.Application.SubProcesses.RobotProcess mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.RobotProcess;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public int BaseCount { get; set; }
        public int DefaultVelocityPercentage { get; set; }

        public double Tray1TrayOffsetX { get; set; }
        public double Tray1TrayOffsetY { get; set; }
        public double Tray1TrayOffsetRZ { get; set; }

        public double Tray2TrayOffsetX { get; set; }
        public double Tray2TrayOffsetY { get; set; }
        public double Tray2TrayOffsetRZ { get; set; }

        public double Tray3TrayOffsetX { get; set; }
        public double Tray3TrayOffsetY { get; set; }
        public double Tray3TrayOffsetRZ { get; set; }

        public double Tray4TrayOffsetX { get; set; }
        public double Tray4TrayOffsetY { get; set; }
        public double Tray4TrayOffsetRZ { get; set; }

        public double Tray5TrayOffsetX { get; set; }
        public double Tray5TrayOffsetY { get; set; }
        public double Tray5TrayOffsetRZ { get; set; }

        public double Tray6TrayOffsetX { get; set; }
        public double Tray6TrayOffsetY { get; set; }
        public double Tray6TrayOffsetRZ { get; set; }

        public RobotSettingsModel()
        {
            Robot_settingsChanged(this, mRobot.Settings);
            mRobot.SettingsChanged += Robot_settingsChanged;
        }

        private void Robot_settingsChanged(object sender, RobotToTrayUnloader.Application.SubProcesses.RobotDataModel e)
        {
            BaseCount = e.BaseCount;
            DefaultVelocityPercentage = e.DefaultVelocityPercentage;

            Tray1TrayOffsetX = e.Tray1TrayOffsetX;
            Tray1TrayOffsetY = e.Tray1TrayOffsetY;
            Tray1TrayOffsetRZ = e.Tray1TrayOffsetRZ;

            Tray2TrayOffsetX = e.Tray2TrayOffsetX;
            Tray2TrayOffsetY = e.Tray2TrayOffsetY;
            Tray2TrayOffsetRZ = e.Tray2TrayOffsetRZ;

            Tray3TrayOffsetX = e.Tray3TrayOffsetX;
            Tray3TrayOffsetY = e.Tray3TrayOffsetY;
            Tray3TrayOffsetRZ = e.Tray3TrayOffsetRZ;

            Tray4TrayOffsetX = e.Tray4TrayOffsetX;
            Tray4TrayOffsetY = e.Tray4TrayOffsetY;
            Tray4TrayOffsetRZ = e.Tray4TrayOffsetRZ;

            Tray5TrayOffsetX = e.Tray5TrayOffsetX;
            Tray5TrayOffsetY = e.Tray5TrayOffsetY;
            Tray5TrayOffsetRZ = e.Tray5TrayOffsetRZ;

            Tray6TrayOffsetX = e.Tray6TrayOffsetX;
            Tray6TrayOffsetY = e.Tray6TrayOffsetY;
            Tray6TrayOffsetRZ = e.Tray6TrayOffsetRZ;
        }

        public void Save(string author)
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.Robot.BaseCount = BaseCount;
            productRecipe.Robot.DefaultVelocityPercentage = DefaultVelocityPercentage;

            productRecipe.Robot.Tray1TrayOffsetX = Tray1TrayOffsetX;
            productRecipe.Robot.Tray1TrayOffsetY = Tray1TrayOffsetY;
            productRecipe.Robot.Tray1TrayOffsetRZ = Tray1TrayOffsetRZ;

            productRecipe.Robot.Tray2TrayOffsetX = Tray2TrayOffsetX;
            productRecipe.Robot.Tray2TrayOffsetY = Tray2TrayOffsetY;
            productRecipe.Robot.Tray2TrayOffsetRZ = Tray2TrayOffsetRZ;

            productRecipe.Robot.Tray3TrayOffsetX = Tray3TrayOffsetX;
            productRecipe.Robot.Tray3TrayOffsetY = Tray3TrayOffsetY;
            productRecipe.Robot.Tray3TrayOffsetRZ = Tray3TrayOffsetRZ;

            productRecipe.Robot.Tray4TrayOffsetX = Tray4TrayOffsetX;
            productRecipe.Robot.Tray4TrayOffsetY = Tray4TrayOffsetY;
            productRecipe.Robot.Tray4TrayOffsetRZ = Tray4TrayOffsetRZ;

            productRecipe.Robot.Tray5TrayOffsetX = Tray5TrayOffsetX;
            productRecipe.Robot.Tray5TrayOffsetY = Tray5TrayOffsetY;
            productRecipe.Robot.Tray5TrayOffsetRZ = Tray5TrayOffsetRZ;

            productRecipe.Robot.Tray6TrayOffsetX = Tray6TrayOffsetX;
            productRecipe.Robot.Tray6TrayOffsetY = Tray6TrayOffsetY;
            productRecipe.Robot.Tray6TrayOffsetRZ = Tray6TrayOffsetRZ;

            mRecipeManager.SaveRecipe(productRecipe, author);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotDataModel
            {
                BaseCount = BaseCount,
                DefaultVelocityPercentage = DefaultVelocityPercentage,
                ProductNotPresentVacuumDelay = 25,
                ProductPresentVacuumDelay = 25,

                Tray1TrayOffsetX = Tray1TrayOffsetX,
                Tray1TrayOffsetY = Tray1TrayOffsetY,
                Tray1TrayOffsetRZ = Tray1TrayOffsetRZ,

                Tray2TrayOffsetX = Tray2TrayOffsetX,
                Tray2TrayOffsetY = Tray2TrayOffsetY,
                Tray2TrayOffsetRZ = Tray2TrayOffsetRZ,

                Tray3TrayOffsetX = Tray3TrayOffsetX,
                Tray3TrayOffsetY = Tray3TrayOffsetY,
                Tray3TrayOffsetRZ = Tray3TrayOffsetRZ,

                Tray4TrayOffsetX = Tray4TrayOffsetX,
                Tray4TrayOffsetY = Tray4TrayOffsetY,
                Tray4TrayOffsetRZ = Tray4TrayOffsetRZ,

                Tray5TrayOffsetX = Tray5TrayOffsetX,
                Tray5TrayOffsetY = Tray5TrayOffsetY,
                Tray5TrayOffsetRZ = Tray5TrayOffsetRZ,

                Tray6TrayOffsetX = Tray6TrayOffsetX,
                Tray6TrayOffsetY = Tray6TrayOffsetY,
                Tray6TrayOffsetRZ = Tray6TrayOffsetRZ
            };

            mRobot.UpdateSettings(dataModel);
        }

        /// <summary>
        /// Because this is used in an assistant there is an added possibility to return back to an older revision.
        /// (before changes were made in the assistant).
        /// </summary>
        /// <param name="revision"></param>
        internal void RefreshFromRecipe(int? revision = null, string author = "")
        {
            X1.Recipe.Lib.Product.Robot subRecipe;
            IProductRecipe productRecipe = mRecipeManager.GetCurrentRecipe();
            if (revision == null)
            { 
                subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.Product.Robot>(productRecipe);
            }
            else
            {
                var recipeRevisions = mRecipeManager.GetAllRevisionsOfRecipe(productRecipe.CustomerName);
                productRecipe = recipeRevisions.FirstOrDefault(productRecipe => productRecipe.Revision == revision);
                if (productRecipe != null)
                {
                    subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.Product.Robot>(productRecipe);
                }
                else
                {
                    return;
                }
            }

            BaseCount = subRecipe.BaseCount;
            DefaultVelocityPercentage = subRecipe.DefaultVelocityPercentage;

            Tray1TrayOffsetX = subRecipe.Tray1TrayOffsetX;
            Tray1TrayOffsetY = subRecipe.Tray1TrayOffsetY;
            Tray1TrayOffsetRZ = subRecipe.Tray1TrayOffsetRZ;

            Tray2TrayOffsetX = subRecipe.Tray2TrayOffsetX;
            Tray2TrayOffsetY = subRecipe.Tray2TrayOffsetY;
            Tray2TrayOffsetRZ = subRecipe.Tray2TrayOffsetRZ;

            Tray3TrayOffsetX = subRecipe.Tray3TrayOffsetX;
            Tray3TrayOffsetY = subRecipe.Tray3TrayOffsetY;
            Tray3TrayOffsetRZ = subRecipe.Tray3TrayOffsetRZ;

            Tray4TrayOffsetX = subRecipe.Tray4TrayOffsetX;
            Tray4TrayOffsetY = subRecipe.Tray4TrayOffsetY;
            Tray4TrayOffsetRZ = subRecipe.Tray4TrayOffsetRZ;

            Tray5TrayOffsetX = subRecipe.Tray5TrayOffsetX;
            Tray5TrayOffsetY = subRecipe.Tray5TrayOffsetY;
            Tray5TrayOffsetRZ = subRecipe.Tray5TrayOffsetRZ;

            Tray6TrayOffsetX = subRecipe.Tray6TrayOffsetX;
            Tray6TrayOffsetY = subRecipe.Tray6TrayOffsetY;
            Tray6TrayOffsetRZ = subRecipe.Tray6TrayOffsetRZ;

            Download();

            if (revision != null)
            {
                //When an old revision is refreshed, we also need to save this version
                mRecipeManager.SaveRecipe(productRecipe, author);
            }
        }
    }
}
