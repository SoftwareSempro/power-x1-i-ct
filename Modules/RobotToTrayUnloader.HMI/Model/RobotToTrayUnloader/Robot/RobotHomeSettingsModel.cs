﻿using Recipe.Lib;
using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class RobotHomeSettingsModel
    {
        private RobotProcess mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.RobotProcess;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double HomePositionX { get; set; }
        public double HomePositionY { get; set; }
        public double HomePositionZ { get; set; }
        public double HomePositionR { get; set; }

        public RobotHomeSettingsModel()
        {
            RobotHome_settingsChanged(this, mRobot.SettingsHome);
            mRobot.SettingsHomeChanged += RobotHome_settingsChanged;
        }

        private void RobotHome_settingsChanged(object sender, RobotHomeDataModel e)
        {
            HomePositionX = e.HomePosition.X;
            HomePositionY = e.HomePosition.Y;
            HomePositionZ = e.HomePosition.Z;
            HomePositionR = e.HomePosition.R;
        }

        public void Save()
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe() as MachineRecipe;
            machineRecipe.RobotTrayUnloader.Home.Position.X = HomePositionX;
            machineRecipe.RobotTrayUnloader.Home.Position.Y = HomePositionY; 
            machineRecipe.RobotTrayUnloader.Home.Position.Z = HomePositionZ; 
            machineRecipe.RobotTrayUnloader.Home.Position.R = HomePositionR;
           
            mRecipeManager.SaveRecipe(machineRecipe);
            Download();
        }

        public void Download()
        {
            var dataModel = new RobotHomeDataModel();
            dataModel.HomePosition.X = HomePositionX;
            dataModel.HomePosition.Y = HomePositionY;
            dataModel.HomePosition.Z = HomePositionZ;
            dataModel.HomePosition.R = HomePositionR;

            mRobot.UpdateRobotHomeSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            IMachineRecipe recipe = mRecipeManager.GetMachineRecipe();
            var subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.Machine.RobotTrayUnloader>(recipe);

            HomePositionX = subRecipe.Home.Position.X;
            HomePositionY = subRecipe.Home.Position.X;
            HomePositionZ = subRecipe.Home.Position.X;
            HomePositionR = subRecipe.Home.Position.X;

            Download();
        }
    }
}
