﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/SystemHub").build();

    // Module region
    connection.on("RobotToTrayUnloaderStatusUpdate", function (item, status) {
        receiveStatusChanged(item, status);
        getCanProcessCommands();
    });

    // StateBar region
    connection.on("RobotToTrayUnloaderStateUpdate", function (item, state) {
        getState();
        getCanProcessCommands();
        getCanStart();
        getCanStop();
    });

    connection.on("RobotToTrayUnloaderInProductionChanged", function () {
        getCanProcessCommands();
    });

    // CheckBoxChanged region
    connection.on("CheckBoxChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    connection.on("LineControlDataUpdated", function (value) {
        updateCovers(value.coversLocked);
    });

    connection.on("Camera3dControllerSensorStatusChanged", function () {
        get3dCameraControllerSensorValues();
    });

    connection.on("Camera3dControllerResultsChanged", function () {
        get3dCameraControllerResults();
    });

    connection.on("ScanningStageActualPositionXChanged", function () {
        getScanningStageActualPositionX(false);
    });

    connection.on("RobotPositionChanged", function () {
        getRobotPositionChanged(false)
    });

    // Sensors
    connection.on("RobotSensorStatusChanged", function () {
        getRobotSensorValues();
    });

    connection.on("RobotHasVacuumChanged", function () {
        getRobotSensorValues();
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
                getCanProcessCommands();
                getCanStart();
                getCanStop();
                getCoversLocked();
                get3dCameraControllerSensorValues();
                get3dCameraControllerResults();
                getScanningStageActualPositionX(true);
                getRobotSensorValues();
                getRobotPositionChanged(true);
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('RobotToTrayUnloaderState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        $(item).removeClass('error');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get status");
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);

        if (item != null) {
            $(item).removeClass('ready');
            $(item).removeClass('error');
            $(item).removeClass('busy')
            item.textContent = status;
            $(item).addClass(status.toLowerCase());
        }
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get simulation enabled");
            },
        });
    }

    function receiveSimulationEnabled(value) {
        var item = document.getElementById('RobotToTrayUnloaderSimulation');
        item.checked = value;
    }

    function getCoversLocked() {
        $.ajax({
            url: "/Setup/Setup?handler=AreCoversLocked",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                updateCovers(value);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }
    function updateCovers(value) {
        console.log('LineControlDataUpdated, covers locked: ' + value);
        if (value != null) {
            displayButton('ExecuteLockCoversButton', !value);
            displayButton('ExecuteUnlockCoversButton', value);
        }
    }

    function getCanProcessCommands() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=CanProcessCommands",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canProcess) {
                receiveCanProcessCommands(canProcess);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveCanProcessCommands(canProcess) {
        disableButton('ExecuteInspectionToInfeedCommand', !canProcess);
        disableButton('ExecuteInspectionCommand', !canProcess);
        disableButton('ExecuteRobotPickCommand', !canProcess);
        disableButton('ExecuteRobotPlaceCommand', !canProcess);
        disableButton('ExecuteRobotHomeCommand', !canProcess);

        //Resources
        disableButton('RobotGoToButton', !canProcess);
        disableButton('VacuumOnButton', !canProcess);
        disableButton('VacuumOffButton', !canProcess);
        disableButton('ActivateBlowOffButton', !canProcess);
        disableButton('DeactivateBlowOffButton', !canProcess);
        disableButton('ScanningStageGoToXButton', !canProcess);

        //Covers
        disableButton('ExecuteLockCoversButton', canProcess);
        disableButton('ExecuteUnlockCoversButton', canProcess);
    }

    function getCanStart() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=CanStart",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStart) {
                disableButton('ExecuteStartButton', !canStart);
                disableButton('ExecuteInitializeButton', !canStart);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getCanStop() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=CanStop",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStop) {
                disableButton('ExecuteStopButton', !canStop);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function get3dCameraControllerSensorValues() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=Get3dCameraControllerSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receive3dCameraControllerSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function receive3dCameraControllerSensorValues(sensorStatusses) {
        document.getElementById('3dCameraControllerControllerReadyCheckbox').checked = sensorStatusses[0];
        document.getElementById('3dCameraControllerControllerBusyCheckbox').checked = sensorStatusses[1];
        document.getElementById('3dCameraControllerControllerIsRunningCheckbox').checked = sensorStatusses[2];
    }

    function get3dCameraControllerResults() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=Get3dCameraControllerResults",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (results) {
                receive3dCameraControllerResults(results);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function receive3dCameraControllerResults(results) {
        document.getElementById('3dCameraControllerResult').value = results[0];
        document.getElementById('3dCameraControllerCommandNumber').value = results[1];
    }

    function getScanningStageActualPositionX(onConnected) {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetScanningStageActualPositionX",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (position) {
                receiveScanningStageActualPositionX(position, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveScanningStageActualPositionX(position, onConnected) {
        var newValue = position.toFixed(2);
        document.getElementById('ScanningStageActualPositionX').value = newValue;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("ScanningStagePositionX").value = newValue;
        }
    }

    function getRobotPositionChanged(onConnected) {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetRobotActualPosition",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (positions) {
                receiveRobotActualPosition(positions, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveRobotActualPosition(positions, onConnected) {
        var newValueX = positions[0].toFixed(2);
        var newValueY = positions[1].toFixed(2);
        var newValueZ = positions[2].toFixed(2);
        var newValueR = positions[3].toFixed(2);

        document.getElementById('RobotActualPositionX').value = newValueX;
        document.getElementById('RobotActualPositionY').value = newValueY;
        document.getElementById('RobotActualPositionZ').value = newValueZ;
        document.getElementById('RobotActualPositionR').value = newValueR;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("RobotPositionX").value = newValueX;
            document.getElementById("RobotPositionY").value = newValueY;
            document.getElementById("RobotPositionZ").value = newValueZ;
            document.getElementById("RobotPositionR").value = newValueR;
        }
    }

    function getRobotSensorValues() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetRobotSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveRobotSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function receiveRobotSensorValues(sensorStatusses) {
        document.getElementById('RobotIsVacuumOnCheckbox').checked = sensorStatusses[0];
        document.getElementById('RobotIsPauseCheckbox').checked = sensorStatusses[1];
    }
})



$("#CloseMainRobotToTrayUnloader").click(function (e) {
    e.preventDefault();
    $("#SetupMainRobotToTrayUnloaderContainer").hide();
});

$("#CloseSetupInspection").click(function (e) {
    e.preventDefault();
    $("#SetupInspectionContainer").hide();
});

$("#CloseSetupRobot").click(function (e) {
    e.preventDefault();
    $("#SetupRobotContainer").hide();
});

$("#CloseSetupRobotPick").click(function (e) {
    e.preventDefault();
    $("#SetupRobotPickContainer").hide();
});

$("#CloseSetupRobotPlace").click(function (e) {
    e.preventDefault();
    $("#SetupRobotPlaceContainer").hide();
});

$("#CloseSetupRobotHome").click(function (e) {
    e.preventDefault();
    $("#SetupRobotHomeContainer").hide();
});

$("#Close3DCamaeraControllerDebug").click(function (e) {
    e.preventDefault();
    $("#Debug3dCameraControllerContainer").hide();
});

$("#CloseScanningStageDebug").click(function (e) {
    e.preventDefault();
    $("#DebugScanningStageContainer").hide();
});

$("#CloseRobotDebug").click(function (e) {
    e.preventDefault();
    $("#DebugRobotContainer").hide();
});

$(function () {
    $("#SetupMainRobotToTrayUnloader").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainRobotToTrayUnloaderContainer");
    });

    $("#SetupInspection").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupInspectionContainer");
    });

    $("#SetupRobot").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotContainer");
    });

    $("#SetupRobotPick").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotPickContainer");
    });

    $("#SetupRobotPlace").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotPlaceContainer");
    });

    $("#SetupRobotHome").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotHomeContainer");
    });

    $("#Debug3dCameraController").click(function (e) {
        OnClickResourceSettingsButton(e, "#Debug3dCameraControllerContainer");
    });

    $("#DebugScanningStage").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugScanningStageContainer");
    });

    $("#DebugRobot").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugRobotContainer");
    });

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupMainRobotToTrayUnloaderContainer").hide();
        $("#SetupInspectionContainer").hide();
        $("#SetupRobotContainer").hide();
        $("#SetupRobotPickContainer").hide();
        $("#SetupRobotPlaceContainer").hide();
        $("#SetupRobotHomeContainer").hide();

    }

    function hideResourceContainers() {
        $("#Debug3dCameraControllerContainer").hide();
        $("#DebugScanningStageContainer").hide();
        $("#DebugRobotContainer").hide();
    }
});
