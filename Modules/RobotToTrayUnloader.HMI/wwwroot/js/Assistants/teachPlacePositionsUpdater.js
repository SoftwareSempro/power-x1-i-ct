﻿"use strict";

$(() => {
    console.log('Constructing: teachPlacePositionUpdater.js');
    startSignalRConnection();
});
function startSignalRConnection() {
    console.log("startSignalRConnection");
    start();
}

function stopSignalRConnection() {
    console.log("stopSignalRConnection");
    if (connection && connection.state === "Connected") {
        connection.stop().then(function () {
            console.log("SignalR connection stopped.");
        }).catch(function (err) {
            console.error("SignalR connection stop error: " + err);
        });
    }
}

var connection = new signalR.HubConnectionBuilder().withUrl("/SystemHub").build()

    connection.on("NavigationUpdated", function () {
        getNavigationData();
    });

connection.on("CanNextUpdated", function (value) {
    console.log('CanNextUpdated: ' + value);
    disableButton('nextButton', !value);
});

    connection.on("HasMaterialChanged", function (item, value) {
        if (item == 'RobotTrayUnloaderInfeedHasMaterial' || item == 'RobotTrayUnloaderRobotHasMaterial')
            getNavigationData();
    });

async function start() {
    try {
        connection.start().then(function () {
            console.log("Start: teachPlacePositionsUpdater.js");
            getNavigationData();
            getState();
            getStatus();
            getCanStart();
            getCanStop();
        });
    } catch (err) {
        console.log(err);
        setTimeout(start, 5000);
    }
};

function getNavigationData() {
    var currentPageIndex = $('#currentPageIndex').text() - 1;
    $.ajax({
        url: './Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=CanNextCommand',
        type: 'POST',
        data: { currentPageIndex: currentPageIndex },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (data) {
            disableButton('nextButton', !data);
            console.log("next button: " + data);
        },
        error: function (response) {
            console.log(response);
        },
    });

    $.ajax({
        url: './Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=CanPreviousCommand',
        type: 'POST',
        data: { currentPageIndex: currentPageIndex },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (data) {
            disableButton('previousButton', !data);
            console.log("previous button: " + data);
        },
        error: function (response) {
            console.log(response);
        },
    });

    connection.on("LineControlDataUpdated", function (value) {
        console.log('LineControlDataUpdated, covers locked: ' + value.coversLocked);
        getNavigationData();
        if (value != null) {
            disableButton('ExecuteLockCoversButton', value.coversLocked);
            disableButton('ExecuteUnlockCoversButton', !value.coversLocked);
            document.getElementById('CoversLocked').checked = value.coversLocked;

            disableButton('ExecuteInitializeButton', !value.coversLocked);
        }
    });

    connection.on("RobotToTrayUnloaderStatusUpdate", function (item, status) {
            receiveStatusChanged(item, status);
    });

    connection.on("RobotToTrayUnloaderStateUpdate", function (item, state) {
        getState();
        getCanStart();
        getCanStop();
    });

    function getCanStart() {
        $.ajax({
            url: "./Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=CanStart",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStart) {
                disableButton('ExecuteInitializeButton', !canStart);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function getCanStop() {
        $.ajax({
            url: "./Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=CanStop",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStop) {
                disableButton('ExecuteStopButton', !canStop);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function getState() {
        $.ajax({
            url: "./Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }
    function receiveStateChanged(state) {
        var item = document.getElementById('RobotTrayUnloaderState');

        if (item != null) {
            $(item).removeClass('stopping');
            $(item).removeClass('stopped');
            $(item).removeClass('starting');
            $(item).removeClass('running');
            $(item).removeClass('error');
            item.textContent = state.state;
            $(item).addClass(state.state.toLowerCase());

            if (state.state.toLowerCase() == 'running') {
                disableButton('startTeachPlacePositionsButton', false);
                disableButton('ExecuteInitializeButton', true);
            }
            else {
                disableButton('startTeachPlacePositionsButton', true);
            }

            if (state.state.toLowerCase() == 'stopped') {
                disableButton('ExecuteUnlockCoversButton', false);
                disableButton('ExecuteInitializeButton', false);
                disableButton('ExecuteStopButton', true);
            }
            else {
                disableButton('ExecuteUnlockCoversButton', true);
                disableButton('ExecuteInitializeButton', true);
                disableButton('ExecuteStopButton', false);
            }
        }
    }

    function getStatus() {
        $.ajax({
            url: "./Assistants/TeachPlacePositions/TeachPlacePositionsAssistant?handler=GetStatus",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (status) {
                receiveStatusChanged(status);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }

    function receiveStatusChanged(item, status) {
        console.log(item + ": " + status)

        if (item == "TeachPlacePositionsStatus" && status.toLowerCase() == 'ready') {
            disableButton('startToHomeButton', false);
            disableButton('startPickProductButton', false);
            disableButton('startToPlaceButton', false);
            getNavigationData();
        }
        var item = document.getElementById(item);
        if (item != null) {
            $(item).removeClass('ready');
            $(item).removeClass('error');
            $(item).removeClass('busy')
            item.textContent = status;
            $(item).addClass(status.toLowerCase());
        }
    }       
}