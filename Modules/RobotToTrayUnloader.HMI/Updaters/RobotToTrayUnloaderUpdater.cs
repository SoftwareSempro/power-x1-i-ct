﻿using log4net;
using Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Platform.HMI;
using Platform.HMI.Resources;
using RobotToTrayUnloader.Application.Resources;
using RobotToTrayUnloader.Application.SubProcesses;
using RobotTrayUnloader.Application.SubProcesses;

namespace RobotToTrayUnloader.Updater
{
    public class RobotToTrayUnloaderUpdater
    {
        private const string RobotToTrayUnloaderStatusUpdate = nameof(RobotToTrayUnloaderStatusUpdate);
        private const string RobotToTrayUnloaderStateUpdate = nameof(RobotToTrayUnloaderStateUpdate);
        private const string RobotToTrayUnloaderSimulationChanged = nameof(RobotToTrayUnloaderSimulationChanged);
        private const string RobotToTrayUnloaderInProductionChanged = nameof(RobotToTrayUnloaderInProductionChanged);

        private readonly ILog mLog = LogClient.Get();

        #region Subprocesses

        private const string cSimulationID = "RobotToTrayUnloaderSimulation";
        private const string cRobotToTrayUnloaderStatusID = "RobotToTrayUnloaderStatus";
        private readonly Application.RobotToTrayUnloader mRobotToTrayUnloader = Application.RobotToTrayUnloader.Instance;

        private const string cInspectionStatusID = "InspectionStatus";
        private readonly Inspection mInspection = Application.RobotToTrayUnloader.Instance.SubProcesses.Inspection;

        private const string cRobotProcessStatusID = "RobotProcessStatus";
        private readonly RobotProcess mRobotProcess = Application.RobotToTrayUnloader.Instance.SubProcesses.RobotProcess;

        private const string cTeachPlacePositionsStatusID = "TeachPlacePositionsStatus";
        private readonly TeachPlacePositions mTeachPlacePositions = Application.RobotToTrayUnloader.Instance.SubProcesses.TeachPlacePositions;

        #endregion

        private const string cScanningStageStatusID = "ScanningstageStatus";
        private readonly ScanningStage mScanningStage = Application.RobotToTrayUnloader.Instance.Resources.ScanningStage;
        private const string ScanningStageActualPositionXChanged = nameof(ScanningStageActualPositionXChanged);
        private const string ScanningStageSensorStatusChanged = nameof(ScanningStageSensorStatusChanged);

        private const string cRobotStatusID = "RobotStatus";
        private readonly Robot mRobot = Application.RobotToTrayUnloader.Instance.Resources.Robot;
        private const string RobotPositionChanged = nameof(RobotPositionChanged);
        private const string RobotHasVacuumChanged = nameof(RobotHasVacuumChanged);

        private const string cCamera3dControllerStatusID = "Camera3dControllerStatus";
        private readonly Camera3dController mCamera3dController = Application.RobotToTrayUnloader.Instance.Resources.Camera3dController;
        private const string Camera3dControllerSensorStatusChanged = nameof(Camera3dControllerSensorStatusChanged);
        private const string Camera3dControllerResultsChanged = nameof(Camera3dControllerResultsChanged);


        private readonly IHubContext<SystemHub> mContext;

        public RobotToTrayUnloaderUpdater(IHubContext<SystemHub> context)
        {
            mLog.Debug("RobotToTrayUnloaderUpdater");

            mContext = context;
            mRobotToTrayUnloader.StateChanged += async(Sender, e) => await SendStateChanged();
            mRobotToTrayUnloader.StatusChanged += async (Sender, e) => await SendStatusChanged(cRobotToTrayUnloaderStatusID, e);
            mRobotToTrayUnloader.InProductionChanged += async (sender, e) => await mContext.Clients.All.SendAsync(RobotToTrayUnloaderInProductionChanged);

            mInspection.StatusChanged += async (sender, e) => await SendStatusChanged(cInspectionStatusID, e);
            mRobotProcess.StatusChanged += async (sender, e) => await SendStatusChanged(cRobotProcessStatusID, e);
            mTeachPlacePositions.StatusChanged += async (sender, e) => await SendStatusChanged(cTeachPlacePositionsStatusID, mTeachPlacePositions.Status);

            mScanningStage.StatusChanged += async (sender, e) => await SendStatusChanged(cScanningStageStatusID, e);
            mScanningStage.PositionXChanged += async (sender, e) => await SendASyncToClients(ScanningStageActualPositionXChanged);
            mScanningStage.SensorStatusChanged += async () => await SendASyncToClients(ScanningStageSensorStatusChanged);
            mRobot.StatusChanged += async (sender, e) => await SendStatusChanged(cRobotStatusID, e);
            mRobot.PositionChanged += async () => await SendASyncToClients(RobotPositionChanged);
            mRobot.SensorStatusChanged += async () => await SendASyncToClients(RobotHasVacuumChanged);
            mRobotToTrayUnloader.SimulationChanged += async (sender, e) => await SendCheckBoxChangedAsyncToClients(cSimulationID, e);

            mCamera3dController.StatusChanged += async (sender, e) => await SendStatusChanged(cCamera3dControllerStatusID, e);
            mCamera3dController.SensorStatusChanged += async () => await SendASyncToClients(Camera3dControllerSensorStatusChanged);
            mCamera3dController.ResultsChanged += async () => await SendASyncToClients(Camera3dControllerResultsChanged);

        }

        internal JsonResult GetStatusses()
        {
            Dictionary<string, string> statusses = new()
            {
                { cRobotToTrayUnloaderStatusID, StatusBar.ConvertToString(mRobotToTrayUnloader.Status) },

                { cInspectionStatusID, StatusBar.ConvertToString(mInspection.Status) },
                { cRobotProcessStatusID, StatusBar.ConvertToString(mRobotProcess.Status) },
                { cTeachPlacePositionsStatusID, StatusBar.ConvertToString(mTeachPlacePositions.Status) },

                { cRobotStatusID, StatusBar.ConvertToString(mRobot.Status) },
                { cScanningStageStatusID, StatusBar.ConvertToString(mScanningStage.Status) },
                { cCamera3dControllerStatusID, StatusBar.ConvertToString(mCamera3dController.Status) }
            };

            return new JsonResult(statusses);
        }

        private async Task SendStatusChanged(string itemToUpdate, int value)
        {
            //Convert value to statusbar item text
            var statusNameOfIntValue = StatusBar.ConvertToString(value);

            mLog.Debug($"SendStatusChanged: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderStatusUpdate, itemToUpdate, statusNameOfIntValue);
        }

        private async Task SendStateChanged()
        {
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderStateUpdate);
        }
        private async Task SendCheckBoxChangedAsyncToClients(string itemToUpdate, bool value)
        {
            mLog.Debug($"SendAsyncToClients: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderSimulationChanged, itemToUpdate, value);
        }

        private async Task SendASyncToClients(string item)
        {
            await mContext.Clients.All.SendAsync(item);
        }
    }          
}
