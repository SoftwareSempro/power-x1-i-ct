﻿using EventHandler.Application;
using RobotTrayUnloader.Lib;

namespace RobotToTrayUnloader.Application
{
    public class EventHandler : EventHandlerSubScriptionBase
    {
        public bool ScanningstageFailedToReachInfeed { set => SetEventPropertyValue(value); }
        public bool ScanningstageFailedToReachOutfeed { set => SetEventPropertyValue(value); }        
        public bool RobotFailedToReachPickPosition { set => SetEventPropertyValue(value); }
        public bool RobotFailedToReachPlacePosition { set => SetEventPropertyValue(value); }
        public bool RobotFailedToReachHomePosition { set => SetEventPropertyValue(value); }
        public bool NoEmptyPocketFound { set => SetEventPropertyValue(value); }
        public bool RobotFailedToPickUpProduct { set => SetEventPropertyValue(value); }
        public bool RobotLostProductDuringMovement { set => SetEventPropertyValue(value); }
        public bool RobotFailedToPlaceProduct { set => SetEventPropertyValue(value); }
        public bool RobotModuleEmergenceStopActive { set => SetEventPropertyValue(value); }
        public bool ScanningStageDriveError { set => SetEventPropertyValue(value); }
        public bool ScanningStageCommunicationError { set => SetEventPropertyValue(value); }
        public bool ScanningStagePositionError { set => SetEventPropertyValue(value); }
        public bool RobotHasError { set => SetEventPropertyValue(value); }
        public bool RobotTrayUnloaderFrontLeftCoverOpenInfo { set => SetEventPropertyValue(value); }
        public bool RobotTrayUnloaderFrontRightCoverOpenInfo { set => SetEventPropertyValue(value); }
        public bool RobotTrayUnloaderRearLeftCoverOpenInfo { set => SetEventPropertyValue(value); }
        public bool RobotTrayUnloaderRearRightCoverOpenInfo { set => SetEventPropertyValue(value); }

        public EventHandler()
        {
            mEventDictionary = new()
            {
                { nameof(ScanningstageFailedToReachInfeed), RobotTrayUnloaderEvents.ScanningstageFailedToReachInfeed },
                { nameof(ScanningstageFailedToReachOutfeed), RobotTrayUnloaderEvents.ScanningstageFailedToReachOutfeed },
                { nameof(RobotFailedToReachPickPosition), RobotTrayUnloaderEvents.RobotFailedToReachPickPosition },
                { nameof(RobotFailedToReachPlacePosition), RobotTrayUnloaderEvents.RobotFailedToReachPlacePosition },
                { nameof(RobotFailedToReachHomePosition), RobotTrayUnloaderEvents.RobotFailedToReachHomePosition },
                { nameof(NoEmptyPocketFound), RobotTrayUnloaderEvents.NoEmptyPocketFoundAssist },
                { nameof(RobotFailedToPickUpProduct), RobotTrayUnloaderEvents.RobotFailedToPickUpProduct },
                { nameof(RobotLostProductDuringMovement), RobotTrayUnloaderEvents.RobotLostProductDuringMovement },
                { nameof(RobotFailedToPlaceProduct), RobotTrayUnloaderEvents.RobotFailedToPlaceProduct },
                { nameof(RobotModuleEmergenceStopActive), RobotTrayUnloaderEvents.RobotModuleEmergenceStopActive },
                { nameof(ScanningStageDriveError), RobotTrayUnloaderEvents.ScanningStageDriveError },
                { nameof(ScanningStageCommunicationError), RobotTrayUnloaderEvents.ScanningStageCommunicationError },
                { nameof(ScanningStagePositionError), RobotTrayUnloaderEvents.ScanningStagePositionError },
                { nameof(RobotHasError), RobotTrayUnloaderEvents.RobotHasError },
                { nameof(RobotTrayUnloaderFrontLeftCoverOpenInfo), RobotTrayUnloaderEvents.RobotTrayUnloaderFrontLeftCoverOpenInfo },
                { nameof(RobotTrayUnloaderFrontRightCoverOpenInfo), RobotTrayUnloaderEvents.RobotTrayUnloaderFrontRightCoverOpenInfo },
                { nameof(RobotTrayUnloaderRearLeftCoverOpenInfo), RobotTrayUnloaderEvents.RobotTrayUnloaderRearLeftCoverOpenInfo },
                { nameof(RobotTrayUnloaderRearRightCoverOpenInfo), RobotTrayUnloaderEvents.RobotTrayUnloaderRearRightCoverOpenInfo },
            };

            RegisterTagModelsInDictionary();
        }
    }
}
