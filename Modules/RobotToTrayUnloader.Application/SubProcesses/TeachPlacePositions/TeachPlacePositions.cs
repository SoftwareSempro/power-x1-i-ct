using MachineManager;
using MachineManager.Lib;
using System;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using Platform;
using Platform.Lib.PlcInterfaces.MainProcess;

namespace RobotTrayUnloader.Application.SubProcesses
{
  public class TeachPlacePositions: SubProcess
  {
     private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPc.Status);
     private static readonly TagModel mTrayNumberTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.TrayNumber);
     private static readonly TagModel mColumnTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.Column);
     private static readonly TagModel mRowTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.Row);
     private static readonly TagModel mPlaceMaterialCommandTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.PlaceMaterialCommand);
     private static readonly TagModel mRemoveMaterialCommandTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.RemoveMaterialCommand);
     private static readonly TagModel mPickCommandTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.PickCommand);
     private static readonly TagModel mToPlaceCommandTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.ToPlaceCommand);
     private static readonly TagModel mToHomeCommandTagModel  = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderTeachPlacePositionsInterface.ToPlc.ToHomeCommand);

     private readonly TeachPlacePositionsDataModel mSettings = new();

        public TeachPlacePositionsDataModel Settings => mSettings;
        public event EventHandler<TeachPlacePositionsDataModel> SettingsChanged;

        #region Properties

     public short TrayNumber
      {
         get => mSettings.TrayNumber;
         set
          {
             if (mSettings.TrayNumber != value)
              {
                  mSettings.TrayNumber = value;
                  SettingsChanged?.Invoke(this, mSettings);
              }
          }
      }
     public short Column
      {
         get => mSettings.Column;
         set
          {
             if (mSettings.Column != value)
              {
                  mSettings.Column = value;
                  SettingsChanged?.Invoke(this, mSettings);
              }
          }
      }
     public short Row
      {
         get => mSettings.Row;
         set
          {
             if (mSettings.Row != value)
              {
                  mSettings.Row = value;
                  SettingsChanged?.Invoke(this, mSettings);
              }
          }
      }

        #endregion

        public TeachPlacePositions()
        {
            TagModelPathDictionary = new()
           {
                { StatusTagModel, nameof(Status) },
                { mTrayNumberTagModel, nameof(TrayNumber) },
                { mColumnTagModel, nameof(Column) },
                { mRowTagModel, nameof(Row) },
          };
        }

        public static async void PlaceMaterialCommand()
        {
            await PlcService.Instance.WriteValueAsync(mPlaceMaterialCommandTagModel, true);
        }

        public static async void RemoveMaterialCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
        }

        public static async void PickCommand()
        {
            await PlcService.Instance.WriteValueAsync(mPickCommandTagModel, true);
        }

        public static async void ToPlaceCommand(TeachPlacePositionsDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mTrayNumberTagModel, settings.TrayNumber);
            await PlcService.Instance.WriteValueAsync(mRowTagModel, settings.Row);
            await PlcService.Instance.WriteValueAsync(mColumnTagModel, settings.Column);
            await PlcService.Instance.WriteValueAsync(mToPlaceCommandTagModel, true);
        }

        public static async void ToHomeCommand()
        {
            await PlcService.Instance.WriteValueAsync(mToHomeCommandTagModel, true);
        }
    }
}
