namespace RobotTrayUnloader.Application.SubProcesses
{
    public class TeachPlacePositionsDataModel
    {
        public short TrayNumber { get; set; }
        public short Column { get; set; }
        public short Row { get; set; }
    }
}
