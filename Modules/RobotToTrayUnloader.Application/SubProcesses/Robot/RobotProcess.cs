﻿using DatabaseManager.Lib;
using MachineManager;
using MachineManager.Lib;
using Platform;
using Recipe.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotProcess : SubProcess
    {
        public event EventHandler<RobotDataModel> SettingsChanged;
        public event EventHandler<RobotPickDataModel> SettingsPickChanged;
        public event EventHandler<RobotPlaceDataModel> SettingsPlaceChanged;
        public event EventHandler<RobotHomeDataModel> SettingsHomeChanged;

        private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPc.Status);

        private readonly TagModel mPickPositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.Position.X);
        private readonly TagModel mPickPositionYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.Position.Y);
        private readonly TagModel mPickPositionZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.Position.Z);
        private readonly TagModel mPickPositionRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.Position.R);
        private readonly TagModel mPickOffsetZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.PickOffSetZ);
        private readonly TagModel mPickApproachVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.VelocityPercentage.ApproachVelocityPercentage);
        private readonly TagModel mPickClearVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Pick.VelocityPercentage.ClearVelocityPercentage);

        private readonly TagModel mPlacePocketOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.PocketOffSetX);
        private readonly TagModel mPlacePocketOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.PocketOffsetY);
        private readonly TagModel mPlaceTrayHeightTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.TrayHeight);
        private readonly TagModel mPlacePocketDepthTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.PocketDepth);
        private readonly TagModel mDeviceToPocketOffsetZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.DeviceToPocketOffsetZ);
        private readonly TagModel mPlaceTrayToFirstPocketOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.TrayToFirstPocketOffsetX);
        private readonly TagModel mPlaceTrayToFirstPocketOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.TrayToFirstPocketOffsetY);
        private readonly TagModel mDeviceSizeZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.DeviceSizeZ);
        private readonly TagModel mPlaceOffsetZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.PlaceOffsetZ);
        private readonly TagModel mPlaceApproachVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.VelocityPercentage.ApproachVelocityPercentage);
        private readonly TagModel mPlaceClearVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Place.VelocityPercentage.ClearVelocityPercentage);

        private readonly TagModel mHomePositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Home.Position.X);
        private readonly TagModel mHomePositionYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Home.Position.Y);
        private readonly TagModel mHomePositionZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Home.Position.Z);
        private readonly TagModel mHomePositionRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.Home.Position.R);

        private readonly TagModel mBaseCountTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.BaseCount);
        private readonly TagModel mDefaultVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Robot.DefaultVelocityPercentage);

        private readonly TagModel mProductPresentVacuumDelayTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Vacuum.ProductPresentVacuumDelay);
        private readonly TagModel mProductNotPresentVacuumDelayTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Vacuum.ProductNotPresentVacuumDelay);

        private readonly TagModel mRetryCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RetryCommand);

        private readonly TagModel mTrayType1TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray1.TrayType);
        private readonly TagModel mTray1TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray1.TrayOffset.X);
        private readonly TagModel mTray1TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray1.TrayOffset.Y);
        private readonly TagModel mTray1TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray1.TrayOffset.RZ);
        private readonly TagModel mTrayType2TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray2.TrayType);
        private readonly TagModel mTray2TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray2.TrayOffset.X);
        private readonly TagModel mTray2TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray2.TrayOffset.Y);
        private readonly TagModel mTray2TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray2.TrayOffset.RZ);
        private readonly TagModel mTrayType3TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray3.TrayType);
        private readonly TagModel mTray3TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray3.TrayOffset.X);
        private readonly TagModel mTray3TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray3.TrayOffset.Y);
        private readonly TagModel mTray3TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray3.TrayOffset.RZ);
        private readonly TagModel mTrayType4TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray4.TrayType);
        private readonly TagModel mTray4TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray4.TrayOffset.X);
        private readonly TagModel mTray4TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray4.TrayOffset.Y);
        private readonly TagModel mTray4TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray4.TrayOffset.RZ);
        private readonly TagModel mTrayType5TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray5.TrayType);
        private readonly TagModel mTray5TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray5.TrayOffset.X);
        private readonly TagModel mTray5TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray5.TrayOffset.Y);
        private readonly TagModel mTray5TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray5.TrayOffset.RZ);
        private readonly TagModel mTrayType6TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray6.TrayType);
        private readonly TagModel mTray6TrayOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray6.TrayOffset.X);
        private readonly TagModel mTray6TrayOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray6.TrayOffset.Y);
        private readonly TagModel mTray6TrayOffsetRZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessInterface.ToPlc.RobotSettings.Trays.Tray6.TrayOffset.RZ);

        private RobotDataModel mSettings;
        private RobotPickDataModel mPickSettings;
        private RobotPlaceDataModel mPlaceSettings;
        private RobotHomeDataModel mHomeSettings;
        private RobotProductRecipeTrayDataModel mProductRecipeSettings;               

        public RobotDataModel Settings => mSettings;
        public RobotPickDataModel SettingsPick => mPickSettings;
        public RobotPlaceDataModel SettingsPlace => mPlaceSettings;
        public RobotHomeDataModel SettingsHome => mHomeSettings;
        public RobotProductRecipeTrayDataModel settingsProductRecipe => mProductRecipeSettings;
               
        public double PickPositionX
        {
            get => (float)mPickSettings.PickPosition.X;
            set
            {
                if (mPickSettings.PickPosition.X != value)
                {
                    mPickSettings.PickPosition.X = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionY
        {
            get => (float)mPickSettings.PickPosition.Y;
            set
            {
                if (mPickSettings.PickPosition.Y != value)
                {
                    mPickSettings.PickPosition.Y = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionZ
        {
            get => (float)mPickSettings.PickPosition.Z;
            set
            {
                if (mPickSettings.PickPosition.Z != value)
                {
                    mPickSettings.PickPosition.Z = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionR
        {
            get => (float)mPickSettings.PickPosition.R;
            set
            {
                if (mPickSettings.PickPosition.R != value)
                {
                    mPickSettings.PickPosition.R = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickOffsetZ
        {
            get => (float)mPickSettings.PickOffsetZ;
            set
            {
                if (mPickSettings.PickOffsetZ != value)
                {
                    mPickSettings.PickOffsetZ = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public int PickApproachVelocityPercentage
        {
            get => mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage;
            set
            {
                if (mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage != value)
                {
                    mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }
        public int PickClearVelocityPercentage
        {
            get => mPickSettings.PickVelocityPercentage.ClearVelocityPercentage;
            set
            {
                if (mPickSettings.PickVelocityPercentage.ClearVelocityPercentage != value)
                {
                    mPickSettings.PickVelocityPercentage.ClearVelocityPercentage = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }
        public double PlacePocketOffsetX
        {
            get => (float)mProductRecipeSettings.PocketOffsetX;
            set
            {
                if (mProductRecipeSettings.PocketOffsetX != value)
                {
                    mProductRecipeSettings.PocketOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlacePocketOffsetY
        {
            get => (float)mProductRecipeSettings.PocketOffsetY;
            set
            {
                if (mProductRecipeSettings.PocketOffsetY != value)
                {
                    mProductRecipeSettings.PocketOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double TrayHeight
        {
            get => (float)mProductRecipeSettings.TrayHeight;
            set
            {
                if (mProductRecipeSettings.TrayHeight != value)
                {
                    mProductRecipeSettings.TrayHeight = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PocketDepth
        {
            get => (float)mProductRecipeSettings.PocketDepth;
            set
            {
                if (mProductRecipeSettings.PocketDepth != value)
                {
                    mProductRecipeSettings.PocketDepth = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceTrayToFirstPocketOffsetX
        {
            get => (float)mProductRecipeSettings.TrayToFirstPocketOffsetX;
            set
            {
                if (mProductRecipeSettings.TrayToFirstPocketOffsetX != value)
                {
                    mProductRecipeSettings.TrayToFirstPocketOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceTrayToFirstPocketOffsetY
        {
            get => (float)mProductRecipeSettings.TrayToFirstPocketOffsetY;
            set
            {
                if (mProductRecipeSettings.TrayToFirstPocketOffsetY != value)
                {
                    mProductRecipeSettings.TrayToFirstPocketOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double DeviceSizeZ
        {
            get => (float)mProductRecipeSettings.DeviceSizeZ;
            set
            {
                if (mProductRecipeSettings.DeviceSizeZ != value)
                {
                    mProductRecipeSettings.DeviceSizeZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double DeviceToPocketOffsetZ
        {
            get => (float)mProductRecipeSettings.DeviceToPocketOffsetZ;
            set
            {
                if (mProductRecipeSettings.DeviceToPocketOffsetZ != value)
                {
                    mProductRecipeSettings.DeviceToPocketOffsetZ = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }

        public double PlaceOffsetZ
        {
            get => (float)mPlaceSettings.PlaceOffsetZ;
            set
            {
                if (mPlaceSettings.PlaceOffsetZ != value)
                {
                    mPlaceSettings.PlaceOffsetZ = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }

        public int PlaceApproachVelocityPercentage
        {
            get => mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage;
            set
            {
                if (mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage != value)
                {
                    mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }
        public int PlaceClearVelocityPercentage
        {
            get => mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage;
            set
            {
                if (mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage != value)
                {
                    mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }

        public double HomePositionX
        {
            get => (float)mHomeSettings.HomePosition.X;
            set
            {
                if (mHomeSettings.HomePosition.X != value)
                {
                    mHomeSettings.HomePosition.X = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionY
        {
            get => (float)mHomeSettings.HomePosition.Y;
            set
            {
                if (mHomeSettings.HomePosition.Y != value)
                {
                    mHomeSettings.HomePosition.Y = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionZ
        {
            get => (float)mHomeSettings.HomePosition.Z;
            set
            {
                if (mHomeSettings.HomePosition.Z != value)
                {
                    mHomeSettings.HomePosition.Z = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionR
        {
            get => (float)mHomeSettings.HomePosition.R;
            set
            {
                if (mHomeSettings.HomePosition.R != value)
                {
                    mHomeSettings.HomePosition.R = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public int BaseCount
        {
            get => mSettings.BaseCount;
            set
            {
                if (mSettings.BaseCount != value)
                {
                    mSettings.BaseCount = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int DefaultVelocityPercentage
        {
            get => mSettings.DefaultVelocityPercentage;
            set
            {
                if (mSettings.DefaultVelocityPercentage != value)
                {
                    mSettings.DefaultVelocityPercentage = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int ProductPresentVacuumDelay
        {
            get => mSettings.ProductPresentVacuumDelay;
            set
            {
                if (mSettings.ProductPresentVacuumDelay != value)
                {
                    mSettings.ProductPresentVacuumDelay = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int ProductNotPresentVacuumDelay
        {
            get => mSettings.ProductNotPresentVacuumDelay;
            set
            {
                if (mSettings.ProductNotPresentVacuumDelay != value)
                {
                    mSettings.ProductNotPresentVacuumDelay = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType1
        {
            get => mSettings.TrayType1;
            set
            {
                if (mSettings.TrayType1 != value)
                {
                    mSettings.TrayType1 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray1TrayOffsetX
        {
            get => (float)mSettings.Tray1TrayOffsetX;
            set
            {
                if (mSettings.Tray1TrayOffsetX != value)
                {
                    mSettings.Tray1TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray1TrayOffsetY
        {
            get => (float)mSettings.Tray1TrayOffsetY;
            set
            {
                if (mSettings.Tray1TrayOffsetY != value)
                {
                    mSettings.Tray1TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray1TrayOffsetRZ
        {
            get => (float)mSettings.Tray1TrayOffsetRZ;
            set
            {
                if (mSettings.Tray1TrayOffsetRZ != value)
                {
                    mSettings.Tray1TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType2
        {
            get => mSettings.TrayType2;
            set
            {
                if (mSettings.TrayType2 != value)
                {
                    mSettings.TrayType2 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray2TrayOffsetX
        {
            get => (float)mSettings.Tray2TrayOffsetX;
            set
            {
                if (mSettings.Tray2TrayOffsetX != value)
                {
                    mSettings.Tray2TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray2TrayOffsetY
        {
            get => (float)mSettings.Tray2TrayOffsetY;
            set
            {
                if (mSettings.Tray2TrayOffsetY != value)
                {
                    mSettings.Tray2TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray2TrayOffsetRZ
        {
            get => (float)mSettings.Tray2TrayOffsetRZ;
            set
            {
                if (mSettings.Tray2TrayOffsetRZ != value)
                {
                    mSettings.Tray2TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType3
        {
            get => mSettings.TrayType3;
            set
            {
                if (mSettings.TrayType3 != value)
                {
                    mSettings.TrayType3 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray3TrayOffsetX
        {
            get => (float)mSettings.Tray3TrayOffsetX;
            set
            {
                if (mSettings.Tray3TrayOffsetX != value)
                {
                    mSettings.Tray3TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray3TrayOffsetY
        {
            get => (float)mSettings.Tray3TrayOffsetY;
            set
            {
                if (mSettings.Tray3TrayOffsetY != value)
                {
                    mSettings.Tray3TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray3TrayOffsetRZ
        {
            get => (float)mSettings.Tray3TrayOffsetRZ;
            set
            {
                if (mSettings.Tray3TrayOffsetRZ != value)
                {
                    mSettings.Tray3TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType4
        {
            get => mSettings.TrayType4;
            set
            {
                if (mSettings.TrayType4 != value)
                {
                    mSettings.TrayType4 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray4TrayOffsetX
        {
            get => (float)mSettings.Tray4TrayOffsetX;
            set
            {
                if (mSettings.Tray4TrayOffsetX != value)
                {
                    mSettings.Tray4TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray4TrayOffsetY
        {
            get => (float)mSettings.Tray4TrayOffsetY;
            set
            {
                if (mSettings.Tray4TrayOffsetY != value)
                {
                    mSettings.Tray4TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray4TrayOffsetRZ
        {
            get => (float)mSettings.Tray4TrayOffsetRZ;
            set
            {
                if (mSettings.Tray4TrayOffsetRZ != value)
                {
                    mSettings.Tray4TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType5
        {
            get => mSettings.TrayType5;
            set
            {
                if (mSettings.TrayType5 != value)
                {
                    mSettings.TrayType5 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray5TrayOffsetX
        {
            get => (float)mSettings.Tray5TrayOffsetX;
            set
            {
                if (mSettings.Tray5TrayOffsetX != value)
                {
                    mSettings.Tray5TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray5TrayOffsetY
        {
            get => (float)mSettings.Tray5TrayOffsetY;
            set
            {
                if (mSettings.Tray5TrayOffsetY != value)
                {
                    mSettings.Tray5TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray5TrayOffsetRZ
        {
            get => (float)mSettings.Tray5TrayOffsetRZ;
            set
            {
                if (mSettings.Tray5TrayOffsetRZ != value)
                {
                    mSettings.Tray5TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType6
        {
            get => mSettings.TrayType6;
            set
            {
                if (mSettings.TrayType6 != value)
                {
                    mSettings.TrayType6 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray6TrayOffsetX
        {
            get => (float)mSettings.Tray6TrayOffsetX;
            set
            {
                if (mSettings.Tray6TrayOffsetX != value)
                {
                    mSettings.Tray6TrayOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray6TrayOffsetY
        {
            get => (float)mSettings.Tray6TrayOffsetY;
            set
            {
                if (mSettings.Tray6TrayOffsetY != value)
                {
                    mSettings.Tray6TrayOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Tray6TrayOffsetRZ
        {
            get => (float)mSettings.Tray6TrayOffsetRZ;
            set
            {
                if (mSettings.Tray6TrayOffsetRZ != value)
                {
                    mSettings.Tray6TrayOffsetRZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public RobotProcess()
        {
            mSettings = new RobotDataModel();
            mPickSettings = new RobotPickDataModel();
            mPlaceSettings = new RobotPlaceDataModel();
            mHomeSettings = new RobotHomeDataModel();
            mProductRecipeSettings = new RobotProductRecipeTrayDataModel();

            TagModelPathDictionary = new()
            {
                 { StatusTagModel, nameof(Status)},
                 { mPickPositionXTagModel, nameof(PickPositionX)},
                 { mPickPositionYTagModel, nameof(PickPositionY)},
                 { mPickPositionZTagModel, nameof(PickPositionZ)},
                 { mPickPositionRTagModel, nameof(PickPositionR)},
                 { mPickOffsetZTagModel, nameof(PickOffsetZ)},
                 { mPickApproachVelocityPercentageTagModel, nameof(PickApproachVelocityPercentage)},
                 { mPickClearVelocityPercentageTagModel, nameof(PickClearVelocityPercentage)},
                 { mPlaceOffsetZTagModel, nameof(PlaceOffsetZ)},
                 { mDeviceToPocketOffsetZTagModel, nameof (DeviceToPocketOffsetZ)},
                 { mPlaceApproachVelocityPercentageTagModel, nameof(PlaceApproachVelocityPercentage)},
                 { mPlaceClearVelocityPercentageTagModel, nameof(PlaceClearVelocityPercentage)},
                 { mHomePositionXTagModel, nameof(HomePositionX)},
                 { mHomePositionYTagModel, nameof(HomePositionY)},
                 { mHomePositionZTagModel, nameof(HomePositionZ)},
                 { mHomePositionRTagModel, nameof(HomePositionR)},
                 { mBaseCountTagModel, nameof(BaseCount)},
                 { mDefaultVelocityPercentageTagModel, nameof(DefaultVelocityPercentage)},
                 { mProductPresentVacuumDelayTagModel, nameof(ProductPresentVacuumDelay)},
                 { mProductNotPresentVacuumDelayTagModel, nameof(ProductNotPresentVacuumDelay)},
                 { mTrayType1TagModel, nameof(TrayType1)},
                 { mTray1TrayOffsetXTagModel, nameof(Tray1TrayOffsetX)},
                 { mTray1TrayOffsetYTagModel, nameof(Tray1TrayOffsetY)},
                 { mTray1TrayOffsetRZTagModel, nameof(Tray1TrayOffsetRZ)},
                 { mTrayType2TagModel, nameof(TrayType2)},
                 { mTray2TrayOffsetXTagModel, nameof(Tray2TrayOffsetX)},
                 { mTray2TrayOffsetYTagModel, nameof(Tray2TrayOffsetY)},
                 { mTray2TrayOffsetRZTagModel, nameof(Tray2TrayOffsetRZ)},
                 { mTrayType3TagModel, nameof(TrayType3)},
                 { mTray3TrayOffsetXTagModel, nameof(Tray3TrayOffsetX)},
                 { mTray3TrayOffsetYTagModel, nameof(Tray3TrayOffsetY)},
                 { mTray3TrayOffsetRZTagModel, nameof(Tray3TrayOffsetRZ)},
                 { mTrayType4TagModel, nameof(TrayType4)},
                 { mTray4TrayOffsetXTagModel, nameof(Tray4TrayOffsetX)},
                 { mTray4TrayOffsetYTagModel, nameof(Tray4TrayOffsetY)},
                 { mTray4TrayOffsetRZTagModel, nameof(Tray4TrayOffsetRZ)},
                 { mTrayType5TagModel, nameof(TrayType5)},
                 { mTray5TrayOffsetXTagModel, nameof(Tray5TrayOffsetX)},
                 { mTray5TrayOffsetYTagModel, nameof(Tray5TrayOffsetY)},
                 { mTray5TrayOffsetRZTagModel, nameof(Tray5TrayOffsetRZ)},
                 { mTrayType6TagModel, nameof(TrayType6)},
                 { mTray6TrayOffsetXTagModel, nameof(Tray6TrayOffsetX)},
                 { mTray6TrayOffsetYTagModel, nameof(Tray6TrayOffsetY)},
                 { mTray6TrayOffsetRZTagModel, nameof(Tray6TrayOffsetRZ)},
            };
        }

        public async void UpdateRobotPickSettings(RobotPickDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mPickPositionXTagModel, (float)settings.PickPosition.X);
            await PlcService.Instance.WriteValueAsync(mPickPositionYTagModel, (float)settings.PickPosition.Y);
            await PlcService.Instance.WriteValueAsync(mPickPositionZTagModel, (float)settings.PickPosition.Z);
            await PlcService.Instance.WriteValueAsync(mPickPositionRTagModel, (float)settings.PickPosition.R);
            await PlcService.Instance.WriteValueAsync(mPickOffsetZTagModel, (float)settings.PickOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPickApproachVelocityPercentageTagModel, (short)settings.PickVelocityPercentage.ApproachVelocityPercentage);
            await PlcService.Instance.WriteValueAsync(mPickClearVelocityPercentageTagModel, (short)settings.PickVelocityPercentage.ClearVelocityPercentage);
        }

        public async void UpdateRobotPlaceSettings(RobotPlaceDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mPlaceOffsetZTagModel, (float)settings.PlaceOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPlaceApproachVelocityPercentageTagModel, (short)settings.PlaceVelocityPercentage.ApproachVelocityPercentage);
            await PlcService.Instance.WriteValueAsync(mPlaceClearVelocityPercentageTagModel, (short)settings.PlaceVelocityPercentage.ClearVelocityPercentage);
        }

        public async void UpdateRobotHomeSettings(RobotHomeDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mHomePositionXTagModel, (float)settings.HomePosition.X);
            await PlcService.Instance.WriteValueAsync(mHomePositionYTagModel, (float)settings.HomePosition.Y);
            await PlcService.Instance.WriteValueAsync(mHomePositionZTagModel, (float)settings.HomePosition.Z);
            await PlcService.Instance.WriteValueAsync(mHomePositionRTagModel, (float)settings.HomePosition.R);
        }

        public async void UpdateRobotProductRecipeSettings(RobotProductRecipeTrayDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mPlacePocketOffsetXTagModel, (float)settings.PocketOffsetX);
            await PlcService.Instance.WriteValueAsync(mPlacePocketOffsetYTagModel, (float)settings.PocketOffsetY);
            await PlcService.Instance.WriteValueAsync(mPlaceTrayHeightTagModel, (float)settings.TrayHeight);
            await PlcService.Instance.WriteValueAsync(mPlacePocketDepthTagModel, (float)settings.PocketDepth);
            await PlcService.Instance.WriteValueAsync(mDeviceToPocketOffsetZTagModel, (float)settings.DeviceToPocketOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPlaceTrayToFirstPocketOffsetXTagModel, (float)settings.TrayToFirstPocketOffsetX);
            await PlcService.Instance.WriteValueAsync(mPlaceTrayToFirstPocketOffsetYTagModel, (float)settings.TrayToFirstPocketOffsetY);
            await PlcService.Instance.WriteValueAsync(mDeviceSizeZTagModel, (float)settings.DeviceSizeZ);
        }

        public async void UpdateSettings(RobotDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mProductPresentVacuumDelayTagModel, (short)settings.ProductPresentVacuumDelay);
            await PlcService.Instance.WriteValueAsync(mProductNotPresentVacuumDelayTagModel, (short)settings.ProductNotPresentVacuumDelay);
            await PlcService.Instance.WriteValueAsync(mBaseCountTagModel, (short)settings.BaseCount);
            await PlcService.Instance.WriteValueAsync(mDefaultVelocityPercentageTagModel, (short)settings.DefaultVelocityPercentage);
            await PlcService.Instance.WriteValueAsync(mTrayType1TagModel, (short)settings.TrayType1);
            await PlcService.Instance.WriteValueAsync(mTrayType2TagModel, (short)settings.TrayType2);
            await PlcService.Instance.WriteValueAsync(mTrayType3TagModel, (short)settings.TrayType3);
            await PlcService.Instance.WriteValueAsync(mTrayType4TagModel, (short)settings.TrayType4);
            await PlcService.Instance.WriteValueAsync(mTrayType5TagModel, (short)settings.TrayType5);
            await PlcService.Instance.WriteValueAsync(mTrayType6TagModel, (short)settings.TrayType6);

            await PlcService.Instance.WriteValueAsync(mTray1TrayOffsetXTagModel, (float)settings.Tray1TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray1TrayOffsetYTagModel, (float)settings.Tray1TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray1TrayOffsetRZTagModel, (float)settings.Tray1TrayOffsetRZ);

            await PlcService.Instance.WriteValueAsync(mTray2TrayOffsetXTagModel, (float)settings.Tray2TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray2TrayOffsetYTagModel, (float)settings.Tray2TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray2TrayOffsetRZTagModel, (float)settings.Tray2TrayOffsetRZ);

            await PlcService.Instance.WriteValueAsync(mTray3TrayOffsetXTagModel, (float)settings.Tray3TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray3TrayOffsetYTagModel, (float)settings.Tray3TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray3TrayOffsetRZTagModel, (float)settings.Tray3TrayOffsetRZ);

            await PlcService.Instance.WriteValueAsync(mTray4TrayOffsetXTagModel, (float)settings.Tray4TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray4TrayOffsetYTagModel, (float)settings.Tray4TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray4TrayOffsetRZTagModel, (float)settings.Tray4TrayOffsetRZ);

            await PlcService.Instance.WriteValueAsync(mTray5TrayOffsetXTagModel, (float)settings.Tray5TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray5TrayOffsetYTagModel, (float)settings.Tray5TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray5TrayOffsetRZTagModel, (float)settings.Tray5TrayOffsetRZ);

            await PlcService.Instance.WriteValueAsync(mTray6TrayOffsetXTagModel, (float)settings.Tray6TrayOffsetX);
            await PlcService.Instance.WriteValueAsync(mTray6TrayOffsetYTagModel, (float)settings.Tray6TrayOffsetY);
            await PlcService.Instance.WriteValueAsync(mTray6TrayOffsetRZTagModel, (float)settings.Tray6TrayOffsetRZ);
        }

        public void ProductRecipeChanged(IProductRecipe productRecipe)
        {
            var subRecipe = Recipe.Lib.Convert.GetSubRecipe<X1.Recipe.Lib.Product.Robot>(productRecipe);

            BaseCount = subRecipe.BaseCount;
            if (subRecipe.Locations.Count > 5)
            {
                TrayType1 = (int)subRecipe.Locations[0];
                TrayType2 = (int)subRecipe.Locations[1];
                TrayType3 = (int)subRecipe.Locations[2];
                TrayType4 = (int)subRecipe.Locations[3];
                TrayType5 = (int)subRecipe.Locations[4];
                TrayType6 = (int)subRecipe.Locations[5];
            }

            DefaultVelocityPercentage = subRecipe.DefaultVelocityPercentage;
            ProductNotPresentVacuumDelay = 25;
            ProductPresentVacuumDelay = 25;

            Tray1TrayOffsetX = subRecipe.Tray1TrayOffsetX;
            Tray1TrayOffsetY = subRecipe.Tray1TrayOffsetY;
            Tray1TrayOffsetRZ = subRecipe.Tray1TrayOffsetRZ;

            Tray2TrayOffsetX = subRecipe.Tray2TrayOffsetX;
            Tray2TrayOffsetY = subRecipe.Tray2TrayOffsetY;
            Tray2TrayOffsetRZ = subRecipe.Tray2TrayOffsetRZ;

            Tray3TrayOffsetX = subRecipe.Tray3TrayOffsetX;
            Tray3TrayOffsetY = subRecipe.Tray3TrayOffsetY;
            Tray3TrayOffsetRZ = subRecipe.Tray3TrayOffsetRZ;

            Tray4TrayOffsetX = subRecipe.Tray4TrayOffsetX;
            Tray4TrayOffsetY = subRecipe.Tray4TrayOffsetY;
            Tray4TrayOffsetRZ = subRecipe.Tray4TrayOffsetRZ;

            Tray5TrayOffsetX = subRecipe.Tray5TrayOffsetX;
            Tray5TrayOffsetY = subRecipe.Tray5TrayOffsetY;
            Tray5TrayOffsetRZ = subRecipe.Tray5TrayOffsetRZ;

            Tray6TrayOffsetX = subRecipe.Tray6TrayOffsetX;
            Tray6TrayOffsetY = subRecipe.Tray6TrayOffsetY;
            Tray6TrayOffsetRZ = subRecipe.Tray6TrayOffsetRZ;

            var settingsDataModel = new RobotDataModel();
            settingsDataModel.BaseCount = BaseCount;

            if (subRecipe.Locations.Count > 5)
            {
                settingsDataModel.TrayType1 = TrayType1;
                settingsDataModel.TrayType2 = TrayType2;
                settingsDataModel.TrayType3 = TrayType3;
                settingsDataModel.TrayType4 = TrayType4;
                settingsDataModel.TrayType5 = TrayType5;
                settingsDataModel.TrayType6 = TrayType6;
            }
            settingsDataModel.DefaultVelocityPercentage = DefaultVelocityPercentage;
            settingsDataModel.ProductNotPresentVacuumDelay = ProductNotPresentVacuumDelay;
            settingsDataModel.ProductPresentVacuumDelay = ProductPresentVacuumDelay;

            settingsDataModel.Tray1TrayOffsetX = Tray1TrayOffsetX;
            settingsDataModel.Tray1TrayOffsetY = Tray1TrayOffsetY;
            settingsDataModel.Tray1TrayOffsetRZ = Tray1TrayOffsetRZ;

            settingsDataModel.Tray2TrayOffsetX = Tray2TrayOffsetX;
            settingsDataModel.Tray2TrayOffsetY = Tray2TrayOffsetY;
            settingsDataModel.Tray2TrayOffsetRZ = Tray2TrayOffsetRZ;

            settingsDataModel.Tray3TrayOffsetX = Tray3TrayOffsetX;
            settingsDataModel.Tray3TrayOffsetY = Tray3TrayOffsetY;
            settingsDataModel.Tray3TrayOffsetRZ = Tray3TrayOffsetRZ;

            settingsDataModel.Tray4TrayOffsetX = Tray4TrayOffsetX;
            settingsDataModel.Tray4TrayOffsetY = Tray4TrayOffsetY;
            settingsDataModel.Tray4TrayOffsetRZ = Tray4TrayOffsetRZ;

            settingsDataModel.Tray5TrayOffsetX = Tray5TrayOffsetX;
            settingsDataModel.Tray5TrayOffsetY = Tray5TrayOffsetY;
            settingsDataModel.Tray5TrayOffsetRZ = Tray5TrayOffsetRZ;

            settingsDataModel.Tray6TrayOffsetX = Tray6TrayOffsetX;
            settingsDataModel.Tray6TrayOffsetY = Tray6TrayOffsetY;
            settingsDataModel.Tray6TrayOffsetRZ = Tray6TrayOffsetRZ;

            UpdateSettings(settingsDataModel);
        }

        public async void ExecuteRetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryCommandTagModel, true);
        }

       
    }
}
