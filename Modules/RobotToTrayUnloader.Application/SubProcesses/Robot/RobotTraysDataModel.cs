﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotTraysDataModel
    {
        public PositionDataModel HomePosition { get; set; }
        public RobotTraysDataModel()
        {
            HomePosition = new PositionDataModel();
        }
    }
   
}
