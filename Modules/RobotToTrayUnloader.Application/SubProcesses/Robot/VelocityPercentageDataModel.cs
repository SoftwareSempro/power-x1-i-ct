﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class VelocityPercentageDataModel
    {
       public int ApproachVelocityPercentage { get; set; }
       public int ClearVelocityPercentage { get; set; }

    }
}
