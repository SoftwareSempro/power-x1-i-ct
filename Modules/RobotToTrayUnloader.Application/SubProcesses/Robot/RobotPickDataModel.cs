﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotPickDataModel
    {
        public PositionDataModel PickPosition { get; set; }
        public double PickOffsetZ { get; set; }
        public VelocityPercentageDataModel PickVelocityPercentage { get; set; }
        public RobotPickDataModel()
        {
            PickPosition = new PositionDataModel();
            PickVelocityPercentage = new VelocityPercentageDataModel();
        }
    }  
}
