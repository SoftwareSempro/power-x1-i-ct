﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
   public class PositionDataModel
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double R { get; set; }

    }
}
