﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotDataModel
    {      
      
        public int BaseCount { get; set; }
        public int DefaultVelocityPercentage { get; set; }
        public int ProductPresentVacuumDelay { get; set; }
        public int ProductNotPresentVacuumDelay { get; set; }
        public int TrayType1 { get; set; }
        public double Tray1TrayOffsetX { get; set; }
        public double Tray1TrayOffsetY { get; set; }
        public double Tray1TrayOffsetRZ { get; set; }
        public int TrayType2 { get; set; }
        public double Tray2TrayOffsetX { get; set; }
        public double Tray2TrayOffsetY { get; set; }
        public double Tray2TrayOffsetRZ { get; set; }
        public int TrayType3 { get; set; }
        public double Tray3TrayOffsetX { get; set; }
        public double Tray3TrayOffsetY { get; set; }
        public double Tray3TrayOffsetRZ { get; set; }
        public int TrayType4 { get; set; }
        public double Tray4TrayOffsetX { get; set; }
        public double Tray4TrayOffsetY { get; set; }
        public double Tray4TrayOffsetRZ { get; set; }
        public int TrayType5 { get; set; }
        public double Tray5TrayOffsetX { get; set; }
        public double Tray5TrayOffsetY { get; set; }
        public double Tray5TrayOffsetRZ { get; set; }
        public int TrayType6 { get; set; }
               
        public double Tray6TrayOffsetX { get; set; }
        public double Tray6TrayOffsetY { get; set; }
        public double Tray6TrayOffsetRZ { get; set; }

    }
}
