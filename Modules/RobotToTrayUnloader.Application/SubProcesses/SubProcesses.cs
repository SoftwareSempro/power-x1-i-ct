﻿using RobotTrayUnloader.Application.SubProcesses;

namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class SubProcesses
    {
        public Inspection Inspection { get; } = new();
        public RobotProcess RobotProcess { get; } = new();
        public TeachPlacePositions TeachPlacePositions { get; } = new();
    }
}
