﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class Inspection : SubProcess
    {
        private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPc.Status);

        private readonly TagModel mInfeedPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.InfeedPosition);
        private readonly TagModel mOutfeedPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.OutfeedPosition);
        private readonly TagModel mToInspectionVelocityPositionOffsetTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.ToInspectionVelocityPositionOffset);
        private readonly TagModel mStartInspectionPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.StartInspectionPosition);
        private readonly TagModel mStopInspectionPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.StopInspectionPosition);
        private readonly TagModel mVelocityTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.Velocity);
        private readonly TagModel mInspectionVelocityTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.InspectionVelocity);
        private readonly TagModel mNoAOITagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.NoAoi);
        private readonly TagModel mInspectionCommandNumberTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessInterface.ToPlc.Inspection.CommandNumber);
        
        private InspectionDataModel mSettings;
        
        public event EventHandler<InspectionDataModel> SettingsChanged;        
        public InspectionDataModel Settings => mSettings;
               
        public double InfeedPosition
        {
            get => (float)mSettings.InfeedPosition;
            set
            {
                if (mSettings.InfeedPosition != value)
                {
                    mSettings.InfeedPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double OutfeedPosition
        {
            get => (float)mSettings.OutfeedPosition;
            set
            {
                if (mSettings.OutfeedPosition != value)
                {
                    mSettings.OutfeedPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double ToInspectionVelocityPositionOffset
        {
            get => (float)mSettings.ToInspectionVelocityPositionOffset;
            set
            {
                if (mSettings.ToInspectionVelocityPositionOffset != value)
                {
                    mSettings.ToInspectionVelocityPositionOffset = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StartInspectionPosition
        {
            get => (float)mSettings.StartInspectionPosition;
            set
            {
                if (mSettings.StartInspectionPosition != value)
                {
                    mSettings.StartInspectionPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StopInspectionPosition
        {
            get => (float)mSettings.StopInspectionPosition;
            set
            {
                if (mSettings.StopInspectionPosition != value)
                {
                    mSettings.StopInspectionPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int Velocity
        {
            get => mSettings.Velocity;
            set
            {
                if (mSettings.Velocity != value)
                {
                    mSettings.Velocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int InspectionVelocity
        {
            get => mSettings.InspectionVelocity;
            set
            {
                if (mSettings.InspectionVelocity != value)
                {
                    mSettings.InspectionVelocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public bool NoAOI
        {
            get => mSettings.NoAOI;
            set
            {
                if (mSettings.NoAOI != value)
                {
                    mSettings.NoAOI = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int InspectionCommandNumber
        {
            get => mSettings.InspectionCommandNumber;
            set
            {
                if (mSettings.InspectionCommandNumber != value)
                {
                    mSettings.InspectionCommandNumber = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public Inspection()
        {
            mSettings = new InspectionDataModel();

            TagModelPathDictionary = new()
            {
                 { StatusTagModel, nameof(Status)},
            };
        }

        public async void UpdateSettings(InspectionDataModel settings)
        {
            InfeedPosition = settings.InfeedPosition;
            OutfeedPosition = settings.OutfeedPosition;
            ToInspectionVelocityPositionOffset = settings.ToInspectionVelocityPositionOffset;
            InspectionVelocity = settings.InspectionVelocity;
            StopInspectionPosition = settings.StopInspectionPosition;
            Velocity = settings.Velocity;
            InspectionVelocity = settings.InspectionVelocity;
            NoAOI = settings.NoAOI;
            InspectionCommandNumber = settings.InspectionCommandNumber;

            await PlcService.Instance.WriteValueAsync(mInfeedPositionTagModel, (float)settings.InfeedPosition);
            await PlcService.Instance.WriteValueAsync(mOutfeedPositionTagModel, (float)settings.OutfeedPosition);
            await PlcService.Instance.WriteValueAsync(mToInspectionVelocityPositionOffsetTagModel, (float)settings.ToInspectionVelocityPositionOffset);
            await PlcService.Instance.WriteValueAsync(mStartInspectionPositionTagModel, (float)settings.StartInspectionPosition);
            await PlcService.Instance.WriteValueAsync(mStopInspectionPositionTagModel, (float) settings.StopInspectionPosition);
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, (short) settings.Velocity);
            await PlcService.Instance.WriteValueAsync(mInspectionVelocityTagModel, (short) settings.InspectionVelocity);
            await PlcService.Instance.WriteValueAsync(mNoAOITagModel, settings.NoAOI);
            await PlcService.Instance.WriteValueAsync(mInspectionCommandNumberTagModel, (short) settings.InspectionCommandNumber);

        }
    }
}
