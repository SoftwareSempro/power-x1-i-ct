﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
   public class InspectionDataModel
    {
        public double InfeedPosition { get; set; }
        public double OutfeedPosition { get; set; }
        public double ToInspectionVelocityPositionOffset { get; set; }  
        public double StartInspectionPosition { get; set; }
        public double StopInspectionPosition { get; set; }
        public int Velocity { get; set; }
        public int InspectionVelocity { get; set; }
        public bool NoAOI { get; set; }
        public int InspectionCommandNumber { get; set; }

    }
}
