﻿using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using System;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System.Collections.Generic;
using RobotTrayUnloader.Application;
using RecipeManager;
using X1.Recipe.Lib;
using RobotToTrayUnloader.Application.SubProcesses;
using RobotTrayUnloader.Lib;
using Recipe.Lib;
using X1.Recipe.Lib.Machine;
using Platform.Application;

namespace RobotToTrayUnloader.Application
{
    public class RobotToTrayUnloader : MainProcess
    {    
       
        private static RobotToTrayUnloader mRobotTrayUnloader;       

        private readonly TagModel mRobotTrayUnloaderStatusTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.Status);
        private readonly TagModel mRobotTrayUnloaderStateTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.State);
        private readonly TagModel mRobotTrayUnloaderIsRunningProductionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.IsRunningProduction);
        private readonly TagModel mRobotTrayUnloaderHasErrorTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.HasError);
        private readonly TagModel mRobotTrayUnloaderInSimulationTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.InSimulation);

        private readonly TagModel mInspectCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.InspectCommand);
        private readonly TagModel mInspectToInfeedCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.InspectToInfeedCommand);
        private readonly TagModel mPickCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.PickCommand);
        private readonly TagModel mPlaceCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.PlaceCommand);
        private readonly TagModel mHomeCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.HomeCommand);
        private readonly TagModel mClearMaterial = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.ClearMaterial);

        private readonly LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;
        public static RobotToTrayUnloader Instance => mRobotTrayUnloader ?? (mRobotTrayUnloader = new RobotToTrayUnloader());
        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();
        public MaterialTracking.MaterialTracking MaterialTracking { get; } = new MaterialTracking.MaterialTracking();
        public EventHandler EventHandler { get; } = new();

        public RobotToTrayUnloader()
            : base(nameof(RobotToTrayUnloader), RobotTrayUnloaderEvents.RobotToTrayUnloaderInSimulation.ID)
        {
            mInitializeCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.InitializeCommand);
            mStartCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.StartCommand);
            mStopCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.StopCommand);

            mStatusTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.Status);
            mStateTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.State);
            mHasErrorTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.HasError);
            mIsRunningProductionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.IsRunningProduction);
            mInSimulationTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPc.InSimulation);

            mEnableSimulationTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.EnableSimulation);
            mVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderInterface.ToPlc.VelocityPercentage);

            AddTagModelDictionary(() => Resources.Camera3dController, Resources.Camera3dController.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.Robot, Resources.Robot.TagModelPathDictionary);
            AddTagModelDictionary(() => Resources.ScanningStage, Resources.ScanningStage.TagModelPathDictionary);

            AddTagModelDictionary(GetNameOf(() => SubProcesses.Inspection), SubProcesses.Inspection.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.RobotProcess), SubProcesses.RobotProcess.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.TeachPlacePositions), SubProcesses.TeachPlacePositions.TagModelPathDictionary);

            AddTagModelDictionary(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(EventHandler), EventHandler.TagModelPathDictionary);

            AddSubscription();

            mEventManager.RegisterEvents(RobotTrayUnloaderEvents.Events);

        }

        protected override void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }        

        public async void ClearMaterial()
        {
            mLog.Debug($"Clear material: RobotToTrayUnloader");
           await mPLCService.WriteValueAsync(mClearMaterial, true);
        }

        public void Inspect()
        {
            mLog.Debug($"Inspect: RobotToTrayUnloader");
            mPLCService.WriteValue(mInspectCommandTagModel, true);
        }

        public void InspectToInfeed()
        {
            mLog.Debug($"InspectToInfeed: RobotToTrayUnloader");
            mPLCService.WriteValue(mInspectToInfeedCommandTagModel, true);
        }

        public void Pick()
        {
            mLog.Debug($"Pick: RobotToTrayUnloader");
            mPLCService.WriteValue(mPickCommandTagModel, true);
        }

        public void Place()
        {
            mLog.Debug($"Place: RobotToTrayUnloader");
            mPLCService.WriteValue(mPlaceCommandTagModel, true);
        }

        public void Home()
        {
            mLog.Debug($"Home: RobotToTrayUnloader");
            mPLCService.WriteValue(mHomeCommandTagModel, true);
        }
    }
}
