using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using System;
using MaterialTracking.Lib;

namespace RobotToTrayUnloader.Application.MaterialTracking
{
    public abstract class BaseTrayMaterialTracking : SubscriptionBase
    {
        public event Action HasMaterialChanged;
        public event EventHandler<bool> TrayHasMaterialChanged;
        public event EventHandler<int> TrayIdChanged;

        private bool mHasMaterial;
        private int mTrayId;
        private TrayType mTrayType;

        #region Members device positions

        private int mDevicePosition12Id;
        private bool mDevicePosition12HasMaterial;
        private int mDevicePosition11Id;
        private bool mDevicePosition11HasMaterial;
        private int mDevicePosition10Id;
        private bool mDevicePosition10HasMaterial;
        private int mDevicePosition9Id;
        private bool mDevicePosition9HasMaterial;
        private int mDevicePosition8Id;
        private bool mDevicePosition8HasMaterial;
        private int mDevicePosition7Id;
        private bool mDevicePosition7HasMaterial;
        private int mDevicePosition6Id;
        private bool mDevicePosition6HasMaterial;
        private int mDevicePosition5Id;
        private bool mDevicePosition5HasMaterial;
        private int mDevicePosition4Id;
        private bool mDevicePosition4HasMaterial;
        private int mDevicePosition3Id;
        private bool mDevicePosition3HasMaterial;
        private int mDevicePosition2Id;
        private bool mDevicePosition2HasMaterial;
        private int mDevicePosition1Id;
        private bool mDevicePosition1HasMaterial;


        protected virtual TagModel DevicePosition1HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition1IdTagModel{ get; }
        protected virtual TagModel DevicePosition2HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition2IdTagModel{ get; }
        protected virtual TagModel DevicePosition3HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition3IdTagModel{ get; }
        protected virtual TagModel DevicePosition4HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition4IdTagModel{ get; }
        protected virtual TagModel DevicePosition5HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition5IdTagModel{ get; }
        protected virtual TagModel DevicePosition6HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition6IdTagModel{ get; }
        protected virtual TagModel DevicePosition7HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition7IdTagModel{ get; }
        protected virtual TagModel DevicePosition8HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition8IdTagModel{ get; }
        protected virtual TagModel DevicePosition9HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition9IdTagModel { get; }
        protected virtual TagModel DevicePosition10HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition10IdTagModel{ get; }
        protected virtual TagModel DevicePosition11HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition11IdTagModel { get; }
        protected virtual TagModel DevicePosition12HasMaterialTagModel { get; }
        protected virtual TagModel DevicePosition12IdTagModel{ get; }

        #endregion

        protected virtual TagModel HasMaterialTagModel { get; }
        protected virtual TagModel TrayIdTagModel { get; }
        protected virtual TagModel TrayTypeTagModel { get; }

        private readonly MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private readonly LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;

        #region Tray
        public bool HasMaterial
        {
            get => mHasMaterial;
            set
            {
                if (mHasMaterial != value)
                {
                    mHasMaterial = value;
                    TrayHasMaterialChanged?.Invoke(this, mHasMaterial);
                }
            }
        }

        public int TrayId
        {
            get => mTrayId;
            set
            {
                if (mTrayId != value)
                {
                    mTrayId = value;
                    TrayIdChanged?.Invoke(this, mTrayId);
                    TrayType = (TrayType)Enum.ToObject(typeof(TrayType), mPlcService.ReadValue(TrayTypeTagModel));

                    if (mLotControl.CurrentLotId != 0 && mTrayId != 0)
                        mLotControl.AddTrayToLot(mTrayId, TrayType);
                }
            }
        }

        public TrayType TrayType
        {
            get => mTrayType;
            set
            {
                if (mTrayType != value)
                {
                    mTrayType = value;
                }
            }
        }

        #endregion

        #region Device locations

        public bool DevicePosition1HasMaterial
        {
            get => mDevicePosition1HasMaterial;
            set
            {
                if (mDevicePosition1HasMaterial != value)
                {
                    mDevicePosition1HasMaterial = value;
                    if (value)
                        DevicePosition1Id = (int) mPlcService.ReadValue(DevicePosition1IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition1Id
        {
            get => mDevicePosition1Id;
            set
            {
                if (mDevicePosition1Id != value)
                {
                    mDevicePosition1Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition1Id, 0, 0, mLotControl.CurrentLotId);
                }
            }
        }

        public bool DevicePosition2HasMaterial
        {
            get => mDevicePosition2HasMaterial;
            set
            {
                if (mDevicePosition2HasMaterial != value)
                {
                    mDevicePosition2HasMaterial = value;
                    if (value)
                        DevicePosition2Id = (int)mPlcService.ReadValue(DevicePosition2IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition2Id
        {
            get => mDevicePosition2Id;
            set
            {
                if (mDevicePosition2Id != value)
                {
                    mDevicePosition2Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition2Id, 1, 0, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition3HasMaterial
        {
            get => mDevicePosition3HasMaterial;
            set
            {
                if (mDevicePosition3HasMaterial != value)
                {
                    mDevicePosition3HasMaterial = value;
                    if (value)
                        DevicePosition3Id = (int)mPlcService.ReadValue(DevicePosition3IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition3Id
        {
            get => mDevicePosition3Id;
            set
            {
                if (mDevicePosition3Id != value)
                {
                    mDevicePosition3Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition3Id, 2, 0, mLotControl.CurrentLotId);
                }
            }
        }

        public bool DevicePosition4HasMaterial
        {
            get => mDevicePosition4HasMaterial;
            set
            {
                if (mDevicePosition4HasMaterial != value)
                {
                    mDevicePosition4HasMaterial = value;
                    if (value)
                        DevicePosition4Id = (int)mPlcService.ReadValue(DevicePosition4IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition4Id
        {
            get => mDevicePosition4Id;
            set
            {
                if (mDevicePosition4Id != value)
                {
                    mDevicePosition4Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition4Id, 3, 0, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition5HasMaterial
        {
            get => mDevicePosition5HasMaterial;
            set
            {
                if (mDevicePosition5HasMaterial != value)
                {
                    mDevicePosition5HasMaterial = value;
                    if (value)
                        DevicePosition5Id = (int)mPlcService.ReadValue(DevicePosition5IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition5Id
        {
            get => mDevicePosition5Id;
            set
            {
                if (mDevicePosition5Id != value)
                {
                    mDevicePosition5Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition5Id, 0, 1, mLotControl.CurrentLotId);
                }
            }
        }

        public bool DevicePosition6HasMaterial
        {
            get => mDevicePosition6HasMaterial;
            set
            {
                if (mDevicePosition6HasMaterial != value)
                {
                    mDevicePosition6HasMaterial = value;
                    if (value)
                        DevicePosition6Id = (int)mPlcService.ReadValue(DevicePosition6IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition6Id
        {
            get => mDevicePosition6Id;
            set
            {
                if (mDevicePosition6Id != value)
                {
                    mDevicePosition6Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition6Id, 1, 1, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition7HasMaterial
        {
            get => mDevicePosition7HasMaterial;
            set
            {
                if (mDevicePosition7HasMaterial != value)
                {
                    mDevicePosition7HasMaterial = value;
                    if (value)
                        DevicePosition7Id = (int)mPlcService.ReadValue(DevicePosition7IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition7Id
        {
            get => mDevicePosition7Id;
            set
            {
                if (mDevicePosition7Id != value)
                {
                    mDevicePosition7Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition7Id, 2, 1, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition8HasMaterial
        {
            get => mDevicePosition8HasMaterial;
            set
            {
                if (mDevicePosition8HasMaterial != value)
                {
                    mDevicePosition8HasMaterial = value;
                    if (value)
                        DevicePosition8Id = (int)mPlcService.ReadValue(DevicePosition8IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition8Id
        {
            get => mDevicePosition8Id;
            set
            {
                if (mDevicePosition8Id != value)
                {
                    mDevicePosition8Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition8Id, 3, 1, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition9HasMaterial
        {
            get => mDevicePosition9HasMaterial;
            set
            {
                if (mDevicePosition9HasMaterial != value)
                {
                    mDevicePosition9HasMaterial = value;
                    if (value)
                        DevicePosition9Id = (int)mPlcService.ReadValue(DevicePosition9IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition9Id
        {
            get => mDevicePosition9Id;
            set
            {
                if (mDevicePosition9Id != value)
                {
                    mDevicePosition9Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition9Id, 0, 2, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition10HasMaterial
        {
            get => mDevicePosition10HasMaterial;
            set
            {
                if (mDevicePosition10HasMaterial != value)
                {
                    mDevicePosition10HasMaterial = value;
                    if (value)
                        DevicePosition10Id = (int)mPlcService.ReadValue(DevicePosition10IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition10Id
        {
            get => mDevicePosition10Id;
            set
            {
                if (mDevicePosition10Id != value)
                {
                    mDevicePosition10Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition10Id, 1, 2, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition11HasMaterial
        {
            get => mDevicePosition11HasMaterial;
            set
            {
                if (mDevicePosition11HasMaterial != value)
                {
                    mDevicePosition11HasMaterial = value;
                    if (value)
                        DevicePosition11Id = (int)mPlcService.ReadValue(DevicePosition11IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition11Id
        {
            get => mDevicePosition11Id;
            set
            {
                if (mDevicePosition11Id != value)
                {
                    mDevicePosition11Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition11Id, 2, 2, mLotControl.CurrentLotId);
                }
            }
        }
        public bool DevicePosition12HasMaterial
        {
            get => mDevicePosition12HasMaterial;
            set
            {
                if (mDevicePosition12HasMaterial != value)
                {
                    mDevicePosition12HasMaterial = value;
                    if (value)
                        DevicePosition12Id = (int)mPlcService.ReadValue(DevicePosition12IdTagModel);
                    HasMaterialChanged?.Invoke();
                }
            }
        }

        public int DevicePosition12Id
        {
            get => mDevicePosition12Id;
            set
            {
                if (mDevicePosition12Id != value)
                {
                    mDevicePosition12Id = value;
                    mMaterialTracking.AddDeviceToTray(mTrayId, mDevicePosition12Id, 3, 2, mLotControl.CurrentLotId);
                }
            }
        }

        #endregion

        public BaseTrayMaterialTracking()
        {
            TagModelPathDictionary = new()
            {
                {HasMaterialTagModel,nameof(HasMaterial)},
                {TrayIdTagModel,nameof(TrayId)},
                {TrayTypeTagModel,nameof(TrayType)},
                {DevicePosition1HasMaterialTagModel,nameof(DevicePosition1HasMaterial)},
                {DevicePosition2HasMaterialTagModel,nameof(DevicePosition2HasMaterial)},
                {DevicePosition3HasMaterialTagModel,nameof(DevicePosition3HasMaterial)},
                {DevicePosition4HasMaterialTagModel,nameof(DevicePosition4HasMaterial)},
                {DevicePosition5HasMaterialTagModel,nameof(DevicePosition5HasMaterial)},
                {DevicePosition6HasMaterialTagModel,nameof(DevicePosition6HasMaterial)},
                {DevicePosition7HasMaterialTagModel,nameof(DevicePosition7HasMaterial)},
                {DevicePosition8HasMaterialTagModel,nameof(DevicePosition8HasMaterial)},
                {DevicePosition9HasMaterialTagModel,nameof(DevicePosition9HasMaterial)},
                {DevicePosition10HasMaterialTagModel,nameof(DevicePosition10HasMaterial)},
                {DevicePosition11HasMaterialTagModel,nameof(DevicePosition11HasMaterial)},
                {DevicePosition12HasMaterialTagModel,nameof(DevicePosition12HasMaterial)},    
            };
        }
    }
}
