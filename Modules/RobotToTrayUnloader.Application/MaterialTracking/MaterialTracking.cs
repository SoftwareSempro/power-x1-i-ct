﻿using LotControl.Application;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using System.Linq;

namespace RobotToTrayUnloader.Application.MaterialTracking
{
    public class MaterialTracking : SubscriptionBase
    {
        #region Events

        public event EventHandler<int> InfeedHasMaterialChanged;
        public event EventHandler<int> RobotHasMaterialChanged;

        public event EventHandler<HasTrayChangedDataModel> TrayIdChanged;

        #endregion

        #region members

        MaterialTrackingApplication mMaterialTracking;
        LotControl.Application.LotControl mLotControl;

        private bool mInfeedHasMaterial;
        private bool mRobotHasMaterial;
        private int mInspectionProcessState;
        private int mInspection3DResult;

        public Tray1MaterialTracking Tray1MaterialTracking { get; set; } = new();
        public Tray2MaterialTracking Tray2MaterialTracking { get; set; } = new();
        public Tray3MaterialTracking Tray3MaterialTracking { get; set; } = new();
        public Tray4MaterialTracking Tray4MaterialTracking { get; set; } = new();
        public Tray5MaterialTracking Tray5MaterialTracking { get; set; } = new();
        public Tray6MaterialTracking Tray6MaterialTracking { get; set; } = new();

        private readonly TagModel mInfeedHasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.HasMaterial);
        private readonly TagModel mInfeedIdTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.Id);
        private readonly TagModel mRobotHasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.RobotDevicePosition.HasMaterial);
        private readonly TagModel mRobotIdTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.RobotDevicePosition.Device.Id);

        private readonly TagModel mInspectionProcessStateTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.ProcessedState);

        private readonly TagModel mInspection3DResultTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.InspectionResult);
     
        #endregion

        #region Properties robot

        public bool InfeedHasMaterial
        {
            get => mInfeedHasMaterial;
            set
            {
                if (mInfeedHasMaterial != value)
                {
                    mInfeedHasMaterial = value;
                    InfeedHasMaterialChanged?.Invoke(this, GetInfeedPositionDeviceId());
                }
            }
        }


        public int InspectionProcessState
        {
            get => mInspectionProcessState;
            set
            {
                if (mInspectionProcessState != value)
                {
                    mInspectionProcessState = value;

                    if (value == (int)DeviceProcessState.InspectedFinal)
                    {
                        var id = GetInfeedPositionDeviceId();

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mInspectionProcessState);
                        }
                    }
                }
            }
        }

        public int Inspection3DResult
        {
            get => mInspection3DResult;
            set
            {
                if (mInspection3DResult != value)
                {
                    mInspection3DResult = value;

                    if (value != (int)ValidationResult.None)
                    {
                        var id = GetInfeedPositionDeviceId();


                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, DeviceProcessTypeResult.Inspection3DResult, (InspectionResult)mInspection3DResult);
                        }
                    }
                }
            }
        }


        public bool RobotHasMaterial
        {
            get => mRobotHasMaterial;
            set
            {
                if (mRobotHasMaterial != value)
                {
                    mRobotHasMaterial = value;
                    RobotHasMaterialChanged?.Invoke(this, GetRobotPositionDeviceId());
                }
            }
        }

        #endregion

        public int Tray1Id { get { return Tray1MaterialTracking.TrayId; } }
        public int Tray2Id { get { return Tray2MaterialTracking.TrayId; } }
        public int Tray3Id { get { return Tray3MaterialTracking.TrayId; } }
        public int Tray4Id { get { return Tray4MaterialTracking.TrayId; } }
        public int Tray5Id { get { return Tray5MaterialTracking.TrayId; } }
        public int Tray6Id { get { return Tray6MaterialTracking.TrayId; } }


        #region Constructor

        public MaterialTracking()
        {
            mMaterialTracking = MaterialTrackingApplication.Instance;
            mLotControl = LotControl.Application.LotControl.Instance;

            TagModelPathDictionary = new()
            {
                { mInfeedHasMaterialTagModel, nameof(InfeedHasMaterial) },
                { mRobotHasMaterialTagModel, nameof(RobotHasMaterial) },
                { mInspectionProcessStateTagModel, nameof(InspectionProcessState) },
                { mInspection3DResultTagModel, nameof(Inspection3DResult) },
            };
            mLotControl.LotOpened += LotControl_LotOpened;

            AddTagModelDictionary(nameof(Tray1MaterialTracking), Tray1MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(Tray2MaterialTracking), Tray2MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(Tray3MaterialTracking), Tray3MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(Tray4MaterialTracking), Tray4MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(Tray5MaterialTracking), Tray5MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(Tray6MaterialTracking), Tray6MaterialTracking.TagModelPathDictionary);

            Tray1MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(1,e);
            Tray2MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(2,e);
            Tray3MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(3,e);
            Tray4MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(4,e);
            Tray5MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(5,e);
            Tray6MaterialTracking.TrayIdChanged += (sender, e) => TrayMaterialTracking_TrayIdChanged(6,e);
        }

        private void TrayMaterialTracking_TrayIdChanged(int position, int id)
        {
            TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel((TrayPosition)position, id));
        }

        private void LotControl_LotOpened(object sender, string e)
        {
            //When lot is opened and there are already trays in the equipment
            if (Tray1MaterialTracking.HasMaterial)
            {              
                mLotControl.AddTrayToLot(Tray1Id, Tray1MaterialTracking.TrayType);
            }

            if (Tray2MaterialTracking.HasMaterial)
            {               
                mLotControl.AddTrayToLot(Tray2Id, Tray2MaterialTracking.TrayType);
            }

            if (Tray3MaterialTracking.HasMaterial)
            {               
                mLotControl.AddTrayToLot(Tray3Id, Tray3MaterialTracking.TrayType);
            }

            if (Tray4MaterialTracking.HasMaterial)
            {              
                mLotControl.AddTrayToLot(Tray4Id, Tray4MaterialTracking.TrayType);
            }

            if (Tray5MaterialTracking.HasMaterial)
            {           
                mLotControl.AddTrayToLot(Tray5Id, Tray5MaterialTracking.TrayType);
            }

            if (Tray6MaterialTracking.HasMaterial)
            {             
                mLotControl.AddTrayToLot(Tray6Id, Tray6MaterialTracking.TrayType);
            }
        }

        #endregion

        public int GetInfeedPositionDeviceId()
        {
            try
            {
                return (int)mPlcService.ReadValue(mInfeedIdTagModel);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int GetRobotPositionDeviceId()
        {
            try
            {
                return (int)mPlcService.ReadValue(mRobotIdTagModel);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private int GetDeviceCount(bool[] trayDevicePositionsHasMaterial)
        {
            return trayDevicePositionsHasMaterial.Where(item => item).Count();
        }
    }
}
