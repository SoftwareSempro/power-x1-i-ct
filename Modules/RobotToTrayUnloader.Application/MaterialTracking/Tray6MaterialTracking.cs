using MachineManager.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;

namespace RobotToTrayUnloader.Application.MaterialTracking
{
    public class Tray6MaterialTracking : BaseTrayMaterialTracking
    {
        #region Members device positions

        protected override TagModel DevicePosition1HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        protected override TagModel DevicePosition1IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation1.Device.Id);
        protected override TagModel DevicePosition2HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        protected override TagModel DevicePosition2IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation2.Device.Id);
        protected override TagModel DevicePosition3HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        protected override TagModel DevicePosition3IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation3.Device.Id);
        protected override TagModel DevicePosition4HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        protected override TagModel DevicePosition4IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation4.Device.Id);
        protected override TagModel DevicePosition5HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        protected override TagModel DevicePosition5IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation5.Device.Id);
        protected override TagModel DevicePosition6HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        protected override TagModel DevicePosition6IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation6.Device.Id);
        protected override TagModel DevicePosition7HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        protected override TagModel DevicePosition7IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation7.Device.Id);
        protected override TagModel DevicePosition8HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        protected override TagModel DevicePosition8IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation8.Device.Id);
        protected override TagModel DevicePosition9HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        protected override TagModel DevicePosition9IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation9.Device.Id);
        protected override TagModel DevicePosition10HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        protected override TagModel DevicePosition10IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation10.Device.Id);
        protected override TagModel DevicePosition11HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        protected override TagModel DevicePosition11IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation11.Device.Id);
        protected override TagModel DevicePosition12HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);
        protected override TagModel DevicePosition12IdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation12.Device.Id);
        
        #endregion

        protected override TagModel HasMaterialTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.HasTray);
        protected override TagModel TrayIdTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.ID);
        protected override TagModel TrayTypeTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.Type);
       
        public Tray6MaterialTracking()
            : base()
        {
            
        }
    }
}
