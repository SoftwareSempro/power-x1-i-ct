using MachineManager.Lib;
using Platform;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;

namespace RobotToTrayUnloader.Application.Resources
{
    public class Camera3dController : Resource
    {
        private int mCamera3dControllerStatus;
        private bool mControllerReady;
        private bool mControllerBusy;
        private bool mControllerIsRunning;
        private int mResult;
        private int mCommandNumber;
        public Action SensorStatusChanged;
        public Action ResultsChanged;

        private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.Status);
        private static readonly  TagModel mControllerReadyTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.ControllerReady);
        private static readonly TagModel mControllerBusyTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.ControllerBusy);
        private static readonly TagModel mControllerIsRunningTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.ControllerIsRunning);
        private static readonly TagModel mResultTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.Result);
        private static readonly TagModel mCommandNumberTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloader3dCameraControllerInterface.ToPc.CommandNumber);


        public bool ControllerReady
        {
            get => mControllerReady;
            set
            {
                if (mControllerReady != value)
                {
                    mControllerReady = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }
        public bool ControllerBusy
        {
            get => mControllerBusy;
            set
            {
                if (mControllerBusy != value)
                {
                    mControllerBusy = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }
        public bool ControllerIsRunning
        {
            get => mControllerIsRunning;
            set
            {
                if (mControllerIsRunning != value)
                {
                    mControllerIsRunning = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }
        public int Result
        {
            get => mResult;
            set
            {
                if (mResult != value)
                {
                    mResult = value;
                    ResultsChanged?.Invoke();
                }
            }
        }
        public int CommandNumber
        {
            get => mCommandNumber;
            set
            {
                if (mCommandNumber != value)
                {
                    mCommandNumber = value;
                    ResultsChanged?.Invoke();
                }
            }
        }
        public Camera3dController()
        {
            TagModelPathDictionary = new()
            {
                { StatusTagModel, nameof(Status) },
                { mControllerReadyTagModel, nameof(ControllerReady) },
                { mControllerBusyTagModel, nameof(ControllerBusy) },
                { mControllerIsRunningTagModel, nameof(ControllerIsRunning) },
                { mResultTagModel, nameof(Result) },
                { mCommandNumberTagModel, nameof(CommandNumber) },
            };
        }
    }
}
