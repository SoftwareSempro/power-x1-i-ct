﻿namespace RobotToTrayUnloader.Application.Resources
{
   public  class Resources
    {
        public Camera3dController Camera3dController { get; } = new Camera3dController();
        public ScanningStage ScanningStage { get; } = new ScanningStage();
        public Robot Robot { get; } = new Robot();
    }
}
