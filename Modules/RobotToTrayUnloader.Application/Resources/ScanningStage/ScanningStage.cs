﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;

namespace RobotToTrayUnloader.Application.Resources
{
    public class ScanningStage : Resource
    {
        private int mScanningStageStatus;
        private double mActualPositionX;
        private bool mProductAtScanningStage;

        
        public event EventHandler<double> PositionXChanged;
        public Action SensorStatusChanged;

        private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPc.Status);
        private static readonly TagModel mActualPositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPc.ActualPositionX);
        private static readonly TagModel mProductAtScanningStageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPc.ProductAtScanningStage);

        private static readonly TagModel mGoToTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPlc.GoToCommand);
        private static readonly TagModel mSetPointXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPlc.SetPointX);
        private static readonly TagModel mVelocityTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStageInterface.ToPlc.Velocity);

        public double ActualPositionX
        {
            get => mActualPositionX;
            private set
            {
                if (mActualPositionX != value)
                {
                    mActualPositionX = value;
                    PositionXChanged?.Invoke(this, mScanningStageStatus);
                }
            }
        }

        public bool ProductAtScanningStage
        {
            get => mProductAtScanningStage;
            private set
            {
                if (mProductAtScanningStage != value)
                {
                    mProductAtScanningStage = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public ScanningStage()
        {
            TagModelPathDictionary = new()
            {
                {StatusTagModel, nameof(Status)},
                { mActualPositionXTagModel, nameof(ActualPositionX) },
                { mProductAtScanningStageTagModel, nameof(ProductAtScanningStage)},

            };
        }

        public async void GoTo(double inputZ, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(mSetPointXTagModel, Convert.ToSingle(inputZ));
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, Convert.ToInt16(velocity));
            await PlcService.Instance.WriteValueAsync(mGoToTagModel, true);
        }

    }
}
