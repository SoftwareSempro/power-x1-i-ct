﻿using MachineManager;
using MachineManager.Lib;
using Platform;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;

namespace RobotToTrayUnloader.Application.Resources
{
    public class Robot : Resource
    {
        private int mStatus;
        private double mActualPositionX;
        private double mActualPositionY;
        private double mActualPositionZ;
        private double mActualPositionR;
        private bool mIsVacuumOn;
        private bool mIsPause;
       
        public Action PositionChanged;
        public Action SensorStatusChanged;

        private static new TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.Status);
        private static readonly TagModel mActualPositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.ActualPosition.X);
        private static readonly TagModel mActualPositionYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.ActualPosition.Y);
        private static readonly TagModel mActualPositionZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.ActualPosition.Z);
        private static readonly TagModel mActualPositionRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.ActualPosition.R);
        private static readonly TagModel mIsVacuumOnTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.IsVacuumOn);
        private static readonly TagModel mIsPauseTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPc.IsPause);

        private static readonly TagModel mGoToTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.GoToCommand);
        private static readonly TagModel mSetPointXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.Setpoint.X);
        private static readonly TagModel mSetPointYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.Setpoint.Y);
        private static readonly TagModel mSetPointZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.Setpoint.Z);
        private static readonly TagModel mSetPointRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.Setpoint.R);
        private static readonly TagModel mVelocityTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.Velocity);
        private static readonly TagModel mVacuumOnCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.VacuumOnCommand);
        private static readonly  TagModel mVacuumOffCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.VacuumOffCommand);
        private static readonly TagModel mActivateBlowOffCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.ActivateBlowOffCommand);
        private static readonly TagModel mDeactivateBlowOffCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderRobotInterface.ToPlc.DeactivateBlowOffCommand);

        public double ActualPositionX
        {
            get => mActualPositionX;
            private set
            {
                if (mActualPositionX != value)
                {
                    mActualPositionX = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        public double ActualPositionY
        {
            get => mActualPositionY;
            private set
            {
                if (mActualPositionY != value)
                {
                    mActualPositionY = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        public double ActualPositionZ
        {
            get => mActualPositionZ;
            private set
            {
                if (mActualPositionZ != value)
                {
                    mActualPositionZ = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        public double ActualPositionR
        {
            get => mActualPositionR;
            private set
            {
                if (mActualPositionR != value)
                {
                    mActualPositionR = value;
                    PositionChanged?.Invoke();
                }
            }
        }

        public bool IsVacuumOn
        {
            get => mIsVacuumOn;
            private set
            {
                if (mIsVacuumOn != value)
                {
                    mIsVacuumOn = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsPause
        {
            get => mIsPause;
            set
            {
                if (mIsPause != value)
                {
                    mIsPause = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }
        public Robot()
        {
            TagModelPathDictionary = new()
            {
                {StatusTagModel, nameof(Status) },
                {mActualPositionXTagModel, nameof(ActualPositionX) },
                {mActualPositionYTagModel, nameof(ActualPositionY) },
                {mActualPositionZTagModel, nameof(ActualPositionZ) },
                {mActualPositionRTagModel, nameof(ActualPositionR) },
                {mIsVacuumOnTagModel, nameof(IsVacuumOn) },
                {mIsPauseTagModel, nameof (IsPause) },
            };
        }

        public async void GoTo(double inputX, double inputY, double inputZ, double inputR, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(mSetPointXTagModel, Convert.ToSingle(inputX));
            await PlcService.Instance.WriteValueAsync(mSetPointYTagModel, Convert.ToSingle(inputY));
            await PlcService.Instance.WriteValueAsync(mSetPointZTagModel, Convert.ToSingle(inputZ));
            await PlcService.Instance.WriteValueAsync(mSetPointRTagModel, Convert.ToSingle(inputR));
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, Convert.ToInt16(velocity));
            await PlcService.Instance.WriteValueAsync(mGoToTagModel, true);
        }

        public async void VacuumOnCommand()
        {
            await PlcService.Instance.WriteValueAsync(mVacuumOnCommandTagModel, true);
        }

        public async void VacuumOffCommand()
        {
            await PlcService.Instance.WriteValueAsync(mVacuumOffCommandTagModel, true);
        }

        public async void DeactivateBlowOff()
        {
            await PlcService.Instance.WriteValueAsync(mDeactivateBlowOffCommandTagModel, true);
        }

        public async void ActivateBlowOff()
        {
            await PlcService.Instance.WriteValueAsync(mActivateBlowOffCommandTagModel, true);
        }


    }
}
