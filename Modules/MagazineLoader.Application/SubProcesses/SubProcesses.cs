﻿namespace MagazineLoader.Application.SubProcesses
{
    public class SubProcesses
    {
        public LoadMagazine LoadMagazine { get; } = new LoadMagazine();
        public ProvideMaterial ProvideMaterial { get; } = new ProvideMaterial();
        public UnloadMagazine UnloadMagazine { get; } = new UnloadMagazine();
    }
}
