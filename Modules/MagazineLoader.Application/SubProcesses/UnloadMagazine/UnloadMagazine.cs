﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.SubProcesses
{
    public class UnloadMagazine: SubscriptionBase
    {
        private readonly TagModel mStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPc.Status);

        private readonly TagModel mLowerConveyorZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.LowerConveyorZ);
        private readonly TagModel mStartPlaceOffsetZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.StartPlaceOffsetZ);
        private readonly TagModel mPlaceYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.PlaceY);
        private readonly TagModel mMagazineLiftSafePositionTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.LiftSafePositionY);        
        private readonly TagModel mVelocityYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.VelocityY);
        private readonly TagModel mVelocityZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.Unload.VelocityZ);
        private readonly TagModel mContinueTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.ContinueCommand);
        private readonly TagModel mRetryTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazineInterface.ToPlc.RetryCommand);
        private UnloadMagazineDataModel mSettings;
        private int mStatus;

        public event EventHandler<UnloadMagazineDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;
        public UnloadMagazineDataModel Settings
        {
            get => mSettings;
            set
            {
                mSettings = value;
                SettingsChanged?.Invoke(this, value);
            }
        }
        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public double LowerConveyorZ
        {
            get => (float)mSettings.LowerConveyorZ;
            set
            {
                if (mSettings.LowerConveyorZ != value)
                {
                   mSettings.LowerConveyorZ = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StartPlaceOffsetZ
        {
            get => (float)mSettings.StartPlaceOffsetZ;
            set
            {
                if (mSettings.StartPlaceOffsetZ != value)
                {
                    mSettings.StartPlaceOffsetZ = Convert.ToDouble(value);
                   SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceY
        {
            get => (float)mSettings.PlaceY;
            set
            {
                if (mSettings.PlaceY != value)
                {
                    mSettings.PlaceY = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazinePositionLiftSafePosition
        {
            get => mSettings.MagazinePositionLiftSafePosition;
            set
            {
                if (mSettings.MagazinePositionLiftSafePosition != value)
                {
                    mSettings.MagazinePositionLiftSafePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int VelocityY
        {
            get => mSettings.VelocityY;
            set
            {
                if (mSettings.VelocityY != value)
                {
                    mSettings.VelocityY = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int VelocityZ
        {
            get => mSettings.VelocityZ;
            set
            {
                if (mSettings.VelocityZ != value)
                {
                    mSettings.VelocityZ = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public async void ExecuteContinueCommand ()
        {
          await PlcService.Instance.WriteValueAsync(mContinueTagModel, true);
        }

        public async void ExecuteRetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryTagModel, true);
        }

        public void UpdateSettings(UnloadMagazineDataModel settings)
        {
            UpdateMachineRecipeSettingsOnPLC(settings.LowerConveyorZ, settings.StartPlaceOffsetZ, settings.PlaceY, settings.MagazinePositionLiftSafePosition, settings.VelocityY, settings.VelocityZ);            
        }

        public async void UpdateMachineRecipeSettingsOnPLC(double lowerConveyorZ, double startPlaceOffsetZ, double placeY, double magazineLiftSafePosition, int velocityY, int velocityZ)
        {
            Settings.StartPlaceOffsetZ = startPlaceOffsetZ;

            await PlcService.Instance.WriteValueAsync(mLowerConveyorZTagModel, (float)lowerConveyorZ);
            await PlcService.Instance.WriteValueAsync(mStartPlaceOffsetZTagModel, (float)startPlaceOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPlaceYTagModel, (float)placeY);
            await PlcService.Instance.WriteValueAsync(mMagazineLiftSafePositionTagModel, (float)magazineLiftSafePosition);
            await PlcService.Instance.WriteValueAsync(mVelocityYTagModel, (short)velocityY);
            await PlcService.Instance.WriteValueAsync(mVelocityZTagModel, (short)velocityZ);
        }       

        public UnloadMagazine()
        {
            Settings = new UnloadMagazineDataModel();

            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)},
                { mLowerConveyorZTagModel, nameof(LowerConveyorZ)},
                { mStartPlaceOffsetZTagModel, nameof(StartPlaceOffsetZ)},
                { mPlaceYTagModel, nameof(PlaceY)},
                { mMagazineLiftSafePositionTagModel, nameof(MagazinePositionLiftSafePosition)},
                { mVelocityYTagModel, nameof(VelocityY) },
                { mVelocityZTagModel, nameof(VelocityZ) }
            };
        }
    }
}
