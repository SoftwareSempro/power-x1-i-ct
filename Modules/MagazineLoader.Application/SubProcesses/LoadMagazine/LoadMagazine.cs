﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace MagazineLoader.Application.SubProcesses
{
    public class LoadMagazine: SubscriptionBase
    {
        private readonly TagModel mStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPc.Status);
        private readonly TagModel mConveyorUpperZPositionTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.UpperConveyorZ);
        private readonly TagModel mStartPickOffsetZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.StartPickOffsetZ);

        private readonly TagModel mEndPickOffsetZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.EndPickOffsetZ);
        private readonly TagModel mPickYTagModelTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.PickY);
        private readonly TagModel mMagazinePositionLiftSafePositionYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.MagazinePositionLiftSafePositionY);
        private readonly TagModel mSlotCountTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.SlotCount);
        private readonly TagModel mVelocityYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.VelocityY);
        private readonly TagModel mVelocityZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.Load.VelocityZ);

        private readonly TagModel mRetryCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.RetryCommand);
        private readonly TagModel mContinueCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLoadMagazineInterface.ToPlc.ContinueCommand);

        private LoadMagazineDataModel mSettings;
        private int mStatus;

        public event EventHandler<LoadMagazineDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;        
        public LoadMagazineDataModel Settings => mSettings;

        public int Status
        {
            get => mStatus;
            private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public double ConveyorUpperZPosition
        {
            get => (float) mSettings.ConveyorUpperZPosition;
            set
            {
                if (mSettings.ConveyorUpperZPosition != value)
                {
                    mSettings.ConveyorUpperZPosition = Convert.ToDouble(value);                   
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StartPickOffsetZ
        {
            get => (float) mSettings.StartPickOffsetZ;
            set
            {
                if (mSettings.StartPickOffsetZ != value)
                {
                    mSettings.StartPickOffsetZ = Convert.ToDouble (value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double EndPickOffsetZ
        {
            get => (float)mSettings.EndPickOffsetZ;
            set
            {
                if (mSettings.EndPickOffsetZ != value)
                {
                    mSettings.EndPickOffsetZ = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PickY
        {
            get => (float) mSettings.PickY;
            set
            {
                if (mSettings.PickY != value)
                {
                    mSettings.PickY = Convert.ToDouble(value); ;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazinePositionLiftSafePositionY
        {
            get => (float) mSettings.MagazinePositionLiftSafePositionY;
            set
            {
                if (mSettings.MagazinePositionLiftSafePositionY != value)
                {
                    mSettings.MagazinePositionLiftSafePositionY = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int SlotCount
        {
            get => mSettings.SlotCount;
            set
            {
                if (mSettings.SlotCount != value)
                {
                    mSettings.SlotCount = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }
        
        public int VelocityY
        {
            get => mSettings.VelocityY;
            set
            {
                if (mSettings.VelocityY != value)
                {
                    mSettings.VelocityY = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int VelocityZ
        {
            get => mSettings.VelocityZ;
            set
            {
                if (mSettings.VelocityZ != value)
                {
                    mSettings.VelocityZ = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public void UpdateSettings(LoadMagazineDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(settings.SlotCount);
            UpdateMachineRecipeSettingsOnPLC(settings.ConveyorUpperZPosition, settings.StartPickOffsetZ, settings.EndPickOffsetZ, settings.PickY, settings.MagazinePositionLiftSafePositionY, settings.VelocityY, settings.VelocityZ);                 
        }

        internal async void UpdateMachineRecipeSettingsOnPLC(double conveyorUpperZPosition, double startPickOffsetZ, double endPickOffsetZ, double pickY, double magazinePositionLiftSafePositionY, int velocityY, int velocityZ)
        {
            await PlcService.Instance.WriteValueAsync(mConveyorUpperZPositionTagModel, (float) conveyorUpperZPosition);
            await PlcService.Instance.WriteValueAsync(mStartPickOffsetZTagModel, (float) startPickOffsetZ);
            await PlcService.Instance.WriteValueAsync(mEndPickOffsetZTagModel, (float) endPickOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPickYTagModelTagModel, (float) pickY);
            await PlcService.Instance.WriteValueAsync(mMagazinePositionLiftSafePositionYTagModel, (float) magazinePositionLiftSafePositionY);
            await PlcService.Instance.WriteValueAsync(mVelocityYTagModel, (short) velocityY);
            await PlcService.Instance.WriteValueAsync(mVelocityZTagModel, (short)velocityZ);
        }

        internal async void UpdateProductRecipeSettingsOnPLC(int slotCount)
        {
            await PlcService.Instance.WriteValueAsync(mSlotCountTagModel, (short) slotCount);
        }

        public LoadMagazine()
        {
            mSettings = new LoadMagazineDataModel();

            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status) },
                { mConveyorUpperZPositionTagModel, nameof(ConveyorUpperZPosition) },
                { mStartPickOffsetZTagModel, nameof(StartPickOffsetZ) },
                { mEndPickOffsetZTagModel, nameof(EndPickOffsetZ) },
                { mPickYTagModelTagModel, nameof(PickY) },
                { mMagazinePositionLiftSafePositionYTagModel, nameof(MagazinePositionLiftSafePositionY) },
                { mSlotCountTagModel, nameof(SlotCount) },
                { mVelocityYTagModel, nameof(VelocityY) },
                { mVelocityZTagModel, nameof(VelocityZ) }
            };
        }

        public async void ExecuteRetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryCommandTagModel, true);
        }

        public async void ExecuteContinueCommand()
        {
            await PlcService.Instance.WriteValueAsync(mContinueCommandTagModel, true);
        }
    }
}
