﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using MaterialTracking;
using MaterialTracking.Lib;
using System;
using System.Collections.Generic;

namespace MagazineLoader.Application.SubProcesses
{
    public class ProvideMaterial: SubscriptionBase
    {
        private readonly TagModel mStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPc.Status);
        private readonly TagModel mMagazineSlotOffsetTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.MagazineSlotOffset);
        private readonly TagModel mMagazineBottumSlotOffsetTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.MagazineBottomSlotOffset);
        private readonly TagModel mDeviceCountPerLeadFrameTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.DeviceCountPerLeadFrame);
        private readonly TagModel mMagazineLiftStartPositionZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.MagazineLiftStartPositionZ);
        private readonly TagModel mMagazineLiftStartPositionYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.MagazineLiftStartPositionY);
        private readonly TagModel mMagazineWidthTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.MagazineWidth);
        private readonly TagModel mVelocityYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.VelocityY);
        private readonly TagModel mVelocityZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.Provide.VelocityZ);
        private readonly TagModel mRemoveMaterialCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.RemoveMaterialCommand);
        private readonly TagModel mRetryCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderProvideMaterialInterface.ToPlc.RetryCommand);

        private ProvideMagazineDataModel mSettings;
        private readonly MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private readonly LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;

        private int mProvideMagazineStatus;
        public event EventHandler<ProvideMagazineDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;
        public ProvideMagazineDataModel Settings
        {
            get => mSettings;
            set
            {
                mSettings = value;
                SettingsChanged?.Invoke(this, value);
            }
        }
        public int Status
        {
            get => mProvideMagazineStatus;
            set
            {
                if (mProvideMagazineStatus != value)
                {
                    mProvideMagazineStatus = value;
                    StatusChanged?.Invoke(this, mProvideMagazineStatus);
                }
            }
        }


        public double MagazineSlotOffset
        {
            get => (float)mSettings.MagazineSlotOffset;
            set
            {
                if (mSettings.MagazineSlotOffset != value)
                {
                    mSettings.MagazineSlotOffset = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazineBottomSlotOffset
        {
            get => (float)mSettings.MagazineBottomSlotOffset;
            set
            {
                if (mSettings.MagazineBottomSlotOffset != value)
                {
                    mSettings.MagazineBottomSlotOffset = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double DeviceCountPerLeadFrame
        {
            get => (float)mSettings.DeviceCountPerLeadFrame;
            set
            {
                if (mSettings.DeviceCountPerLeadFrame != value)
                {
                    mSettings.DeviceCountPerLeadFrame = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazineLiftStartPositionZ
        {
            get => (float)mSettings.MagazineLiftStartPositionZ;
            set
            {
                if (mSettings.MagazineLiftStartPositionZ != value)
                {
                    mSettings.MagazineLiftStartPositionZ = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazineLiftStartPositionY
        {
            get => (float)mSettings.MagazineLiftStartPositionY;
            set
            {
                if (mSettings.MagazineLiftStartPositionY != value)
                {
                    mSettings.MagazineLiftStartPositionY = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazineWidth
        {
            get => (float)mSettings.MagazineWidth;
            set
            {
                if (mSettings.MagazineWidth != value)
                {
                    mSettings.MagazineWidth = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int VelocityY
        {
            get => mSettings.VelocityY;
            set
            {
                if (mSettings.VelocityY != value)
                {
                    mSettings.VelocityY = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int VelocityZ
        {
            get => mSettings.VelocityZ;
            set
            {
                if (mSettings.VelocityZ != value)
                {
                    mSettings.VelocityZ = Convert.ToInt16(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public void UpdateSettings(ProvideMagazineDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(settings.DeviceCountPerLeadFrame, settings.MagazineBottomSlotOffset, settings.MagazineSlotOffset, settings.MagazineWidth);
            UpdateMachineRecipeSettingsOnPLC(settings.MagazineLiftStartPositionY, settings.MagazineLiftStartPositionZ, settings.VelocityY, settings.VelocityZ);
        }

        internal async void UpdateMachineRecipeSettingsOnPLC(double magazineLiftStartPositionY, double magazineLiftStartPositionZ, int velocityY, int velocityZ)
        {
            await PlcService.Instance.WriteValueAsync(mMagazineLiftStartPositionYTagModel, (float)magazineLiftStartPositionY);
            await PlcService.Instance.WriteValueAsync(mMagazineLiftStartPositionZTagModel, (float)magazineLiftStartPositionZ);
            await PlcService.Instance.WriteValueAsync(mVelocityYTagModel, (short)velocityY);
            await PlcService.Instance.WriteValueAsync(mVelocityZTagModel, (short)velocityZ);
        }

        internal async void UpdateProductRecipeSettingsOnPLC(int devicesPerSlot, double bottomSlotOffset, double slotOffset, double magazineWidth)
        {
            await PlcService.Instance.WriteValueAsync(mMagazineSlotOffsetTagModel, (float)slotOffset);
            await PlcService.Instance.WriteValueAsync(mMagazineBottumSlotOffsetTagModel, (float)bottomSlotOffset);
            await PlcService.Instance.WriteValueAsync(mDeviceCountPerLeadFrameTagModel, (short)devicesPerSlot);
            await PlcService.Instance.WriteValueAsync(mMagazineWidthTagModel, (float)magazineWidth);
        }

        public ProvideMaterial()
        {
            Settings = new ProvideMagazineDataModel();

            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status) },
                { mMagazineSlotOffsetTagModel, nameof(MagazineSlotOffset) },
                { mMagazineBottumSlotOffsetTagModel, nameof(MagazineBottomSlotOffset) },
                { mDeviceCountPerLeadFrameTagModel, nameof(DeviceCountPerLeadFrame) },
                { mMagazineLiftStartPositionZTagModel, nameof(MagazineLiftStartPositionZ) },
                { mMagazineLiftStartPositionYTagModel, nameof(MagazineLiftStartPositionY) },
                { mMagazineWidthTagModel, nameof(MagazineWidth) },
                { mVelocityYTagModel, nameof(VelocityY) },
                { mVelocityZTagModel, nameof(VelocityZ) }
             };

        }

        public async void ExecuteRemoveMaterialCommand(int removedDeviceId)
        {
            await PlcService.Instance.WriteValueAsync(mRemoveMaterialCommandTagModel, true);
            mMaterialTracking.UpdateDevice(removedDeviceId, mLotControl.CurrentLotId, DeviceProcessState.Removed);
        }

        public async void ExecuteRetryCommand()
        {
            await PlcService.Instance.WriteValueAsync(mRetryCommandTagModel, true);
        }
    }
}