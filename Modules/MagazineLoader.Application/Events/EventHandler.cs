﻿using EventHandler.Application;
using MagazineLoader.Lib;

namespace MagazineLoader.Application
{
    public class EventHandler : EventHandlerSubScriptionBase
    {
        public bool UpperConveyorAlmostEmpty { set => SetEventPropertyValue(value); }
        public bool UpperConveyorEmpty { set => SetEventPropertyValue(value); }
        public bool MagazineDidNotReachPickupPosition { set => SetEventPropertyValue(value); }
        public bool UpperHatchFailedToOpen { set => SetEventPropertyValue(value); }
        public bool UpperHatchFailedToClose { set => SetEventPropertyValue(value); }
        public bool FailedToClampMagazineInGripper { set => SetEventPropertyValue(value); }
        public bool MagazineLiftDidNotReachLoadSafePositionY { set => SetEventPropertyValue(value); }
        public bool MagazineLiftDidNotReachProvidePosition { set => SetEventPropertyValue(value); }
        public bool LeadFramePusherFailedToReachProvidePosition { set => SetEventPropertyValue(value); }
        public bool LeadFramePusherFailedToReachRetractPosition { set => SetEventPropertyValue(value); }
        public bool MagazineCanNotMoveBecauseMaterialInTransfer { set => SetEventPropertyValue(value); }
        public bool FailedToProvideMaterial { set => SetEventPropertyValue(value); }
        public bool MaterialDidNotReachOutfeedAssist { set => SetEventPropertyValue(value); }
        public bool MagazineLiftDidNotReachUnloadPositionZ { set => SetEventPropertyValue(value); }
        public bool LowerConveyorAlmostFull { set => SetEventPropertyValue(value); }
        public bool LowerConveyorFull { set => SetEventPropertyValue(value); }
        public bool LowerConveyorFullAssist { set => SetEventPropertyValue(value); }
        public bool FailedToReachUnloadPositionY { set => SetEventPropertyValue(value); }
        public bool FailedToUnclampMagazineInGripper { set => SetEventPropertyValue(value); }
        public bool FailedToReachClearUnloadPositionZ { set => SetEventPropertyValue(value); }
        public bool MagazineLiftDidNotReachUnloadSafePositionY { set => SetEventPropertyValue(value); }    
        public bool MagazineLoaderMainDoorUnlocked { set => SetEventPropertyValue(value); }   
        public bool MagazineloaderMainDoorLockError { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderProvideDoorOpenError { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderEmergencyStopActive { set => SetEventPropertyValue(value); }

        public bool GripperFailedToReachCloseSensor { set => SetEventPropertyValue(value); }
        public bool GripperFailedToReachOpenSensor { set => SetEventPropertyValue(value); }
        public bool LoaderLiftNotInPosition { set => SetEventPropertyValue(value); }
        public bool LiftZDriveError { set => SetEventPropertyValue(value); }
        public bool LiftZCommunicationError { set => SetEventPropertyValue(value); }
        
        public bool MagazineMissingInLiftAssist { set => SetEventPropertyValue(value); }
        public bool MagazineUnknownInLiftAssist { set => SetEventPropertyValue(value); }
        public bool LowerHatchFailedToReachClosePosition { set => SetEventPropertyValue(value); }
        public bool LowerHatchFailedToReachOpenPosition { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderDoorLoadNotLocked { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderDoorUnloadNotLocked { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderNotAllCoversLocked { set => SetEventPropertyValue(value); }


        public EventHandler()
        {
            mEventDictionary = new()
            {
                { nameof(UpperConveyorAlmostEmpty), MagazineLoaderEvents.UpperConveyorAlmostEmpty },
                { nameof(UpperConveyorEmpty), MagazineLoaderEvents.UpperConveyorEmpty },
                { nameof(MagazineDidNotReachPickupPosition), MagazineLoaderEvents.MagazineDidNotReachPickupPosition },
                { nameof(UpperHatchFailedToOpen), MagazineLoaderEvents.UpperHatchFailedToOpen },
                { nameof(UpperHatchFailedToClose), MagazineLoaderEvents.UpperHatchFailedToClose },
                { nameof(FailedToClampMagazineInGripper), MagazineLoaderEvents.FailedToClampMagazineInGripper },
                { nameof(MagazineLiftDidNotReachLoadSafePositionY), MagazineLoaderEvents.MagazineLiftDidNotReachSafePositionY },
                { nameof(MagazineLiftDidNotReachProvidePosition), MagazineLoaderEvents.MagazineLiftDidNotReachProvidePosition },
                { nameof(LeadFramePusherFailedToReachProvidePosition), MagazineLoaderEvents.LeadFramePusherFailedToReachProvidePosition },
                { nameof(LeadFramePusherFailedToReachRetractPosition), MagazineLoaderEvents.LeadFramePusherFailedToReachRetractPosition },
                { nameof(MagazineCanNotMoveBecauseMaterialInTransfer), MagazineLoaderEvents.MagazineCanNotMoveBecauseMaterialInTransferProvide },
                { nameof(FailedToProvideMaterial), MagazineLoaderEvents.FailedToProvideMaterial },
                { nameof(MaterialDidNotReachOutfeedAssist), MagazineLoaderEvents.MaterialDidNotReachOutfeed },
                { nameof(MagazineLiftDidNotReachUnloadPositionZ), MagazineLoaderEvents.MagazineLiftDidNotReachUnloadPositionZ },
                { nameof(LowerConveyorAlmostFull), MagazineLoaderEvents.LowerConveyorAlmostFull },
                { nameof(LowerConveyorFull), MagazineLoaderEvents.LowerConveyorFull },
                { nameof(LowerConveyorFullAssist), MagazineLoaderEvents.LowerConveyorFullAssist },
                { nameof(FailedToReachUnloadPositionY), MagazineLoaderEvents.FailedToReachUnloadPositionY },
                { nameof(FailedToUnclampMagazineInGripper), MagazineLoaderEvents.FailedToUnclampMagazineInGripper },
                { nameof(FailedToReachClearUnloadPositionZ), MagazineLoaderEvents.FailedToReachClearUnloadPositionZ },
                { nameof(MagazineLiftDidNotReachUnloadSafePositionY), MagazineLoaderEvents.MagazineLiftDidNotReachUnloadSafePositionY },              
                { nameof(MagazineLoaderMainDoorUnlocked), MagazineLoaderEvents.MagazineLoaderMainDoorUnlocked },
                { nameof(MagazineloaderMainDoorLockError), MagazineLoaderEvents.MagazineLoaderMainDoorLockError },
                { nameof(MagazineLoaderProvideDoorOpenError), MagazineLoaderEvents.MagazineLoaderProvideDoorOpenError },
                { nameof(MagazineLoaderEmergencyStopActive), MagazineLoaderEvents.MagazineLoaderEmergencyStopActive },
                { nameof(GripperFailedToReachCloseSensor), MagazineLoaderEvents.GripperFailedToReachCloseSensor },
                { nameof(GripperFailedToReachOpenSensor), MagazineLoaderEvents.GripperFailedToReachOpenSensor },
                { nameof(LoaderLiftNotInPosition), MagazineLoaderEvents.LoaderLiftNotInPosition },
                { nameof(LiftZDriveError), MagazineLoaderEvents.LiftZDriveError },
                { nameof(LiftZCommunicationError), MagazineLoaderEvents.LiftZCommunicationError },
                { nameof(MagazineMissingInLiftAssist), MagazineLoaderEvents.MagazineMissingInLiftAssist },
                { nameof(MagazineUnknownInLiftAssist), MagazineLoaderEvents.MagazineUnknownInLiftAssist },
                { nameof(LowerHatchFailedToReachClosePosition), MagazineLoaderEvents.LowerHatchFailedToReachClosePosition },
                { nameof(LowerHatchFailedToReachOpenPosition), MagazineLoaderEvents.LowerHatchFailedToReachOpenPosition },
                { nameof(MagazineLoaderDoorLoadNotLocked), MagazineLoaderEvents.MagazineLoaderLoadMainDoorNotLockedAssist },
                { nameof(MagazineLoaderDoorUnloadNotLocked), MagazineLoaderEvents.MagazineLoaderUnloadMainDoorNotLockedAssist },
                { nameof(MagazineLoaderNotAllCoversLocked), MagazineLoaderEvents.MagazineLoaderNotAllCoversLocked },
            };

            RegisterTagModelsInDictionary();
        }
    }
}
