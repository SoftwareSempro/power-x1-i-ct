﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
    public class LeadFramePusher
    {
        private int mLeadFramePusherStatus;
        private bool mPrimairPusherIn;
        private bool mPrimairPusherMiddle;
        private bool mPrimairPusherOut;
        private bool mSecondairPusherIn;
        private bool mSecondairPusherOut;

        public event EventHandler<int> StatusChanged;
        public event Action SensorStatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.Status);
        public TagModel PrimairPusherInTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.PrimairPusherIn);
        public TagModel PrimairPusherMiddleTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.PrimairPusherMiddle);
        public TagModel PrimairPusherOutTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.PrimairPusherOut);
        public TagModel SecondairPusherInTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.SecondairPusherIn);
        public TagModel SecondairPusherOutTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPc.SecondairPusherOut);

        public TagModel PushCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.PushCommand);
        public TagModel RetreiveCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.RetrieveCommand);
        public TagModel PrimairPushSettingTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.PrimairPusherSettings.Push);
        public TagModel PrimairRetreiveSettingTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.PrimairPusherSettings.Retreive);
        public TagModel SecondairPushSettingTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.SecondairPusherSettings.Push);
        public TagModel SecondairRetreiveSettingTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherInterface.ToPlc.SecondairPusherSettings.Retreive);
        public int Status
        {
            get => mLeadFramePusherStatus;
            private set
            {
                if (mLeadFramePusherStatus != value)
                {
                    mLeadFramePusherStatus = value;
                    StatusChanged?.Invoke(this, mLeadFramePusherStatus);
                }
            }
        }

        public bool PrimairPusherIn
        {
            get => mPrimairPusherIn;
            private set
            {
                if (mPrimairPusherIn != value)
                {
                    mPrimairPusherIn = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool PrimairPusherMiddle
        {
            get => mPrimairPusherMiddle;
            private set
            {
                if (mPrimairPusherMiddle != value)
                {
                    mPrimairPusherMiddle = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool PrimairPusherOut
        {
            get => mPrimairPusherOut;
            private set
            {
                if (mPrimairPusherOut != value)
                {
                    mPrimairPusherOut = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool SecondairPusherIn
        {
            get => mSecondairPusherIn;
            private set
            {
                if (mSecondairPusherIn != value)
                {
                    mSecondairPusherIn = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool SecondairPusherOut
        {
            get => mSecondairPusherOut;
            private set
            {
                if (mSecondairPusherOut != value)
                {
                    mSecondairPusherOut = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public async void PrimairPusherRetrieve()
        {
            await PlcService.Instance.WriteValueAsync(PrimairRetreiveSettingTagModel, true);
            await PlcService.Instance.WriteValueAsync(RetreiveCommandTagModel, true);
        }

        public async void PrimairPusherPush()
        {
            await PlcService.Instance.WriteValueAsync(PrimairPushSettingTagModel, true);
            await PlcService.Instance.WriteValueAsync(PushCommandTagModel, true);
        }

        public async void SecondairPusherRetrieve()
        {
            await PlcService.Instance.WriteValueAsync(SecondairRetreiveSettingTagModel, true);
            await PlcService.Instance.WriteValueAsync(RetreiveCommandTagModel, true);
        }

        public async void SecondairPusherPush()
        {
            await PlcService.Instance.WriteValueAsync(SecondairPushSettingTagModel, true);
            await PlcService.Instance.WriteValueAsync(PushCommandTagModel, true);
        }
    }
}
