﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace MagazineLoader.Application.Resources
{
    public class LowerConveyor
    {
        private int mStatus;
        private bool mMagazinePresent;
        private bool mConveyorFull;

        public event EventHandler<int> StatusChanged;
        public event Action SensorStatusChanged;

        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerConveyorInterface.ToPc.Status);
        public TagModel MagazinePresentTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerConveyorInterface.ToPc.MagazinePresentSensor);
        public TagModel ConveyorFullTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerConveyorInterface.ToPc.ConveyorFull);

        public TagModel ConveyorStartCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerConveyorInterface.ToPlc.StartCommand);
        public TagModel ConveyorStopCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerConveyorInterface.ToPlc.StopCommand);
        public int Status
        {
            get => mStatus;
            private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool MagazinePresent
        {
            get => mMagazinePresent;
            private set
            {
                if (mMagazinePresent != value)
                {
                    mMagazinePresent = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool ConveyorFull
        {
            get => mConveyorFull;
            private set
            {
                if (mConveyorFull != value)
                {
                    mConveyorFull = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public async void ConveyorStart()
        {
            await PlcService.Instance.WriteValueAsync(ConveyorStartCommandTagModel, true);
        }

        public async void ConveyorStop()
        {
            await PlcService.Instance.WriteValueAsync(ConveyorStopCommandTagModel, true);
        }
    }
}
