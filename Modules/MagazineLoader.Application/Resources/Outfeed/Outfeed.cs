﻿using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
    public class Outfeed
    {
        public bool mProductAtTransfer;

        public event Action SensorStatusChanged;

        public TagModel ProductAtTransferTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderOutfeedInterface.ToPc.ProductAtTransfer);
    

    public bool ProductAtTransfer
    {
        get => mProductAtTransfer;
        private set
        {
            if (mProductAtTransfer != value)
            {
                mProductAtTransfer = value;
                SensorStatusChanged?.Invoke();
            }
        }
    }
  }
}
