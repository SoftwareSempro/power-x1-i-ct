﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace MagazineLoader.Application.Resources
{
   public class LowerHatch
   {
        private int mStatus;
        private bool mHatchIsOpen;
        private bool mHatchIsClosed;

        public event EventHandler<int> StatusChanged;
        public event Action SensorStatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerHatchInterface.ToPc.Status);
        public TagModel HatchIsOpenTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerHatchInterface.ToPc.IsOpen);
        public TagModel HatchIsClosedTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerHatchInterface.ToPc.IsClosed);

        public TagModel HatchOpenTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerHatchInterface.ToPlc.OpenCommand);
        public TagModel HatchCloseTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLowerHatchInterface.ToPlc.CloseCommand);
        public int Status
        {
            get => mStatus;
            private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool HatchIsOpen
        {
            get => mHatchIsOpen;
            private set
            {
                if (mHatchIsOpen != value)
                {
                    mHatchIsOpen = value;
                    SensorStatusChanged?.Invoke();
                }                       
            }
        }

        public bool HatchIsClosed
        {
            get => mHatchIsClosed;
            private set
            {
                if (mHatchIsClosed != value)
                {
                    mHatchIsClosed = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public async void HatchOpen()
        {
            await PlcService.Instance.WriteValueAsync(HatchOpenTagModel, true);
        }

        public async void HatchClose()
        {
            await PlcService.Instance.WriteValueAsync(HatchCloseTagModel, true);
        }
    }
}
