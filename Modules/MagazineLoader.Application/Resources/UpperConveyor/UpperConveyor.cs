﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
   public class UpperConveyor
    {
        private int mUpperConveyorStatus;
        private bool mMagazinePresent;
        private bool mConveyorAlmostEmpty;

        public event EventHandler<int> StatusChanged;
        public event Action SensorStatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorInterface.ToPc.Status);
        public TagModel MagazinePresentTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorInterface.ToPc.MagazinePresentSensor);
        public TagModel ConveyorAlmostEmptyTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorInterface.ToPc.conveyorAlmostEmpty);

        public TagModel ConveyorStartCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorInterface.ToPlc.StartCommand);
        public TagModel ConveyorStopCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorInterface.ToPlc.StopCommand);
        public int Status
        {
            get => mUpperConveyorStatus;
            private set
            {
                if (mUpperConveyorStatus != value)
                {
                    mUpperConveyorStatus = value;
                    StatusChanged?.Invoke(this, mUpperConveyorStatus);
                }
            }
        }

        public bool MagazinePresent
        {
            get => mMagazinePresent;
            private set
            {
                if (mMagazinePresent != value)
                {
                    mMagazinePresent = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool ConveyorAlmostEmpty
        {
            get => mConveyorAlmostEmpty;
            private set
            {
                if (mConveyorAlmostEmpty != value)
                {
                    mConveyorAlmostEmpty = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public async void ConveyorStart()
        {
            await PlcService.Instance.WriteValueAsync(ConveyorStartCommandTagModel, true);
        }

        public async void ConveyorStop()
        {
            await PlcService.Instance.WriteValueAsync(ConveyorStopCommandTagModel, true);
        }
    }
}
