﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
   public class Lift
    {
        private int mLiftStatus;
        private double mPositionY;
        private double mPositionZ;
        private bool mIsGripperOpen;
        private bool mIsGripperClosed;

        public event EventHandler<int> StatusChanged;
        public event EventHandler<double> PositionYChanged;
        public event EventHandler<double> PositionZChanged;
        public event Action SensorStatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPc.Status);
        public TagModel PositionYTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPc.PositionY);
        public TagModel PositionZTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPc.PositionZ);
        public TagModel GripperOpenTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPc.GripperOpen);
        public TagModel GripperClosedTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPc.GripperClosed);

        public TagModel GoToTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.GoToCommand);
        public TagModel SetPointYTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.SetPointY);
        public TagModel SetPointZTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.SetPointZ);
        public TagModel VelocityTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.Velocity);
        public TagModel GripperOpenCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.GripperOpenCommand);
        public TagModel GripperCloseCommandTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftInterface.ToPlc.GripperCloseCommand);
        public int Status
        {
            get => mLiftStatus;
            private set
            {
                if (mLiftStatus != value)
                {
                    mLiftStatus = value;
                    StatusChanged?.Invoke(this, mLiftStatus);
                }
            }
        }

        public double PositionY
        {
            get => mPositionY;
            private set
            {
                if (mPositionY != value)
                {
                    mPositionY = value;
                    PositionYChanged?.Invoke(this, mPositionY);
                }
            }
        }

        public double PositionZ
        {
            get => mPositionZ;
            private set
            {
                if (mPositionZ != value)
                {
                    mPositionZ = value;
                    PositionZChanged?.Invoke(this, mPositionZ);
                }
            }
        }

        public bool IsGripperOpen
        {
            get => mIsGripperOpen;
            private set
            {
                if (mIsGripperOpen != value)
                {
                    mIsGripperOpen = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public bool IsGripperClosed
        {
            get => mIsGripperClosed;
            private set
            {
                if (mIsGripperClosed != value)
                {
                    mIsGripperClosed = value;
                    SensorStatusChanged?.Invoke();
                }
            }
        }

        public async void GoTo(double inputY, double inputZ, int velocity)
        {
            await PlcService.Instance.WriteValueAsync(SetPointYTagModel, (float)inputY);
            await PlcService.Instance.WriteValueAsync(SetPointZTagModel, (float)inputZ);
            await PlcService.Instance.WriteValueAsync(VelocityTagModel, (float)velocity);
            await PlcService.Instance.WriteValueAsync(GoToTagModel, true);
        }

        public async void GripperOpen()
        {
            await PlcService.Instance.WriteValueAsync(GripperOpenCommandTagModel, true);
        }

        public async void GripperClose()
        {
            await PlcService.Instance.WriteValueAsync(GripperCloseCommandTagModel, true);
        }
    }
}
