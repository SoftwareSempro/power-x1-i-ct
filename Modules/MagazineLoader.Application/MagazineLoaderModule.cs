﻿using System;
using System.Collections.Generic;
using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using MagazineLoader.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using Recipe.Lib;
using RecipeManager;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace MagazineLoader.Application
{
    public class MagazineLoaderModule : SubscriptionBase
    {
        public event EventHandler<bool> HasErrorChanged;
        public event EventHandler<int> StateChanged;
        public event EventHandler<int> StatusChanged;
        public event EventHandler<bool> SimulationChanged;
        public event EventHandler<bool> InProductionChanged;

        public event EventHandler<MagazineLoaderDataModel> SettingsChanged;

        private ILog mLog = LogClient.Get();
        private PlcService mPLCService = PlcService.Instance;
        private EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;

        private bool mHasError;
        private int mState;
        private int mStatus;

        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        private static MagazineLoaderModule mMagazineLoader;
        private bool mIsRunningProduction;
        private bool mInSimulation;

        private readonly TagModel mMagazineLoaderStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPc.Status);
        private readonly TagModel mMagazineLoaderStateTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPc.State);
        private readonly TagModel mIsRunningProductionTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPc.IsRunningProduction);
        private readonly TagModel mInSimulationCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPc.InSimulation);
        
        private readonly TagModel mInitializeCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.InitializeCommand);
        private readonly TagModel mStartCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.StartCommand);
        private readonly TagModel mStopCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.StopCommand);
        private readonly TagModel mLoadMagazineCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.LoadMagazineCommand);
        private readonly TagModel mProvideMaterialCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.ProvideLeadFramesCommand);
        private readonly TagModel mUnloadMagazineCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.UnloadMagazineCommand);
        private readonly TagModel mVelocityPercentageTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.VelocityPercentage);
        private readonly TagModel mSimulationEnabledCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.EnableSimulation);
        private readonly TagModel mClearMaterial = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModuleInterface.ToPlc.ClearMaterial);

        public static MagazineLoaderModule Instance => mMagazineLoader ??= new MagazineLoaderModule();

        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();
        public MaterialTracking MaterialTracking { get; } = new MaterialTracking();
        public MagazineLoaderDataModel Settings { get; }

        public EventHandler EventHandler { get; } = new();

        public bool IsRunningProduction
        {
            get => mIsRunningProduction;
            set
            {
                if (mIsRunningProduction != value)
                {
                    mIsRunningProduction = value;
                    InProductionChanged?.Invoke(this, mIsRunningProduction);
                }
            }
        }

        public bool HasError
        {
            get => mHasError;
            set
            {
                if (mHasError != value)
                {
                    mHasError = value;
                    HasErrorChanged?.Invoke(this, mHasError);
                }
            }
        }
        public int State
        {
            get => mState;
            set
            {
                if (mState != value)
                {
                    mState = value;
                    StateChanged?.Invoke(this, mState);
                }
            }
        }

        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool InSimulation
        {
            get => mInSimulation;
            set
            {
                if (mInSimulation != value)
                {
                    mInSimulation = value;
                    SimulationChanged?.Invoke(this, mInSimulation);
                }
            }
        }

        public MagazineLoaderModule()
        {
            Settings = new MagazineLoaderDataModel();
            Settings.DataModelChanged += SettingsChanged;

            InitializeSettings();
            mRecipeManager.MachineRecipeChanged += RecipeManager_MachineRecipeChanged;
            mRecipeManager.ProductRecipeChanged += RecipeManager_ProductRecipeChanged;

            TagModelPathDictionary = new Dictionary<ITagModel, string>
            {
                {mMagazineLoaderStatusTagModel, nameof(Status) },
                {mMagazineLoaderStateTagModel, nameof(State) },
                {mIsRunningProductionTagModel, nameof(IsRunningProduction) },
                {mInSimulationCommandTagModel, nameof(InSimulation) },
                //Settings
                {mVelocityPercentageTagModel, GetNameOf(() => Settings.VelocityPercentage)},
                //Resources
                {Resources.UpperConveyor.StatusTagModel, GetNameOf(() => Resources.UpperConveyor.Status)},
                {Resources.UpperConveyor.MagazinePresentTagModel, GetNameOf(() => Resources.UpperConveyor.MagazinePresent)},
                {Resources.UpperConveyor.ConveyorAlmostEmptyTagModel, GetNameOf(() => Resources.UpperConveyor.ConveyorAlmostEmpty)},
                {Resources.UpperHatch.StatusTagModel, GetNameOf(() => Resources.UpperHatch.Status)},
                {Resources.UpperHatch.HatchIsOpenTagModel, GetNameOf(() => Resources.UpperHatch.HatchIsOpen)},
                {Resources.UpperHatch.HatchIsClosedTagModel, GetNameOf(() => Resources.UpperHatch.HatchIsClosed)},
                {Resources.Lift.StatusTagModel, GetNameOf(() => Resources.Lift.Status)},
                {Resources.Lift.PositionYTagModel, GetNameOf(() => Resources.Lift.PositionY)},
                {Resources.Lift.PositionZTagModel, GetNameOf(() => Resources.Lift.PositionZ)},
                {Resources.Lift.GripperOpenTagModel, GetNameOf(() => Resources.Lift.IsGripperOpen)},
                {Resources.Lift.GripperClosedTagModel, GetNameOf(() => Resources.Lift.IsGripperClosed)},
                {Resources.LeadFramePusher.StatusTagModel, GetNameOf(() => Resources.LeadFramePusher.Status)},
                {Resources.LeadFramePusher.PrimairPusherInTagModel, GetNameOf(() => Resources.LeadFramePusher.PrimairPusherIn)},
                {Resources.LeadFramePusher.PrimairPusherMiddleTagModel, GetNameOf(() => Resources.LeadFramePusher.PrimairPusherMiddle)},
                {Resources.LeadFramePusher.PrimairPusherOutTagModel, GetNameOf(() => Resources.LeadFramePusher.PrimairPusherOut)},
                {Resources.LeadFramePusher.SecondairPusherInTagModel, GetNameOf(() => Resources.LeadFramePusher.SecondairPusherIn)},
                {Resources.LeadFramePusher.SecondairPusherOutTagModel, GetNameOf(() => Resources.LeadFramePusher.SecondairPusherOut)},
                {Resources.LowerHatch.StatusTagModel, GetNameOf(() => Resources.LowerHatch.Status)},
                {Resources.LowerHatch.HatchIsOpenTagModel, GetNameOf(() => Resources.LowerHatch.HatchIsOpen)},
                {Resources.LowerHatch.HatchIsClosedTagModel, GetNameOf(() => Resources.LowerHatch.HatchIsClosed)},
                {Resources.LowerConveyor.StatusTagModel, GetNameOf(() => Resources.LowerConveyor.Status)},
                {Resources.LowerConveyor.MagazinePresentTagModel, GetNameOf(() => Resources.LowerConveyor.MagazinePresent)},
                {Resources.LowerConveyor.ConveyorFullTagModel, GetNameOf(() => Resources.LowerConveyor.ConveyorFull)},
                {Resources.Outfeed.ProductAtTransferTagModel, GetNameOf(() => Resources.Outfeed.ProductAtTransfer)}
             };

            AddTagModelDictionary(GetNameOf(() => SubProcesses.LoadMagazine), SubProcesses.LoadMagazine.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.ProvideMaterial), SubProcesses.ProvideMaterial.TagModelPathDictionary);
            AddTagModelDictionary(GetNameOf(() => SubProcesses.UnloadMagazine), SubProcesses.UnloadMagazine.TagModelPathDictionary);

            AddTagModelDictionary(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionary(nameof(EventHandler), EventHandler.TagModelPathDictionary);

            AddSubscription();

            mEventManager.RegisterEvents(MagazineLoaderEvents.Events);

            mPlcService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }

        private void InitializeSettings()
        {          
            RecipeManager_MachineRecipeChanged(this, mRecipeManager.GetMachineRecipe());          
            RecipeManager_ProductRecipeChanged(this, mRecipeManager.GetCurrentRecipe());
        }

        public void Initialize()
        {
            mLog.Debug($"Initialize: MagazineLoaderModule");
            mPLCService.WriteValue(mInitializeCommandTagModel, true);
        }
        public bool CanStart()
        {
            return (State == (int)Platform.Lib.State.Stopped || State == (int)Platform.Lib.State.Error);
        }

        public void Start()
        {
            mLog.Debug($"Start: MagazineLoaderModule ");
            mPLCService.WriteValue(mStartCommandTagModel, true);
        }
        public bool CanStop()
        {
            return (State != (int)Platform.Lib.State.Stopped && State != (int)Platform.Lib.State.Error);
        }

        public void Stop()
        {
            mLog.Debug($"Stop: MagazineLoaderModule ");
            mPLCService.WriteValue(mStopCommandTagModel, true);
        }

        public async void ClearMaterial()
        {
            mLog.Debug($"Clear material: MagazineLoaderModule ");
            await mPLCService.WriteValueAsync(mClearMaterial, true);
        }

        public void ExecuteLoadMagazine()
        {
            mPLCService.WriteValue(mLoadMagazineCommandTagModel, true);
        }

        public void ExecuteProvideMaterial()
        {
            mPLCService.WriteValue(mProvideMaterialCommandTagModel, true);
        }

        public void ExecuteUnloadMagazine()
        {
            mPLCService.WriteValue(mUnloadMagazineCommandTagModel, true);
        }

        public void SetVelocityPercentage(int percentage)
        {
            mPLCService.WriteValue(mVelocityPercentageTagModel, (short) percentage);
        }

        public void EnableSimulation()
        {
            InSimulation = true;
            mPLCService.WriteValue(mSimulationEnabledCommandTagModel, true);
        }

        public void DisableSimulation()
        {
            InSimulation = false;
            mPLCService.WriteValue(mSimulationEnabledCommandTagModel, false);
        }

        private void RecipeManager_MachineRecipeChanged(object sender, IMachineRecipe e)
        {
            var recipe = (e as MachineRecipe);
            SubProcesses.LoadMagazine.UpdateMachineRecipeSettingsOnPLC(recipe.MagazineLoader.LoadMagazine.ConveyorUpperZPosition, 
                recipe.MagazineLoader.LoadMagazine.StartPickOffsetZ, 
                recipe.MagazineLoader.LoadMagazine.EndPickOffsetZ, 
                recipe.MagazineLoader.LoadMagazine.PickY, 
                recipe.MagazineLoader.LoadMagazine.MagazineLiftSafePositionY,
                recipe.MagazineLoader.LoadMagazine.VelocityY,
                recipe.MagazineLoader.LoadMagazine.VelocityZ);

            SubProcesses.ProvideMaterial.UpdateMachineRecipeSettingsOnPLC(recipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionY, 
                recipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionZ,
                recipe.MagazineLoader.ProvideMaterial.VelocityY,
                recipe.MagazineLoader.ProvideMaterial.VelocityZ);

            SubProcesses.UnloadMagazine.UpdateMachineRecipeSettingsOnPLC(recipe.MagazineLoader.UnloadMagazine.LowerConveyorZPosition,
                recipe.MagazineLoader.UnloadMagazine.StartPlaceOffsetZPosition, 
                recipe.MagazineLoader.UnloadMagazine.PlaceYPosition,
                recipe.MagazineLoader.UnloadMagazine.MagazineLiftSafePositionY,
                recipe.MagazineLoader.UnloadMagazine.VelocityY,
                recipe.MagazineLoader.UnloadMagazine.VelocityZ);
        }

        private void RecipeManager_ProductRecipeChanged(object sender, IProductRecipe e)
        {
            var recipe = (e as ProductRecipe);
            SubProcesses.LoadMagazine.UpdateProductRecipeSettingsOnPLC(recipe.Magazine.SlotCount);
            SubProcesses.ProvideMaterial.UpdateProductRecipeSettingsOnPLC(recipe.Magazine.LeadframesPerSlot, recipe.Magazine.BottomSlotOffset, 
                recipe.Magazine.SlotOffset, recipe.Magazine.MagazineWidth);            
        }    
    }
}
