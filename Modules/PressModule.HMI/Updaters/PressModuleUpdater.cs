﻿using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Platform.HMI;
using Platform.HMI.Resources;
using Platform.HMI.Updaters;
using Platform.Lib;

namespace PressModule.Updater
{
    public class PressModuleUpdater : BaseUpdater
    {
        private const string PressModuleStatusUpdate = nameof(PressModuleStatusUpdate);
        private const string PressModuleStateUpdate = nameof(PressModuleStateUpdate);
        private const string PressModuleSimulationChanged = nameof(PressModuleSimulationChanged);
        private const string PressModuleInProductionChanged = nameof(PressModuleInProductionChanged);
        private const string ProcessingStatusChanged = nameof(ProcessingStatusChanged);

        private const string cModuleName = "PressModule";
        private const string cStatus = "Status";
        private const string cModuleStatus = cModuleName + cStatus;
        private readonly Application.PressModule mPressModule;

        #region Subprocesses
        private const string cTrimmingStatusID = "TrimmingStatus";
        private const string cFormingStatusID = "FormingStatus";
        private const string cIndexHandlerStatusID = "IndexHandlerStatus";
        private const string cDambarCheckStatusID = "DambarCheckStatus";
        private const string cWasteHandlerStatusID = "WasteHandlerStatus";
        private const string WasteHandlerBinCountChanged = nameof(WasteHandlerBinCountChanged);

        #endregion

        #region Resources

        private const string cIndexerStatusID = "IndexerStatus";
        private const string IndexerSensorStatusChanged = nameof(IndexerSensorStatusChanged);

        private const string cTrimStatusID = "TrimStatus";
        private const string TrimActualPositionZChanged = nameof(TrimActualPositionZChanged);
        private const string TrimSensorStatusChanged = nameof(TrimSensorStatusChanged);

        private const string cDambarStatusID = "DambarStatus";
        private const string DambarCheckSensorStatusChanged = nameof(DambarCheckSensorStatusChanged);
        private const string DambarControllerResultsChanged = nameof(DambarControllerResultsChanged);

        private const string cFormStatusID = "FormStatus";        
        private const string FormActualPositionZChanged = nameof(FormActualPositionZChanged);
        private const string FormSensorStatusChanged = nameof(FormSensorStatusChanged);
        
        private const string WasteBinStatusChanged = nameof(WasteBinStatusChanged);

        private const string cVacuumCleanerStatusID = "VacuumCleanerStatus";

        #endregion        

        public PressModuleUpdater(IHubContext<SystemHub> context, PressModule.Application.PressModule pressModule)
            : base(context)
        {
            mLog.Debug("PressModuleUpdater");

            mPressModule = pressModule;           
            mPressModule.StateChanged += async (sender, e) => await SendASyncToClients(PressModuleStateUpdate);
            mPressModule.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, e);
            mPressModule.SimulationChanged += async (sender, e) => await SendSimulationChanged(cModuleName, e);
            mPressModule.InProductionChanged += async (sender, e) => await SendASyncToClients(PressModuleInProductionChanged);


            mPressModule.SubProcesses.Trimming.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cTrimmingStatusID, e);
            mPressModule.SubProcesses.Trimming.ProcessingStatusChanged += async (sender, e) => await SendASyncToClients(ProcessingStatusChanged);
            mPressModule.SubProcesses.Forming.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cFormingStatusID, e);
            mPressModule.SubProcesses.Forming.ProcessingStatusChanged += async (sender, e) => await SendASyncToClients(ProcessingStatusChanged);
            mPressModule.SubProcesses.IndexHandler.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cIndexHandlerStatusID, e);
            mPressModule.SubProcesses.IndexHandler.ProcessingStatusChanged += async (sender, e) => await SendASyncToClients(ProcessingStatusChanged);
            mPressModule.SubProcesses.Dambar.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cDambarCheckStatusID, e);
            mPressModule.SubProcesses.Dambar.ProcessingStatusChanged += async (sender, e) => await SendASyncToClients(ProcessingStatusChanged);
            mPressModule.SubProcesses.WasteHandler.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cWasteHandlerStatusID, e);
            mPressModule.SubProcesses.WasteHandler.ProcessingStatusChanged += async (sender, e) => await SendASyncToClients(ProcessingStatusChanged);
            mPressModule.SubProcesses.WasteHandler.CountChanged += async (sender, e) => await SendASyncToClients(WasteHandlerBinCountChanged);
                                   

            mPressModule.Resources.Indexer.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cIndexerStatusID, e);
            mPressModule.Resources.Indexer.SensorStatusChanged += async () => await SendASyncToClients(IndexerSensorStatusChanged);           

            mPressModule.Resources.Trim.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cTrimStatusID, e);
            mPressModule.Resources.Trim.SensorStatusChanged += async () => await SendASyncToClients(TrimSensorStatusChanged);
            mPressModule.Resources.Trim.PositionChanged += async () => await SendASyncToClients(TrimActualPositionZChanged);            
            

            mPressModule.Resources.DambarCheck.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cDambarStatusID, e);
            mPressModule.Resources.DambarCheck.SensorStatusChanged += async () => await SendASyncToClients(DambarCheckSensorStatusChanged);
            mPressModule.Resources.DambarCheck.ResultsChanged += async () => await SendASyncToClients(DambarControllerResultsChanged);

            mPressModule.Resources.Form.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cFormStatusID, e);
            mPressModule.Resources.Form.SensorStatusChanged += async () => await SendASyncToClients(FormSensorStatusChanged);
            mPressModule.Resources.Form.PositionChanged += async () => await SendASyncToClients(FormActualPositionZChanged);            

            mPressModule.Resources.WasteBin.SensorStatusChanged += async () => await SendASyncToClients(WasteBinStatusChanged);

            mPressModule.Resources.VacuumCleaner.StatusChanged += async (sender, e) => await SendStatusChanged(cModuleName, cVacuumCleanerStatusID, e);
                       
        }

        internal JsonResult GetStatusses()
        {
            Dictionary<string, string> statusses = new()
            {              
                { cTrimmingStatusID, StatusBar.ConvertToString(mPressModule.SubProcesses.Trimming.Status) },
                { cWasteHandlerStatusID, StatusBar.ConvertToString(mPressModule.SubProcesses.WasteHandler.Status) },
                { cDambarCheckStatusID, StatusBar.ConvertToString(mPressModule.SubProcesses.Dambar.Status) },
                { cFormingStatusID, StatusBar.ConvertToString(mPressModule.SubProcesses.Forming.Status) },
                { cIndexHandlerStatusID, StatusBar.ConvertToString(mPressModule.SubProcesses.IndexHandler.Status) },

                { cTrimStatusID, StatusBar.ConvertToString(mPressModule.Resources.Trim.Status) },
                { cFormStatusID, StatusBar.ConvertToString(mPressModule.Resources.Form.Status) },
                { cIndexerStatusID, StatusBar.ConvertToString(mPressModule.Resources.Indexer.Status) },
                { cDambarStatusID, StatusBar.ConvertToString(mPressModule.Resources.DambarCheck.Status) },
                { cVacuumCleanerStatusID, StatusBar.ConvertToString(mPressModule.Resources.VacuumCleaner.Status) },

                { cModuleStatus, StatusBar.ConvertToString(mPressModule.Status) },
            };

            return new JsonResult(statusses);
        }

       
        //private async Task SendPressModuleSimulationChanged(string itemToUpdate, bool value)
        //{
        //    mLog.Debug($"SendAsyncToClients: {itemToUpdate}, value: {value}");
        //    await mContext.Clients.All.SendAsync(PressModuleSimulationChanged, itemToUpdate, value);
        //}               
    }
}
