using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PressModule.HMI.Pages.Event.Events
{
    public class PressModuleInSimulation : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl;
        private readonly PressModule.Application.PressModule mPressModule;

        public PressModuleInSimulation(LineControl.Application.LineControl lineControl, PressModule.Application.PressModule pressModule)
        {
            mLineControl = lineControl;
            mPressModule = pressModule;
        }
        public void OnPostDisableSimulation()
        {
            mPressModule.DisableSimulation();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
            mPressModule.Stop();
        }
    }
}
