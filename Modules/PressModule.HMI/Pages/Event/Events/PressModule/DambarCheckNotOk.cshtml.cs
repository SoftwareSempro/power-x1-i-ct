using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Platform.Lib;
using PressModule.Application;
using PressModule.Application.SubProcesses;

namespace PressModule.Pages
{
    [BindProperties]
    public class DambarCheckNotOk : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private readonly PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;

        public void OnPostRetry()
        {
            Dambar.RetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
            mPressModule.Stop();
        }

        public void OnPostRemovedMaterial()
        {
            var deviceId = mPressModule.MaterialTracking.GetPressPositionId((int)DeviceLocation.Dambar);
            mPressModule.SubProcesses.Dambar.RemoveMaterialCommand(deviceId);
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new {state = ((State)mPressModule.State).ToString()});
        }
    }
}
