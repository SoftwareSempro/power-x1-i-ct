using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Platform.Lib;
using PressModule.Application;

namespace PressModule.Pages
{
    [BindProperties]
    public class WasteHandlerLeadframeWasteBinFull : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl;
        private readonly PressModule.Application.PressModule mPressModule;

        public WasteHandlerLeadframeWasteBinFull(LineControl.Application.LineControl lineControl, PressModule.Application.PressModule pressModule)
        {
            mLineControl = lineControl;
            mPressModule = pressModule;
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
            mPressModule.Stop();
        }

        public void OnPostRemovedMaterial()
        {
            mPressModule.SubProcesses.WasteHandler.ResetWasteHandlerCount();
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new {state = ((State)mPressModule.State).ToString()});
        }
    }
}
