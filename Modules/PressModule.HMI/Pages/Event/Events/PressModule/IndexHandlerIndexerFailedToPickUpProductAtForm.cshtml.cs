using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Platform.Lib;
using PressModule.Application;

namespace PressModule.Pages
{
    [BindProperties]
    public class IndexHandlerIndexerFailedToPickUpProductAtForm : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl;
        private readonly PressModule.Application.PressModule mPressModule;

        public IndexHandlerIndexerFailedToPickUpProductAtForm(LineControl.Application.LineControl lineControl, PressModule.Application.PressModule pressModule)
        {
            mLineControl = lineControl;
            mPressModule = pressModule;
        }
        public void OnPostStop()
        {
            mLineControl.Stop();
            mPressModule.Stop();   
        }

        public void OnPostRemovedMaterial()
        {
            var id = mPressModule.MaterialTracking.GetPressPositionId((int)DeviceLocation.Form);
            mPressModule.SubProcesses.IndexHandler.ExecuteRemoveDeviceCommand(id);
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new {state = ((State)mPressModule.State).ToString()});
        }
    }
}
