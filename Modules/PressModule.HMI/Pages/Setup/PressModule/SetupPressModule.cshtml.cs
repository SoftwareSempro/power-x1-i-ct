using Authorization.Lib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Platform.HMI.Resources;
using Platform.Lib;
using Power_X1_iCT.Model;
using PressModule.Updater;
using System;
using System.Collections.Generic;


namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]
    [Authorize(Policy = SetupPolicy.Name)]
    public class SetupPressModuleModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly PressModuleUpdater mUpdater;

        private PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;

        public PressModuleSettingsModel PressModuleSettingsModel { get; set; }
        public TrimmingSettingsModel TrimmingSettingsModel { get; set; }
        public FormingSettingsModel FormingSettingsModel { get; set; }
        public WasteHandlerSettingsModel WasteHandlerSettingsModel { get; set; }
        public bool SimulationEnabled { get; set; }
        public bool IsRunningProduction { get; set; }

        public SetupPressModuleModel(PressModuleUpdater pressModuleUpdater, ILogger<SetupPressModuleModel> logger)
        {
            mLog = logger;
            mUpdater = pressModuleUpdater;
            PressModuleSettingsModel = new PressModuleSettingsModel();
            TrimmingSettingsModel = new TrimmingSettingsModel();
            FormingSettingsModel = new FormingSettingsModel();
            WasteHandlerSettingsModel = new WasteHandlerSettingsModel();

            mPressModule.SimulationChanged += PressModule_SimulationChanged;
            PressModule_SimulationChanged(this, mPressModule.InSimulation);
        }

        private void PressModule_SimulationChanged(object? sender, bool e)
        {
            SimulationEnabled = e;
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((State)mPressModule.State).ToString() });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mPressModule.InSimulation);
        }
        public JsonResult OnPostCanProcessCommands()
        {
            var canProcess = !mPressModule.IsRunningProduction &&
                mPressModule.State == (int)State.Running &&
                mPressModule.Status != (int)Status.Busy;
            return new JsonResult(canProcess);
        }

        #region Main process
        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mPressModule.Initialize();
        }
        public JsonResult OnPostCanStart()
        {
            return new JsonResult(mPressModule.CanStart());
        }

        public void OnPostStart()
        {
            mLog.LogInformation("OnStart");
            mPressModule.Start();
        }
        public JsonResult OnPostCanStop()
        {
            return new JsonResult(mPressModule.CanStop());
        }

        public void OnPostStop()
        {
            mLog.LogInformation("OnStop");
            mPressModule.Stop();
        }

        public void OnPostTrim()
        {
            mLog.LogInformation("OnTrim");
            mPressModule.ExecuteTrim();
        }

        public void OnPostForm()
        {
            mLog.LogInformation("OnForm");
            mPressModule.ExecuteForm();
        }

        public void OnPostDambar()
        {
            mLog.LogInformation("OnDambar");
            mPressModule.ExecuteDambar();
        }

        public void OnPostIndexerToPick()
        {
            mLog.LogInformation("OnIndexerToPick");
            mPressModule.ExecuteIndexerPick();
        }

        public void OnPostIndexerToPlace()
        {
            mLog.LogInformation("OnIndexerToPlace");
            mPressModule.ExecuteIndexerPlace();
        }

        #endregion

        #region SubProcesses

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadTrimmingSettingsToPLC(TrimmingSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveTrimmingSettings(TrimmingSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshTrimmingSettings(TrimmingSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadFormingSettingsToPLC(FormingSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveFormingSettings(FormingSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshFormingSettings(FormingSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostDownloadWasteHandlerSettingsToPLC(WasteHandlerSettingsModel model) => this.CallActionIfAuthorized(() => model.Download());
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostSaveWasteHandlerSettings(WasteHandlerSettingsModel model) => this.CallActionIfAuthorized(() => model.Save(User?.Identity?.Name ?? "N/A"));
        [Authorize(Policy = EngineerPolicy.Name)]
        public IActionResult OnPostRefreshWasteHandlerSettings(WasteHandlerSettingsModel model) => this.CallActionIfAuthorized(() => model.RefreshFromRecipe());

        public void OnPostResetWasteBinCounter()
        {
            mPressModule.SubProcesses.WasteHandler.ResetWasteHandlerCount();
        }

        public JsonResult OnPostGetWastHandlerBinCount()
        {
            return new JsonResult(mPressModule.SubProcesses.WasteHandler.WasteHandlerCount);
        }

        public void OnPostRemoveTrimTool()
        {
            mPressModule.ExecuteRemoveTrimTool();
        }

        public void OnPostTrimMaintenance()
        {
            mPressModule.ExecuteTrimMaintenance();
        }

        public void OnPostRemoveFormTool()
        {
            mPressModule.ExecuteRemoveFormTool();
        }

        public void OnPostFormMaintenance()
        { 
            mPressModule.ExecuteFormMaintenance();
        }
        #endregion

        #region Resources

        #region Trim

        public void OnPostTrimGoToZ(string input1, string input2)
        {
            mPressModule.Resources.Trim.GoTo(Convert.ToDouble(input1), Convert.ToInt32(input2));
        }


        public JsonResult OnPostGetTrimActualPositionZ()
        {
            return new JsonResult(mPressModule.Resources.Trim.PositionZ);
        }

        public JsonResult OnPostGetTrimSensorStatus()
        {
           bool status = new bool();
            status = mPressModule.Resources.Trim.ToolLocked;

            return new JsonResult(status);
        }
        #endregion

        #region Form
        public void OnPostFormGoToZ(string input1, string input2)
        {
            mPressModule.Resources.Form.GoTo(Convert.ToDouble(input1), Convert.ToInt32(input2));
        }

        public JsonResult OnPostGetFormActualPositionZ()
        {
            return new JsonResult(mPressModule.Resources.Form.PositionZ);
        }

        public JsonResult OnPostGetFormSensorStatus()
        {
            bool status = new bool();
            status = mPressModule.Resources.Form.ToolLocked;

            return new JsonResult(status);
        }
        #endregion

        #region Indexer

        public void OnPostIndexerLeft()
        {
            mPressModule.Resources.Indexer.ToLeft();            
        }

        public void OnPostIndexerRight()
        {
            mPressModule.Resources.Indexer.ToRight();
        }
        public void OnPostIndexerPinDown()
        {
            mPressModule.Resources.Indexer.PinDown();
        }

        public void OnPostIndexerPinUp()
        {
            mPressModule.Resources.Indexer.PinUp();
        }

        public void OnPostIndexerVacuumOn(bool[] input)
        {
            if (input.Length >= 5)
            {
                mPressModule.Resources.Indexer.VacuumOn(input);
            }
        }

        public void OnPostIndexerVacuumOff(bool[] input)
        {
            if (input.Length >= 5)
            {
                mPressModule.Resources.Indexer.VacuumOff(input);
            }
        }

        public void OnPostIndexerDeactivateBlowOff(bool[] input)
        {
            if (input.Length >= 5)
            {
                mPressModule.Resources.Indexer.DeactivateVacuumBlowOff(input);
            }
        }

        public void OnPostIndexerActivateBlowOff(bool[] input)
        {
            if (input.Length >= 5)
            {
                mPressModule.Resources.Indexer.ActivateVacuumBlowOff(input);
            }
        }

        public JsonResult OnPostGetIndexerSensorValues()
        {
            List<bool> sensorStatusses = new()
            {
                mPressModule.Resources.Indexer.IsOnInfeed,
                mPressModule.Resources.Indexer.IsOnOutfeed,
                mPressModule.Resources.Indexer.IsUp,
                mPressModule.Resources.Indexer.IsDown,
                mPressModule.Resources.Indexer.IsVacuum1On,
                mPressModule.Resources.Indexer.IsVacuum2On,
                mPressModule.Resources.Indexer.IsVacuum3On,
                mPressModule.Resources.Indexer.IsVacuum4On,
                mPressModule.Resources.Indexer.IsVacuum5On,
                mPressModule.Resources.Indexer.IsVacuum6On,
            };

            return new JsonResult(sensorStatusses);
        }

        #endregion

        #region Dambar

        public JsonResult OnPostGetDambarCheckSensorValues()
        {
            bool status = new bool();
            status = mPressModule.Resources.DambarCheck.DambarCheckCommandResult;

            return new JsonResult(status);
        }

        #endregion

        #region VacuumCleaner

        public void OnPostVacuumCleanerOn()
        {
            mPressModule.Resources.VacuumCleaner.On();
        }

        public void OnPostVacuumCleanerOff()
        {
            mPressModule.Resources.VacuumCleaner.Off();
        }

        #endregion

        #region WasteBin

        public JsonResult OnPostGetWasteBinSensorValues()
        {
            bool status = new bool();
            status = mPressModule.Resources.WasteBin.BinPresent;

            return new JsonResult(status);
        }
        #endregion

        #endregion

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mPressModule.DisableSimulation();
            }
            else
            {
                mPressModule.EnableSimulation();
            }
        }
        public ActionResult OnPostSetVelocityPercentage(int percentage)
        {
            PressModuleSettingsModel.SetVelocityPercentage(percentage);

            return new JsonResult("Succes");
        }
    }
}

