﻿using PressModule.Application.SubProcesses;
using RecipeManager;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class TrimmingSettingsModel
    {
        private PressModule.Application.SubProcesses.Trimming mTrimming = PressModule.Application.PressModule.Instance.SubProcesses.Trimming;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double TrimTopPosition { get; set; }
        public double TrimSensorPosition { get; set; }
        public double TrimClosePosition { get; set; }
        public double TrimSafePosition { get; set; }
        public double TrimRemovePosition { get; set; }
        public double TrimMaintenancePosition { get; set; }
        public int TrimVelocity { get; set; }

        public TrimmingSettingsModel()
        {
            Trimming_SettingsChanged(this, mTrimming.Settings);
            mTrimming.SettingsChanged += Trimming_SettingsChanged;
        }

        private void Trimming_SettingsChanged(object? sender, TrimmingDataModel e)
        {
            TrimTopPosition = e.TopPosition;
            TrimSensorPosition = e.SensorPosition;
            TrimClosePosition = e.ClosePosition;
            TrimSafePosition = e.SafePosition;
            TrimRemovePosition = e.RemovePosition;
            TrimMaintenancePosition = e.MaintenancePosition;
            TrimVelocity = e.Velocity;
        }

        public void Save(string author)
        {
            if (mRecipeManager.GetCurrentRecipe() is not ProductRecipe productRecipe)
                return;
            productRecipe.Trim.TopPosition = TrimTopPosition;
            productRecipe.Trim.SensorPosition = TrimSensorPosition;
            productRecipe.Trim.ClosePosition = TrimClosePosition;           
            productRecipe.Trim.RemovePosition = TrimRemovePosition;
            productRecipe.Trim.MaintenancePosition = TrimMaintenancePosition;
            productRecipe.Trim.Velocity = TrimVelocity;
            mRecipeManager.SaveRecipe(productRecipe, author);

            if (mRecipeManager.GetMachineRecipe() is not MachineRecipe machineRecipe)
                return;
            machineRecipe.PressModule.Trimming.SafePosition = TrimSafePosition;
            mRecipeManager.SaveRecipe(machineRecipe);

            Download();
        }
        public void Download()
        {
            var dataModel = new TrimmingDataModel();
            dataModel.TopPosition = TrimTopPosition;
            dataModel.SensorPosition = TrimSensorPosition;
            dataModel.ClosePosition = TrimClosePosition;
            dataModel.SafePosition = TrimSafePosition;
            dataModel.RemovePosition = TrimRemovePosition;
            dataModel.MaintenancePosition = TrimMaintenancePosition;
            dataModel.Velocity = TrimVelocity;

            mTrimming.UpdateSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            if (mRecipeManager.GetMachineRecipe() is not MachineRecipe machineRecipe)
                return;

            TrimSafePosition = machineRecipe.PressModule.Trimming.SafePosition;

            if (mRecipeManager.GetCurrentRecipe() is not ProductRecipe productRecipe)
                return;

            TrimTopPosition = productRecipe.Trim.TopPosition;
            TrimSensorPosition = productRecipe.Trim.SensorPosition;
            TrimClosePosition = productRecipe.Trim.ClosePosition;
            TrimRemovePosition = productRecipe.Trim.RemovePosition;
            TrimMaintenancePosition = productRecipe.Trim.MaintenancePosition;
            TrimVelocity = productRecipe.Trim.Velocity;

            var trimmingDataModel = new TrimmingDataModel
            {
                TopPosition = (float)TrimTopPosition,
                SensorPosition = (float)TrimSensorPosition,
                ClosePosition = (float)TrimClosePosition,
                SafePosition = (float)TrimSafePosition,
                RemovePosition = (float)TrimRemovePosition,
                MaintenancePosition = (float)TrimMaintenancePosition,
                Velocity = (short)TrimVelocity,
            };

            mTrimming.UpdateSettings(trimmingDataModel);
        }
    }
}
