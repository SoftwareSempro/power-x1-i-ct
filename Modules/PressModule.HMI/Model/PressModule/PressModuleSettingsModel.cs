﻿using PressModule.Application;

namespace Power_X1_iCT.Model
{
    public class PressModuleSettingsModel
    {
        private PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;

        public int VelocityPercentage { get; set; }

        public PressModuleSettingsModel()
        {
            VelocityPercentage = mPressModule.Settings.VelocityPercentage;
        } 

        public void SetVelocityPercentage(int percentage)
        {
            mPressModule.SetVelocityPercentage(percentage);
        }
    }
}
