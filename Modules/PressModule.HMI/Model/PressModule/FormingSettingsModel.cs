﻿using PressModule.Application.SubProcesses;
using PressModule.PLCInterfaces.Trimming;
using RecipeManager;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class FormingSettingsModel
    {
        private PressModule.Application.SubProcesses.Forming mForming = PressModule.Application.PressModule.Instance.SubProcesses.Forming;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double FormTopPosition { get; set; }
        public double FormSensorPosition { get; set; }
        public double FormClosePosition { get; set; }
        public double FormSafePosition { get; set; }
        public double FormRemovePosition { get; set; }
        public double FormMaintenancePosition { get; set; }
        public int FormVelocity { get; set; }

        public FormingSettingsModel()
        {
            Forming_SettingsChanged(this, mForming.Settings);
            mForming.SettingsChanged += Forming_SettingsChanged;
        }
        private void Forming_SettingsChanged(object? sender, FormingDataModel e)
        {
            FormTopPosition = e.TopPosition;
            FormSensorPosition = e.SensorPosition;
            FormClosePosition = e.ClosePosition;
            FormSafePosition = e.SafePosition;
            FormRemovePosition = e.RemovePosition;
            FormMaintenancePosition = e.MaintenancePosition;
            FormVelocity = e.Velocity;
        }

        public void Save(string author)
        {
            if (mRecipeManager.GetCurrentRecipe() is not ProductRecipe productRecipe)
                return;
            productRecipe.Form.TopPosition = FormTopPosition;
            productRecipe.Form.SensorPosition = FormSensorPosition;
            productRecipe.Form.ClosePosition = FormClosePosition;            
            productRecipe.Form.RemovePosition = FormRemovePosition;
            productRecipe.Form.MaintenancePosition = FormMaintenancePosition;
            productRecipe.Form.Velocity = FormVelocity;           
            mRecipeManager.SaveRecipe(productRecipe, author);

            if (mRecipeManager.GetMachineRecipe() is not MachineRecipe machineRecipe)
                return;
            machineRecipe.PressModule.forming.SafePosition = FormSafePosition;
            mRecipeManager.SaveRecipe(machineRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new FormingDataModel();
            dataModel.TopPosition = FormTopPosition;
            dataModel.SensorPosition = FormSensorPosition;
            dataModel.ClosePosition = FormClosePosition;
            dataModel.SafePosition = FormSafePosition;
            dataModel.RemovePosition = FormRemovePosition;
            dataModel.MaintenancePosition = FormMaintenancePosition;
            dataModel.Velocity = FormVelocity;

            mForming.UpdateSettings(dataModel);
        }

        internal void RefreshFromRecipe()
        {
            if (mRecipeManager.GetMachineRecipe() is not MachineRecipe machineRecipe)
                return;

            FormSafePosition = machineRecipe.PressModule.forming.SafePosition;

            if (mRecipeManager.GetCurrentRecipe() is not ProductRecipe productRecipe)
                return;

            FormTopPosition = productRecipe.Form.TopPosition;
            FormSensorPosition = productRecipe.Form.SensorPosition;
            FormClosePosition = productRecipe.Form.ClosePosition;
            FormRemovePosition = productRecipe.Form.RemovePosition;
            FormMaintenancePosition = productRecipe.Form.MaintenancePosition;
            FormVelocity = productRecipe.Form.Velocity;

            var formingDataModel = new FormingDataModel
            {
                TopPosition = (float)FormTopPosition,
                SensorPosition = (float)FormSensorPosition,
                ClosePosition = (float)FormClosePosition,
                SafePosition = (float)FormSafePosition,
                RemovePosition = (float)FormRemovePosition,
                MaintenancePosition = (float)FormMaintenancePosition,
                Velocity = (short)FormVelocity,
            };

            mForming.UpdateSettings(formingDataModel);
        }
    }
}
