﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/SystemHub").build();

    // Module region
    connection.on("PressModuleStatusUpdate", function (item, status) {
        receiveStatusChanged(item, status);
        getCanProcessCommands();
    });

    connection.on("PressModuleStateUpdate", function (item, state) {
        getState();
        getCanProcessCommands();
        getCanStart();
        getCanStop();
    });

    connection.on("PressModuleInProductionChanged", function () {
        getCanProcessCommands();
    });

    connection.on("PressModuleSimulationChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    connection.on("LineControlDataUpdated", function (value) {
        updateCovers(value.coversLocked);
    });

    connection.on("WasteHandlerBinCountChanged", function () {
        getWastHandlerBinCount()
    });

    // Resources
    connection.on("TrimActualPositionZChanged", function () {
        getTrimActualPositionZ(false);
    });

    connection.on("TrimSensorStatusChanged", function () {
        getTrimSensorStatus();
    });

    connection.on("FormActualPositionZChanged", function () {
        getFormActualPositionZ(false);
    });

    connection.on("FormSensorStatusChanged", function () {
        getFormSensorStatus();
    });

    connection.on("IndexerSensorStatusChanged", function () {
        getIndexerSensorValues();
    });

    connection.on("DambarCheckSensorStatusChanged", function () {
        getDambarCheckSensorStatus();
    });

    connection.on("WasteBinStatusChanged", function () {
        getWasteBinSensorStatus();
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
                getCanProcessCommands();
                getCanStart();
                getCanStop();
                getCoversLocked();
                getWastHandlerBinCount();
                getTrimActualPositionZ(true);
                getTrimSensorStatus();
                getFormActualPositionZ(true);
                getFormSensorStatus();
                getIndexerSensorValues();
                getDambarCheckSensorStatus();
                getWasteBinSensorStatus();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupPressModule?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('PressModuleState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        $(item).removeClass('error');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupPressModule?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        $(item).removeClass('busy')
        item.textContent = status;
        $(item).addClass(status.toLowerCase());
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupPressModule?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get simulation enabled");
            },
        });
    }

    function receiveSimulationEnabled(value) {
        var item = document.getElementById('PressModuleSimulation');
        item.checked = value;
    }

    function getCoversLocked() {
        $.ajax({
            url: "/Setup/Setup?handler=AreCoversLocked",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                updateCovers(value);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }
    function updateCovers(value) {
        console.log('LineControlDataUpdated, covers locked: ' + value);
        if (value != null) {
            displayButton('ExecuteLockCoversButton', !value);
            displayButton('ExecuteUnlockCoversButton', value);
        }
    }

    function getCanProcessCommands() {
        $.ajax({
            url: "./SetupPressModule?handler=CanProcessCommands",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canProcess) {
                receiveCanProcessCommands(canProcess);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveCanProcessCommands(canProcess) {
        disableButton('ExecuteTrimmingButton', !canProcess);
        disableButton('ExecuteWasteHandlerButton', !canProcess);
        disableButton('ExecuteDambarButton', !canProcess);
        disableButton('ExecuteFormingButton', !canProcess);
        disableButton('ExecuteIndexerToPickButton', !canProcess);
        disableButton('ExecuteIndexerToPlaceButton', !canProcess);

        //Maintenance
        disableButton('GoToFormMaintenance', !canProcess);
        disableButton('GoToTrimMaintenance', !canProcess);
        disableButton('RemoveFormTool', !canProcess);
        disableButton('RemoveTrimTool', !canProcess);

        //Resources
        disableButton('FormGoToZButton', !canProcess);
        disableButton('IndexerLeftButton', !canProcess);
        disableButton('IndexerRightButton', !canProcess);
        disableButton('IndexerUpButton', !canProcess);
        disableButton('IndexerDownButton', !canProcess);
        disableButton('VacuumOnButton', !canProcess);
        disableButton('VacuumOffButton', !canProcess);
        disableButton('ActivateBlowOffButton', !canProcess);
        disableButton('DeactivateBlowOffButton', !canProcess);
        disableButton('TrimGoToZButton', !canProcess);
        disableButton('VacuumCleanerOn', !canProcess);
        disableButton('VacuumCleanerOff', !canProcess);
    }

    function getCanStart() {
        $.ajax({
            url: "./SetupPressModule?handler=CanStart",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStart) {
                disableButton('ExecuteStartButton', !canStart);
                disableButton('ExecuteInitializeButton', !canStart);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getCanStop() {
        $.ajax({
            url: "./SetupPressModule?handler=CanStop",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStop) {
                disableButton('ExecuteStopButton', !canStop);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getWastHandlerBinCount() {
        $.ajax({
            url: "./SetupPressModule?handler=GetWastHandlerBinCount",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (count) {
                receiveWasteHandlerBinCount(count);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveWasteHandlerBinCount(count) {
        document.getElementById('WasteHandlerBinCount').value = count;
    }

    function getTrimActualPositionZ(onConnected) {
        $.ajax({
            url: "./SetupPressModule?handler=GetTrimActualPositionZ",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (position) {
                receiveTrimActualPositionZ(position, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveTrimActualPositionZ(position, onConnected) {
        var newValue = position.toFixed(2);
        document.getElementById('TrimActualPositionZ').value = newValue;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("TrimPositionZ").value = newValue;
        }
    }

    function getTrimSensorStatus() {
        $.ajax({
            url: "./SetupPressModule?handler=GetTrimSensorStatus",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (status) {
                receiveTrimSensorStatus(status);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveTrimSensorStatus(status) {
        document.getElementById('TrimToolLocked').checked = status;
    }

    function getFormActualPositionZ(onConnected) {
        $.ajax({
            url: "./SetupPressModule?handler=GetFormActualPositionZ",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (position) {
                receiveFormActualPositionZ(position, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveFormActualPositionZ(position, onConnected) {
        var newValue = position.toFixed(2);
        document.getElementById('FormActualPositionZ').value = newValue;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("FormPositionZ").value = newValue;
        }
    }

    function getFormSensorStatus() {
        $.ajax({
            url: "./SetupPressModule?handler=GetFormSensorStatus",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (status) {
                receiveFormSensorStatus(status);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveFormSensorStatus(status) {
        document.getElementById('FormToolLocked').checked = status;
    }

    function getIndexerSensorValues() {
        $.ajax({
            url: "./SetupPressModule?handler=GetIndexerSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetIndexerSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetIndexerSensorValues(sensorStatusses) {
        document.getElementById('IndexerIsOnInfeedCheckbox').checked = sensorStatusses[0];
        document.getElementById('IndexerIsOnOutfeedCheckbox').checked = sensorStatusses[1];
        document.getElementById('IndexerIsUpCheckbox').checked = sensorStatusses[2];
        document.getElementById('IndexerIsDownCheckbox').checked = sensorStatusses[3];
        document.getElementById('IndexerIsVacuumOn1Checkbox').checked = sensorStatusses[4];
        document.getElementById('IndexerIsVacuumOn2Checkbox').checked = sensorStatusses[5];
        document.getElementById('IndexerIsVacuumOn3Checkbox').checked = sensorStatusses[6];
        document.getElementById('IndexerIsVacuumOn4Checkbox').checked = sensorStatusses[7];
        document.getElementById('IndexerIsVacuumOn5Checkbox').checked = sensorStatusses[8];
        document.getElementById('IndexerIsVacuumOn6Checkbox').checked = sensorStatusses[9];
    }

    function getDambarCheckSensorStatus() {
        $.ajax({
            url: "./SetupPressModule?handler=GetDambarCheckSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatus) {
                receiveDambarCheckValues(sensorStatus);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveDambarCheckValues(sensorStatus) {
        document.getElementById('DambarCheckOkCheckbox').checked = sensorStatus;
    }
    
    function getWasteBinSensorStatus() {
        $.ajax({
            url: "./SetupPressModule?handler=GetWasteBinSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatus) {
                receiveWastBinValues(sensorStatus);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveWastBinValues(sensorStatus) {
        document.getElementById('WasteBinCheckbox').checked = sensorStatus;
    }
})

$("#CloseMainPressModule").click(function (e) {
    e.preventDefault();
    $("#SetupMainPressModuleContainer").hide();
});

$("#CloseSetupTrimming").click(function (e) {
    e.preventDefault();
    $("#SetupTrimmingContainer").hide();
});

$("#CloseMaintenanceTrimming").click(function (e) {
    e.preventDefault();
    $("#MaintenanceTrimmingContainer").hide();
});

$("#CloseSetupWasteHandler").click(function (e) {
    e.preventDefault();
    $("#SetupWasteHandlerContainer").hide();
});

$("#CloseSetupForming").click(function (e) {
    e.preventDefault();
    $("#SetupFormingContainer").hide();
});

$("#CloseMaintenanceForming").click(function (e) {
    e.preventDefault();
    $("#MaintenanceFormingContainer").hide();
});


$("#CloseTrimDebug").click(function (e) {
    e.preventDefault();
    $("#DebugTrimContainer").hide();
});

$("#CloseFormDebug").click(function (e) {
    e.preventDefault();
    $("#DebugFormContainer").hide();
});

$("#CloseIndexerDebug").click(function (e) {
    e.preventDefault();
    $("#DebugIndexerContainer").hide();
});

$("#CloseDambarDebug").click(function (e) {
    e.preventDefault();
    $("#DebugDambarContainer").hide();
});

$("#CloseWasteBinDebug").click(function (e) {
    e.preventDefault();
    $("#DebugWasteBinContainer").hide();
});

$("#CloseVacuumCleanerDebug").click(function (e) {
    e.preventDefault();
    $("#DebugVacuumCleanerContainer").hide();
});

$(function () {
    $("#SetupMainPressModule").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainPressModuleContainer");
    });

    $("#SetupTrimming").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupTrimmingContainer");
    });

    $("#MaintenanceTrimming").click(function (e) {
        OnClickProcessSettingsButton(e, "#MaintenanceTrimmingContainer");
    });

    $("#SetupWasteHandler").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupWasteHandlerContainer");
    });

    $("#SetupForming").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupFormingContainer");
    });

    $("#MaintenanceForming").click(function (e) {
        OnClickProcessSettingsButton(e, "#MaintenanceFormingContainer");
    });
    

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }


    $("#DebugTrim").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugTrimContainer");
    });

    $("#DebugForm").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugFormContainer");
    });

    $("#DebugIndexer").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugIndexerContainer");
    });

    $("#DebugDambar").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugDambarContainer");
    });

    $("#DebugWasteBin").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugWasteBinContainer");
    });

    $("#DebugVacuumCleaner").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugVacuumCleanerContainer");
    });

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupMainPressModuleContainer").hide();
        $("#SetupTrimmingContainer").hide();
        $("#MaintenanceTrimmingContainer").hide();        
        $("#SetupWasteHandlerContainer").hide();
        $("#SetupFormingContainer").hide();
        $("#MaintenanceFormingContainer").hide();
    }

    function hideResourceContainers() {
        $("#DebugTrimContainer").hide();
        $("#DebugFormContainer").hide();
        $("#DebugIndexerContainer").hide();
        $("#DebugDambarContainer").hide();
        $("#DebugWasteBinContainer").hide();
        $("#DebugVacuumCleanerContainer").hide();
    }
});
