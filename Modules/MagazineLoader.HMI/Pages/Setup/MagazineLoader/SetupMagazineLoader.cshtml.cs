using Authorization.Lib;
using MagazineLoader;
using MagazineLoader.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Platform.HMI.Resources;
using Platform.Lib;
using Power_X1_iCT.Model;

namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]
    [Authorize(Policy = SetupPolicy.Name)]
    public class SetupMagazineLoaderModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly MagazineLoaderUpdater mUpdater;
        private MagazineLoaderModule mMagazineLoaderModule = MagazineLoaderModule.Instance;

        public MagazineLoaderSettingsModel MagazineLoaderSettingsModel { get; set; }

        public LoadMagazineSettingsModel LoadMagazineSettingsModel { get; set; }

        public ProvideMaterialSettingsModel ProvideMaterialSettingsModel { get; set; }

        public UnloadMagazineSettingsModel UnloadMagazineSettingsModel { get; set; }

        public bool SimulationEnabled { get; set; }

        public bool IsRunningProduction { get; set; }

        public SetupMagazineLoaderModel(MagazineLoaderUpdater magazineLoaderUpdater, ILogger<SetupMagazineLoaderModel> logger)
        {
            mLog = logger;
            mUpdater = magazineLoaderUpdater;
            MagazineLoaderSettingsModel = new MagazineLoaderSettingsModel();
            LoadMagazineSettingsModel = new LoadMagazineSettingsModel();
            ProvideMaterialSettingsModel = new ProvideMaterialSettingsModel();
            UnloadMagazineSettingsModel = new UnloadMagazineSettingsModel();

            mMagazineLoaderModule.SimulationChanged += MagazineLoaderModule_SimulationChanged;
            MagazineLoaderModule_SimulationChanged(this, mMagazineLoaderModule.InSimulation);
        }

        private void MagazineLoaderModule_SimulationChanged(object? sender, bool e)
        {
            SimulationEnabled = e;
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((State)mMagazineLoaderModule.State).ToString() });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mMagazineLoaderModule.InSimulation);
        }

        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mMagazineLoaderModule.Initialize();
        }
        public JsonResult OnPostCanStart()
        {
            return new JsonResult(mMagazineLoaderModule.CanStart());
        }

        public void OnPostStart()
        {
            mMagazineLoaderModule.Start();
        }
        public JsonResult OnPostCanStop()
        {
            return new JsonResult(mMagazineLoaderModule.CanStop());
        }

        public void OnPostStop()
        {
            mMagazineLoaderModule.Stop();
        }
        public JsonResult OnPostCanProcessCommands()
        {
            var canProcess = !mMagazineLoaderModule.IsRunningProduction &&
                mMagazineLoaderModule.State == (int)State.Running &&
                mMagazineLoaderModule.Status != (int)StatusBar.Status.Busy;
            return new JsonResult(canProcess);

        }

        public void OnPostLoadMagazine()
        {
            mMagazineLoaderModule.ExecuteLoadMagazine();
        }

        public void OnPostProvideMaterial()
        {
            mMagazineLoaderModule.ExecuteProvideMaterial();
        }

        public void OnPostUnloadMagazine()
        {
            mMagazineLoaderModule.ExecuteUnloadMagazine();
        }
        #region Resources
        #region LowerHatch
        public void OnPostLowerHatchOpen()
        {
            mMagazineLoaderModule.Resources.LowerHatch.HatchOpen();
        }

        public void OnPostLowerHatchClose()
        {
            mMagazineLoaderModule.Resources.LowerHatch.HatchClose();
        }

        public JsonResult OnPostGetLowerHatchSensorValues()
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.LowerHatch.HatchIsOpen;
            sensorStatusses[1] = mMagazineLoaderModule.Resources.LowerHatch.HatchIsClosed;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #region LowerConveyor
        public void OnPostLowerConveyorStart()
        {
            mMagazineLoaderModule.Resources.LowerConveyor.ConveyorStart();
        }

        public void OnPostLowerConveyorStop()
        {
            mMagazineLoaderModule.Resources.LowerConveyor.ConveyorStop();
        }

        public JsonResult OnPostGetLowerConveyorSensorValues()
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.LowerConveyor.MagazinePresent;
            sensorStatusses[1] = mMagazineLoaderModule.Resources.LowerConveyor.ConveyorFull;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #region Lift      

        public void OnPostLiftGoToY(string position, string velocity)
        {
            mMagazineLoaderModule.Resources.Lift.GoTo(Convert.ToDouble(position), mMagazineLoaderModule.Resources.Lift.PositionZ, Convert.ToInt32(velocity));
        }

        public void OnPostLiftGoToZ(string position, string velocity)
        {
            mMagazineLoaderModule.Resources.Lift.GoTo(mMagazineLoaderModule.Resources.Lift.PositionY, Convert.ToDouble(position), Convert.ToInt32(velocity));
        }

        public void OnPostGripperOpen()
        {
            mMagazineLoaderModule.Resources.Lift.GripperOpen();
        }

        public void OnPostGripperClose()
        {
            mMagazineLoaderModule.Resources.Lift.GripperClose();
        }
       
        public JsonResult OnPostGetLiftActualPositionY()
        {
            return new JsonResult(mMagazineLoaderModule.Resources.Lift.PositionY);
        }


        public JsonResult OnPostGetLiftActualPositionZ()
        {
            return new JsonResult(mMagazineLoaderModule.Resources.Lift.PositionZ);
        }

        public JsonResult OnPostGetGripperSensorValues()
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.Lift.IsGripperOpen;
            sensorStatusses[1] = mMagazineLoaderModule.Resources.Lift.IsGripperClosed;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #region Upperhatch
        public void OnPostUpperHatchOpen()
        {
            mMagazineLoaderModule.Resources.UpperHatch.HatchOpen();
        }

        public void OnPostUpperHatchClose()
        {
            mMagazineLoaderModule.Resources.UpperHatch.HatchClose();
        }

        public JsonResult OnPostGetUpperHatchSensorValues()
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.UpperHatch.HatchIsOpen;
            sensorStatusses[1] = mMagazineLoaderModule.Resources.UpperHatch.HatchIsClosed;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #region UpperConveyor
        public void OnPostUpperConveyorStart()
        {
            mMagazineLoaderModule.Resources.UpperConveyor.ConveyorStart();
        }

        public void OnPostUpperConveyorStop()
        {
            mMagazineLoaderModule.Resources.UpperConveyor.ConveyorStop();
        }

        public JsonResult OnPostGetUpperConveyorSensorValues()
        {
            bool[] sensorStatusses = new bool[2];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.UpperConveyor.MagazinePresent;
            sensorStatusses[1] = mMagazineLoaderModule.Resources.UpperConveyor.ConveyorAlmostEmpty;

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #region LeadframePusher
        public JsonResult OnPostGetLeadframePusherSensorValues()
        {
            List<bool> sensorStatusses = new()
            {
                mMagazineLoaderModule.Resources.LeadFramePusher.PrimairPusherIn,
                mMagazineLoaderModule.Resources.LeadFramePusher.PrimairPusherMiddle,
                mMagazineLoaderModule.Resources.LeadFramePusher.PrimairPusherOut,
                mMagazineLoaderModule.Resources.LeadFramePusher.SecondairPusherIn,
                mMagazineLoaderModule.Resources.LeadFramePusher.SecondairPusherOut
            };

            return new JsonResult(sensorStatusses);
        }

        public void OnPostPrimairPusherRetrieve()
        {            
                mMagazineLoaderModule.Resources.LeadFramePusher.PrimairPusherRetrieve();
        }

        public void OnPostPrimairPusherPush()
        {
            mMagazineLoaderModule.Resources.LeadFramePusher.PrimairPusherPush();
        }

        public void OnPostSecondairPusherRetrieve()
        {
            mMagazineLoaderModule.Resources.LeadFramePusher.SecondairPusherRetrieve();
        }

        public void OnPostSecondairPusherPush()
        {
            mMagazineLoaderModule.Resources.LeadFramePusher.SecondairPusherPush();
        }
        #endregion

        #region Outfeed
        public JsonResult OnPostGetOutfeedSensorValues()
        {
            bool[] sensorStatusses = new bool[1];
            sensorStatusses[0] = mMagazineLoaderModule.Resources.Outfeed.ProductAtTransfer;            

            return new JsonResult(sensorStatusses);
        }
        #endregion

        #endregion
        public void OnPostDownloadLoadMagazineSettingsToPLC(LoadMagazineSettingsModel model)
        {
            model.Download();
        }               
        public IActionResult OnPostDownloadProvideMaterialSettingsToPLC()
        {
            ProvideMaterialSettingsModel.Download();

            // otherwise do some processing
            return RedirectToPage();
        }

        public void OnPostDownloadUnloadMagazineSettingsToPLC(UnloadMagazineSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveLoadMagazineSettings()
        {
            LoadMagazineSettingsModel.Save(User?.Identity?.Name ?? "N/A");
        }
        public void OnPostSaveProvideMaterialSettings()
        {
            ProvideMaterialSettingsModel.Save(User?.Identity?.Name ?? "N/A");
        }

        public void OnPostSaveUnloadMagazineSettings()
        {
            UnloadMagazineSettingsModel.Save();
        }

        public IActionResult OnPostRefreshLoadMagazineSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public IActionResult OnPostRefreshProvideMaterialsSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public IActionResult OnPostRefreshUnloadMagazineSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mMagazineLoaderModule.DisableSimulation();
            }
            else
            {
                mMagazineLoaderModule.EnableSimulation();
            }
        }

        public ActionResult OnPostSetVelocityPercentage(int percentage)
        {
            MagazineLoaderSettingsModel.SetVelocityPercentage(percentage);

            return new JsonResult("Succes");
        }
    }
}
