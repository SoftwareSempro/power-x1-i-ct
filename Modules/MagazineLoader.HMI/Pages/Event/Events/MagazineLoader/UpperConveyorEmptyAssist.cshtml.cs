using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MagazineLoader.Pages
{
    [BindProperties]
    public class UpperConveyorEmptyAssist : PageModel
    {
        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;
        private MagazineLoader.Application.MagazineLoaderModule mMagazineLoader = MagazineLoader.Application.MagazineLoaderModule.Instance;

        public void OnPostContinue()
        {
            mMagazineLoader.SubProcesses.LoadMagazine.ExecuteContinueCommand();
        }
        public void OnPostRetry()
        {
            mMagazineLoader.SubProcesses.LoadMagazine.ExecuteRetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
        }

        public void OnPostCloseLot()
        {
            mLotControl.CloseLot();
        }
    }
}
