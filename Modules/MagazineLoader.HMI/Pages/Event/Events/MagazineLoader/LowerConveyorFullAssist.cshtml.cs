using MagazineLoader.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MagazineLoader.Pages
{
    [BindProperties]
    public class LowerConveyorFullAssist : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private readonly MagazineLoaderModule mMagazineLoader = MagazineLoaderModule.Instance;

        public void OnPostContinue()
        {
            mMagazineLoader.SubProcesses.UnloadMagazine.ExecuteContinueCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
        }
    }
}
