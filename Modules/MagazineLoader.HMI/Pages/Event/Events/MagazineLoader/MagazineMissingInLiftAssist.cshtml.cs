using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MagazineLoader.Pages
{
    [BindProperties]
    public class MagazineMissingInLiftAssist : PageModel
    {
        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private MagazineLoader.Application.MagazineLoaderModule mMagazineLoader = MagazineLoader.Application.MagazineLoaderModule.Instance;

        public void OnPostRetry()
        {
            mMagazineLoader.SubProcesses.LoadMagazine.ExecuteRetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
            mMagazineLoader.Stop();
        }
    }
}
