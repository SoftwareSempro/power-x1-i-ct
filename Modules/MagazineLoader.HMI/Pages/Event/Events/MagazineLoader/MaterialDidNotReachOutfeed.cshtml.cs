using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Platform.Lib;

namespace MagazineLoader.Pages
{
    [BindProperties]
    public class MaterialDidNotReachOutfeedAssist : PageModel
    {
        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private MagazineLoader.Application.MagazineLoaderModule mMagazineLoader = MagazineLoader.Application.MagazineLoaderModule.Instance;

        public void OnPostRetry()
        {
            mMagazineLoader.SubProcesses.ProvideMaterial.ExecuteRetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
            mMagazineLoader.Stop();
        }

        public void OnPostRemovedMaterial()
        {
            var deviceId = mMagazineLoader.MaterialTracking.OutfeedLeadframeId;
            mMagazineLoader.SubProcesses.ProvideMaterial.ExecuteRemoveMaterialCommand(deviceId);
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((State)mMagazineLoader.State).ToString() });
        }
    }
}
