using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MagazineLoader.Pages
{
    [BindProperties]
    public class MagazineLoaderUnloadMainDoorNotLockedAssist : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private readonly MagazineLoader.Application.MagazineLoaderModule mMagazineLoader = MagazineLoader.Application.MagazineLoaderModule.Instance;
        public void OnPostRetry()
        {
            mMagazineLoader.SubProcesses.UnloadMagazine.ExecuteRetryCommand();
        }

        public void OnPostStop()
        {
            mLineControl.Stop();
        }
    }
}
