﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/SystemHub").build();

    // Module region
    connection.on("MagazineLoaderStatusUpdate", function (item, status) {
        receiveStatusChanged(item, status);
        getCanProcessCommands();
    });

    connection.on("MagazineLoaderStateUpdate", function () {
        getCanProcessCommands();
        getCanStart();
        getCanStop();
        getState();
    });

    connection.on("MagazineLoaderInProductionChanged", function () {
        getCanProcessCommands();
    });

    connection.on("MagazineLoaderSimulationChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    connection.on("LineControlDataUpdated", function (value) {
        updateCovers(value.coversLocked);
    });

    connection.on("UpperHatchSensorStatusChanged", function () {
        getUpperHatchSensorValues();
    });

    connection.on("UpperConveyorSenserStatusChanged", function () {
        getUpperConveyorSensorValues();
    });

    connection.on("LowerHatchSensorStatusChanged", function () {
        getLowerHatchSensorValues();
    });

    connection.on("LowerConveyorSenserStatusChanged", function () {
        getLowerConveyorSensorValues();
    });

    connection.on("LeadframePusherSensorStatusChanged", function () {
        getLeadframePusherSensorValues();
    });

    connection.on("OutfeedSensorStatusChanged", function () {
        getOutfeedSensorValues();
    });

    // Positions
    connection.on("LiftActualPositionYChanged", function () {
        getLiftActualPositionY(false);
    });

    connection.on("LiftActualPositionZChanged", function () {
        getLiftActualPositionZ(false);
    });

    connection.on("GripperSensorStatusChanged", function () {
        getGripperSensorValues();
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
                getCanProcessCommands();
                getCanStart();
                getCanStop();
                getCoversLocked();
                getGripperSensorValues();
                getUpperHatchSensorValues();
                getUpperConveyorSensorValues();
                getLowerHatchSensorValues();
                getLowerConveyorSensorValues();
                getLeadframePusherSensorValues();
                getLiftActualPositionZ(true);
                getLiftActualPositionY(true);
                getOutfeedSensorValues();
                getCanProcessCommands();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('MagazineLoaderState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        $(item).removeClass('error');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        $(item).removeClass('busy')
        item.textContent = status;
        $(item).addClass(status.toLowerCase());
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveSimulationEnabled(value) {
        var item = document.getElementById('MagazineLoaderSimulation');
        item.checked = value;
    }

    function getCanProcessCommands() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=CanProcessCommands",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canProcess) {
                receiveCanProcessCommands(canProcess);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getCoversLocked() {
        $.ajax({
            url: "/Setup/Setup?handler=AreCoversLocked",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                updateCovers(value);
            },
            error: function (response) {
                console.log(response);
            },
        });
    }
    function updateCovers(value) {
        console.log('LineControlDataUpdated, covers locked: ' + value);
        if (value != null) {
            displayButton('ExecuteLockCoversButton', !value);
            displayButton('ExecuteUnlockCoversButton', value);
        }
    }

    function receiveCanProcessCommands(canProcess) {
        disableButton('ExecuteLoadMagazineButton', !canProcess);
        disableButton('ExecuteProvideMaterialButton', !canProcess);
        disableButton('ExecuteUnloadMagazineButton', !canProcess);

        //Resources
        disableButton('PrimairPusherRetrieve', !canProcess);
        disableButton('PrimairPusherPush', !canProcess);
        disableButton('SecondairPusherRetrieve', !canProcess);
        disableButton('SecondairPusherPush', !canProcess);
        disableButton('GripperOpen', !canProcess);
        disableButton('GripperClose', !canProcess);
        disableButton('LiftGoToYButton', !canProcess);
        disableButton('LiftGoToZButton', !canProcess);
        disableButton('ExecuteLowerConveyorStart', !canProcess);
        disableButton('ExecuteLowerConveyorStop', !canProcess);
        disableButton('ExecuteLowerHatchOpen', !canProcess);
        disableButton('ExecuteLowerHatchClose', !canProcess);
        disableButton('ExecuteUpperConveyorStart', !canProcess);
        disableButton('ExecuteUpperConveyorStop', !canProcess);
        disableButton('ExecuteUpperHatchOpen', !canProcess);
        disableButton('ExecuteUpperHatchClose', !canProcess);
    }

    function getCanStart() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=CanStart",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStart) {
                disableButton('ExecuteStartButton', !canStart);
                disableButton('ExecuteInitializeButton', !canStart);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getCanStop() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=CanStop",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (canStop) {
                disableButton('ExecuteStopButton', !canStop);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function getUpperHatchSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetUpperHatchSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetUpperHatchSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetUpperHatchSensorValues(sensorStatusses) {
        document.getElementById('UpperHatchOpenCheckbox').checked = sensorStatusses[0];
        document.getElementById('UpperHatchDownCheckbox').checked = sensorStatusses[1];
    }

    function getUpperConveyorSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetUpperConveyorSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetUpperConveyorSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetUpperConveyorSensorValues(sensorStatusses) {
        document.getElementById('UpperConveyorProductAtTransfer').checked = sensorStatusses[0];
        document.getElementById('UpperConveyorAlmostEmpty').checked = sensorStatusses[1];
    }

    function getLowerHatchSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetLowerHatchSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetLowerHatchSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetLowerHatchSensorValues(sensorStatusses) {
        document.getElementById('LowerHatchOpenCheckbox').checked = sensorStatusses[0];
        document.getElementById('LowerHatchClosedCheckbox').checked = sensorStatusses[1];
    }

    function getLowerConveyorSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetLowerConveyorSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetLowerConveyorSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetLowerConveyorSensorValues(sensorStatusses) {
        document.getElementById('LowerConveyorMagazinePresent').checked = sensorStatusses[0];
        document.getElementById('LowerConveyorAlmostFull').checked = sensorStatusses[1];
    }

    function getLeadframePusherSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetLeadframePusherSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receiveGetLeadframePusherSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveGetLeadframePusherSensorValues(sensorStatusses) {
        document.getElementById('PrimairRetrievedCheckbox').checked = sensorStatusses[0];
        document.getElementById('PrimairPushedCheckbox').checked = sensorStatusses[1] || sensorStatusses[2];
        document.getElementById('SecondairRetrievedCheckbox').checked = sensorStatusses[3];
        document.getElementById('SecondairPushedCheckbox').checked = sensorStatusses[4];      
    }

    function getLiftActualPositionY(onConnected) {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetLiftActualPositionY",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (position) {
                receiveLiftActualPositionY(position, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveLiftActualPositionY(position, onConnected) {
        var newValue = position.toFixed(2);
        document.getElementById('LiftActualPositionY').value = newValue;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("LiftPositionY").value = newValue;
        }
    }

    function getLiftActualPositionZ(onConnected) {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetLiftActualPositionZ",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (position) {
                receiveLiftActualPositionZ(position, onConnected);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receiveLiftActualPositionZ(position, onConnected) {
        var newValue = position.toFixed(2);
        document.getElementById('LiftActualPositionZ').value = newValue;

        //On connection we set the actual value on the 'go to' value
        if (onConnected) {
            document.getElementById("LiftPositionZ").value = newValue;
        }
    }

    function getGripperSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetGripperSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receivegetGripperSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receivegetGripperSensorValues(sensorStatusses) {
        document.getElementById('GripperOpenCheckbox').checked = sensorStatusses[0];
        document.getElementById('GripperClosedCheckbox').checked = sensorStatusses[1];
    }

    function getOutfeedSensorValues() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetOutfeedSensorValues",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (sensorStatusses) {
                receivegetOutfeedSensorValues(sensorStatusses);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
            },
        });
    }

    function receivegetOutfeedSensorValues(sensorStatusses) {
        document.getElementById('ProductAtOutfeed').checked = sensorStatusses[0];
    }

    $("#PrimairPusherRetrieve").click(function (e) {
        e.preventDefault();

        sendCommand("SetupMagazineLoader", "PrimairPusherRetrieve");
    });

    $("#PrimairPusherPush").click(function (e) {
        e.preventDefault();
        sendCommand("SetupMagazineLoader", "PrimairPusherPush");
    });

    $("#SecondairPusherRetrieve").click(function (e) {
        e.preventDefault();
        sendCommand("SetupMagazineLoader", "SecondairPusherRetrieve");
    });

    $("#SecondairPusherPush").click(function (e) {
        e.preventDefault();
        sendCommand("SetupMagazineLoader", "SecondairPusherPush");
    });
})



$("#CloseMainMagazineLoader").click(function (e) {
    e.preventDefault();
    $("#SetupMainMagazineLoaderContainer").hide();
});

$("#CloseSetupLoadMagazine").click(function (e) {
    e.preventDefault();
    $("#SetupLoadMagazineContainer").hide();
});

$("#CloseSetupUnloadMagazine").click(function (e) {
    e.preventDefault();
    $("#SetupUnloadMagazineContainer").hide();
});

$("#CloseProvideMaterial").click(function (e) {
    e.preventDefault();
    $("#SetupProvideMaterialContainer").hide();
});

$("#CloseUpperConveyorDebug").click(function (e) {
    e.preventDefault();
    $("#DebugUpperConveyorContainer").hide();
});

$("#CloseUpperHatchDebug").click(function (e) {
    e.preventDefault();
    $("#DebugUpperHatchContainer").hide();
});

$("#CloseLiftDebug").click(function (e) {
    e.preventDefault();
    $("#DebugLiftContainer").hide();
});

$("#CloseLeadframePusherDebug").click(function (e) {
    e.preventDefault();
    $("#DebugLeadframePusherContainer").hide();
});

$("#CloseLowerConveyorDebug").click(function (e) {
    e.preventDefault();
    $("#DebugLowerConveyorContainer").hide();
});

$("#CloseLowerHatchDebug").click(function (e) {
    e.preventDefault();
    $("#DebugLowerHatchContainer").hide();
});

$("#CloseOutfeedDebug").click(function (e) {
    e.preventDefault();
    $("#DebugOutfeedContainer").hide();
});

$(function () {
    $("#SetupMainMagazineLoader").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainMagazineLoaderContainer");
    });

    $("#SetupLoadMagazine").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupLoadMagazineContainer");
    });

    $("#SetupProvideMaterial").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupProvideMaterialContainer");
    });

    $("#SetupUnloadMagazine").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupUnloadMagazineContainer");
    });

    $("#DebugUpperConveyor").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugUpperConveyorContainer");
    });

    $("#DebugUpperHatch").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugUpperHatchContainer");
    });

    $("#DebugLift").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugLiftContainer");
    });

    $("#DebugLeadFramePusher").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugLeadframePusherContainer");
    });

    $("#DebugLowerConveyor").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugLowerConveyorContainer");
    });

    $("#DebugLowerHatch").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugLowerHatchContainer");
    });

    $("#DebugOutfeed").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugOutfeedContainer");
    });

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupLoadMagazineContainer").hide();
        $("#SetupProvideMaterialContainer").hide();
        $("#SetupUnloadMagazineContainer").hide();
        $("#SetupMainMagazineLoaderContainer").hide();
    }

    function hideResourceContainers() {
        $("#DebugUpperConveyorContainer").hide();
        $("#DebugUpperHatchContainer").hide();
        $("#DebugLiftContainer").hide();
        $("#DebugLeadframePusherContainer").hide();
        $("#DebugLowerConveyorContainer").hide();
        $("#DebugLowerHatchContainer").hide();
        $("#DebugOutfeedContainer").hide();
    }
});

$(function () {
    $("#Simulation").change(function (e) {
        e.preventDefault();
        sendCommand("SetupMagazineLoader", "SimulationChanged");
    });
})