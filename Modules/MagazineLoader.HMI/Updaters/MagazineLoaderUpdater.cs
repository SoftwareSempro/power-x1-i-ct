﻿using log4net;
using Logging;
using MagazineLoader.Application.Resources;
using MagazineLoader.Application.SubProcesses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Platform.HMI;
using Platform.HMI.Resources;

namespace MagazineLoader
{
    public class MagazineLoaderUpdater
    {
        private const string MagazineLoaderStatusUpdate = nameof(MagazineLoaderStatusUpdate);
        private const string MagazineLoaderStateUpdate = nameof(MagazineLoaderStateUpdate);
        private const string MagazineLoaderSimulationChanged = nameof(MagazineLoaderSimulationChanged);
        private const string MagazineLoaderInProductionChanged = nameof(MagazineLoaderInProductionChanged);

        private readonly ILog mLog = LogClient.Get();

        private const string cSimulationID = "MagazineLoaderSimulation";
        private const string cMagazineLoaderStatusID = "MagazineLoaderStatus";
        private readonly Application.MagazineLoaderModule mMagazineLoader = Application.MagazineLoaderModule.Instance;

        private const string cLoadMagazineStatusID = "LoadMagazineStatus";
        private LoadMagazine mLoadMagazine = Application.MagazineLoaderModule.Instance.SubProcesses.LoadMagazine;

        private const string cProvideMaterialStatusID = "ProvideMaterialStatus";
        private ProvideMaterial mProvideMagazine = Application.MagazineLoaderModule.Instance.SubProcesses.ProvideMaterial;

        private const string cUnloadMagazineStatusID = "UnloadMagazineStatus";
        private UnloadMagazine mUnloadMagazine = Application.MagazineLoaderModule.Instance.SubProcesses.UnloadMagazine;

        private const string cUpperHatchStatusID = "UpperHatchStatus";
        private UpperHatch mUpperHatch = Application.MagazineLoaderModule.Instance.Resources.UpperHatch;
        private const string UpperHatchSensorStatusChanged = nameof(UpperHatchSensorStatusChanged);

        private const string cUpperConveyorStatusID = "UpperConveyorStatus";
        private UpperConveyor mUpperConveyor = Application.MagazineLoaderModule.Instance.Resources.UpperConveyor;
        private const string UpperConveyorSenserStatusChanged = nameof(UpperConveyorSenserStatusChanged);

        private const string cLiftStatusID = "LiftStatus";
        private Lift mLift = Application.MagazineLoaderModule.Instance.Resources.Lift;
        private const string LiftActualPositionYChanged = nameof(LiftActualPositionYChanged);
        private const string LiftActualPositionZChanged = nameof(LiftActualPositionZChanged);
        private const string GripperSensorStatusChanged = nameof(GripperSensorStatusChanged);

        private const string cLeadFramePusherStatusID = "LeadFramePusherStatus";
        private LeadFramePusher mLeadFramePusher = Application.MagazineLoaderModule.Instance.Resources.LeadFramePusher;
        private const string LeadframePusherSensorStatusChanged = nameof(LeadframePusherSensorStatusChanged);

        private const string cLowerHatchStatusID = "LowerHatchStatus";
        private LowerHatch mLowerHatch = Application.MagazineLoaderModule.Instance.Resources.LowerHatch;
        private const string LowerHatchSensorStatusChanged = nameof(LowerHatchSensorStatusChanged);

        private const string cLowerConveyorStatusID = "LowerConveyorStatus";
        private LowerConveyor mLowerConveyor = Application.MagazineLoaderModule.Instance.Resources.LowerConveyor;
        private const string LowerConveyorSenserStatusChanged = nameof(LowerConveyorSenserStatusChanged);

        private Outfeed mOutfeed = Application.MagazineLoaderModule.Instance.Resources.Outfeed;
        private const string OutfeedSensorStatusChanged = nameof(OutfeedSensorStatusChanged);

        private readonly IHubContext<SystemHub> mContext;

        public MagazineLoaderUpdater(IHubContext<SystemHub> context)
        {
            mLog.Debug("MagazineLoaderUpdater");

            mContext = context;
            mMagazineLoader.StateChanged += async (sender, e) => await SendStateChanged();
            mMagazineLoader.StatusChanged += async (sender, e) => await SendStatusChanged(cMagazineLoaderStatusID, mMagazineLoader.Status);
            mMagazineLoader.InProductionChanged += async (sender, e) => await mContext.Clients.All.SendAsync(MagazineLoaderInProductionChanged);
            mLoadMagazine.StatusChanged += async (sender, e) => await SendStatusChanged(cLoadMagazineStatusID, mLoadMagazine.Status);
            mProvideMagazine.StatusChanged += async (sender, e) => await SendStatusChanged(cProvideMaterialStatusID, mProvideMagazine.Status);
            mUnloadMagazine.StatusChanged += async (sender, e) => await SendStatusChanged(cUnloadMagazineStatusID, mUnloadMagazine.Status);
            mUpperHatch.StatusChanged += async (sender, e) => await SendStatusChanged(cUpperHatchStatusID, mUpperHatch.Status);
            mUpperHatch.SensorStatusChanged += async () => await SendASyncToClients(UpperHatchSensorStatusChanged);
            mUpperConveyor.StatusChanged += async (sender, e) => await SendStatusChanged(cUpperConveyorStatusID, mUpperConveyor.Status);
            mUpperConveyor.SensorStatusChanged += async () => await SendASyncToClients(UpperConveyorSenserStatusChanged);
            mLift.StatusChanged += async (sender, e) => await SendStatusChanged(cLiftStatusID, mLift.Status);
            mLift.PositionYChanged += async (sender, e) => await SendASyncToClients(LiftActualPositionYChanged);
            mLift.PositionZChanged += async (sender, e) => await SendASyncToClients(LiftActualPositionZChanged);
            mLift.SensorStatusChanged += async () => await SendASyncToClients(GripperSensorStatusChanged);            
            mLeadFramePusher.StatusChanged += async (sender, e) => await SendStatusChanged(cLeadFramePusherStatusID, mLeadFramePusher.Status);
            mLeadFramePusher.SensorStatusChanged += async () => await SendASyncToClients(LeadframePusherSensorStatusChanged);
            mLowerHatch.StatusChanged += async (sender, e) => await SendStatusChanged(cLowerHatchStatusID, mLowerHatch.Status);
            mLowerHatch.SensorStatusChanged += async () => await SendASyncToClients(LowerHatchSensorStatusChanged);
            mLowerConveyor.StatusChanged += async (sender, e) => await SendStatusChanged(cLowerConveyorStatusID, mLowerConveyor.Status);
            mLowerConveyor.SensorStatusChanged += async () => await SendASyncToClients(LowerConveyorSenserStatusChanged);
            mOutfeed.SensorStatusChanged += async () => await SendASyncToClients(OutfeedSensorStatusChanged);
            mMagazineLoader.SimulationChanged += async (sender, e) => await SendMagazineLoaderSimulationChanged(cSimulationID, mMagazineLoader.InSimulation);
        }

        internal JsonResult GetStatusses()
        {
            Dictionary<string, string> statusses = new();

            statusses.Add(cMagazineLoaderStatusID, StatusBar.ConvertToString(mMagazineLoader.Status));

            statusses.Add(cLoadMagazineStatusID, StatusBar.ConvertToString(mLoadMagazine.Status));
            statusses.Add(cProvideMaterialStatusID, StatusBar.ConvertToString(mProvideMagazine.Status));
            statusses.Add(cUnloadMagazineStatusID, StatusBar.ConvertToString(mUnloadMagazine.Status));
            statusses.Add(cUpperHatchStatusID, StatusBar.ConvertToString(mUpperHatch.Status));
            statusses.Add(cUpperConveyorStatusID, StatusBar.ConvertToString(mUpperConveyor.Status));
            statusses.Add(cLiftStatusID, StatusBar.ConvertToString(mLift.Status));
            statusses.Add(cLeadFramePusherStatusID, StatusBar.ConvertToString(mLeadFramePusher.Status));
            statusses.Add(cLowerHatchStatusID, StatusBar.ConvertToString(mLowerHatch.Status));
            statusses.Add(cLowerConveyorStatusID, StatusBar.ConvertToString(mLowerConveyor.Status));

            return new JsonResult(statusses);
        }

        private async Task SendStatusChanged(string itemToUpdate, int value)
        {
            //Convert value to statusbar item text
            var statusNameOfIntValue = StatusBar.ConvertToString(value);

            mLog.Debug($"SendStatusChanged: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(MagazineLoaderStatusUpdate, itemToUpdate, statusNameOfIntValue);
        }

        private async Task SendStateChanged()
        {
            await mContext.Clients.All.SendAsync(MagazineLoaderStateUpdate);
        }

        private async Task SendMagazineLoaderSimulationChanged(string itemToUpdate, bool value)
        {
            mLog.Debug($"SendAsyncToClients: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(MagazineLoaderSimulationChanged, itemToUpdate, value);
        }

        private async Task SendASyncToClients(string item)
        {
            await mContext.Clients.All.SendAsync(item);
        }
    }
}

