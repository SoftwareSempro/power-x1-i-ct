﻿using MagazineLoader.Application.SubProcesses;
using RecipeManager;
using System;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class UnloadMagazineSettingsModel
    {
        private MagazineLoader.Application.SubProcesses.UnloadMagazine mUnloadMagazine = MagazineLoader.Application.MagazineLoaderModule.Instance.SubProcesses.UnloadMagazine;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public double LowerConveyorZ { get; set; }
        public double StartPlaceOffsetZ { get; set; }
        public double PlaceY { get; set; }
        public double LiftSafePositionY { get; set; }   
        public int UnloadVelocityY { get; set; }
        public int UnloadVelocityZ { get; set; }

        public UnloadMagazineSettingsModel()
        {
            UnloadMagazine_SettingsChanged(this, mUnloadMagazine.Settings);
            mUnloadMagazine.SettingsChanged += UnloadMagazine_SettingsChanged;
        }

        private void UnloadMagazine_SettingsChanged(Object sender, UnloadMagazineDataModel e)
        {
            LowerConveyorZ = e.LowerConveyorZ;
            StartPlaceOffsetZ = e.StartPlaceOffsetZ;
            PlaceY = e.PlaceY;
            LiftSafePositionY = e.MagazinePositionLiftSafePosition;
            UnloadVelocityY = e.VelocityY;
            UnloadVelocityZ = e.VelocityZ;
        }

        public void Save()
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe() as MachineRecipe;
            machineRecipe.MagazineLoader.UnloadMagazine.LowerConveyorZPosition = LowerConveyorZ;
            machineRecipe.MagazineLoader.UnloadMagazine.StartPlaceOffsetZPosition = StartPlaceOffsetZ;
            machineRecipe.MagazineLoader.UnloadMagazine.PlaceYPosition = PlaceY;
            machineRecipe.MagazineLoader.UnloadMagazine.MagazineLiftSafePositionY = LiftSafePositionY;
            machineRecipe.MagazineLoader.UnloadMagazine.VelocityY = UnloadVelocityY;
            machineRecipe.MagazineLoader.UnloadMagazine.VelocityZ = UnloadVelocityZ;
            mRecipeManager.SaveRecipe(machineRecipe);            
        }

        public void Download()
        {
            var dataModel = new UnloadMagazineDataModel();
            dataModel.LowerConveyorZ = LowerConveyorZ;
            dataModel.StartPlaceOffsetZ = StartPlaceOffsetZ;
            dataModel.PlaceY = PlaceY;
            dataModel.MagazinePositionLiftSafePosition = LiftSafePositionY;
            dataModel.VelocityY = Convert.ToInt16(UnloadVelocityY);
            dataModel.VelocityZ = Convert.ToInt16(UnloadVelocityZ);

            mUnloadMagazine.UpdateSettings(dataModel);

        }
    }
}
