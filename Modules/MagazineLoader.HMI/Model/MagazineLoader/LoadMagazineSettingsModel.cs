﻿using MagazineLoader.Application.SubProcesses;
using RecipeManager;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class LoadMagazineSettingsModel
    {
        private MagazineLoader.Application.SubProcesses.LoadMagazine mLoadMagazine = MagazineLoader.Application.MagazineLoaderModule.Instance.SubProcesses.LoadMagazine;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public double UpperConveyorZ { get; set; }
        public double StartPickOffsetZ { get; set; }
        public double EndPickOffsetZ { get; set; }
        public double PickY { get; set; }
        public double MagazinePositionLiftSafePositionY { get; set; }
        public int SlotCount { get; set; }
        public int LoadVelocityY { get; set; }
        public int LoadVelocityZ { get; set; }

        public LoadMagazineSettingsModel()
        {
            LoadMagazine_SettingsChanged(this, mLoadMagazine.Settings);
            mLoadMagazine.SettingsChanged += LoadMagazine_SettingsChanged;
        }

        private void LoadMagazine_SettingsChanged(object sender, LoadMagazineDataModel e)
        {
            UpperConveyorZ = e.ConveyorUpperZPosition;
            SlotCount = e.SlotCount;
            StartPickOffsetZ = e.StartPickOffsetZ;
            EndPickOffsetZ = e.EndPickOffsetZ;
            PickY = e.PickY;
            MagazinePositionLiftSafePositionY = e.MagazinePositionLiftSafePositionY;
            LoadVelocityY = e.VelocityY;
            LoadVelocityZ = e.VelocityZ;
        }

        public void Save(string author)
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe() as MachineRecipe;
            machineRecipe.MagazineLoader.LoadMagazine.ConveyorUpperZPosition = UpperConveyorZ;
            machineRecipe.MagazineLoader.LoadMagazine.StartPickOffsetZ = StartPickOffsetZ;
            machineRecipe.MagazineLoader.LoadMagazine.EndPickOffsetZ = EndPickOffsetZ;
            machineRecipe.MagazineLoader.LoadMagazine.MagazineLiftSafePositionY = MagazinePositionLiftSafePositionY;
            machineRecipe.MagazineLoader.LoadMagazine.PickY = PickY;
            machineRecipe.MagazineLoader.LoadMagazine.VelocityY = LoadVelocityY;
            machineRecipe.MagazineLoader.LoadMagazine.VelocityZ = LoadVelocityZ;
            mRecipeManager.SaveRecipe(machineRecipe);

            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.Magazine.SlotCount = SlotCount;
            mRecipeManager.SaveRecipe(productRecipe, author);
        }

        internal void Dispose()
        {
            mLoadMagazine.SettingsChanged -= LoadMagazine_SettingsChanged;
        }

        public void Download()
        {
            var dataModel = new LoadMagazineDataModel();
            dataModel.ConveyorUpperZPosition = UpperConveyorZ;
            dataModel.StartPickOffsetZ = StartPickOffsetZ;
            dataModel.EndPickOffsetZ = EndPickOffsetZ;
            dataModel.PickY = PickY;
            dataModel.MagazinePositionLiftSafePositionY = MagazinePositionLiftSafePositionY;
            dataModel.SlotCount = Convert.ToInt16(SlotCount);
            dataModel.VelocityY = Convert.ToInt16(LoadVelocityY);
            dataModel.VelocityZ = Convert.ToInt16(LoadVelocityZ);

            mLoadMagazine.UpdateSettings(dataModel);
        }
    }
}
