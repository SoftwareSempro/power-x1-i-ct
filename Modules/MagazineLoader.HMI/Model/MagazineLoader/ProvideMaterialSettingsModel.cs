﻿using MagazineLoader.Application.SubProcesses;
using RecipeManager;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class ProvideMaterialSettingsModel
    {
        private MagazineLoader.Application.SubProcesses.ProvideMaterial mProvideMagazine = MagazineLoader.Application.MagazineLoaderModule.Instance.SubProcesses.ProvideMaterial;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double MagazineSlotOffset { get; set; }
        public double MagazineBottomSlotOffset { get; set; }
        public int DeviceCountPerLeadFrame { get; set; }
        public double MagazineLiftStartPositionZ { get; set; }
        public double MagazineLiftStartPositionY { get; set; }
        public double MagazineWidth { get; set; }
        public int ProvideVelocityY { get; set; }
        public int ProvideVelocityZ { get; set; }

        public ProvideMaterialSettingsModel()
        {
            ProvideMagazine_SettingsChanged(this, mProvideMagazine.Settings);
            mProvideMagazine.SettingsChanged += ProvideMagazine_SettingsChanged;
        }

        private void ProvideMagazine_SettingsChanged(object sender, ProvideMagazineDataModel e)
        {
            MagazineSlotOffset = e.MagazineSlotOffset;
            MagazineBottomSlotOffset = e.MagazineBottomSlotOffset;
            DeviceCountPerLeadFrame = e.DeviceCountPerLeadFrame;
            MagazineLiftStartPositionZ = e.MagazineLiftStartPositionZ;
            MagazineLiftStartPositionY = e.MagazineLiftStartPositionY;
            MagazineWidth = e.MagazineWidth;
            ProvideVelocityY = e.VelocityY;
            ProvideVelocityZ = e.VelocityZ;
        }

        public void Save(string author)
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe() as MachineRecipe;
            machineRecipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionY = MagazineLiftStartPositionY;
            machineRecipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionZ = MagazineLiftStartPositionZ;
            machineRecipe.MagazineLoader.ProvideMaterial.VelocityY = ProvideVelocityY;
            machineRecipe.MagazineLoader.ProvideMaterial.VelocityZ = ProvideVelocityZ;
            mRecipeManager.SaveRecipe(machineRecipe);

            var productRecipe = mRecipeManager.GetCurrentRecipe() as ProductRecipe;
            productRecipe.Magazine.SlotOffset = MagazineSlotOffset;
            productRecipe.Magazine.BottomSlotOffset = MagazineBottomSlotOffset;
            productRecipe.LeadFrame.DeviceQuantity = DeviceCountPerLeadFrame;
            productRecipe.Magazine.LeadframesPerSlot = 1;
            productRecipe.Magazine.MagazineWidth = MagazineWidth;
            mRecipeManager.SaveRecipe(productRecipe, author);
        }

        public void Download()
        {
            var dataModel = new ProvideMagazineDataModel();
            dataModel.MagazineSlotOffset = MagazineSlotOffset;
            dataModel.MagazineBottomSlotOffset = MagazineBottomSlotOffset;
            dataModel.DeviceCountPerLeadFrame = Convert.ToInt16(DeviceCountPerLeadFrame);
            dataModel.MagazineLiftStartPositionZ = MagazineLiftStartPositionZ;
            dataModel.MagazineLiftStartPositionY = MagazineLiftStartPositionY;
            dataModel.MagazineWidth = MagazineWidth;
            dataModel.VelocityY = Convert.ToInt16(ProvideVelocityY);
            dataModel.VelocityZ = Convert.ToInt16(ProvideVelocityZ);

            mProvideMagazine.UpdateSettings(dataModel);
        }
    }
}
