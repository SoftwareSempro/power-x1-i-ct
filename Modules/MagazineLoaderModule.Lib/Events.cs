﻿using System.Collections.Generic;
using EventHandler.Lib;

namespace MagazineLoader.Lib
{
    public class MagazineLoaderEvents
    {
        private const string cModuleName = "MagazineLoader";
        private const string cLoadMagazineProcessName = "Load magazine";
        private const string cProvideMaterialProcessName = "Provide material";
        private const string cUnloadMagazineProcessName = "Unload magazine";
        private const string cSafetyName = "Safety";
        private const string cLiftResourceName = "Magazine lift";
        private const string cLowerHatchResourceName = "Lower hatch";

        //Main
        private const string cMagazineLoaderMainPLCEvents = "instMagazineLoaderMainEvents";
        private const string cMagazineLoaderNotallCoversLockedInstance = "instNotAllCoversLocked";

        //Load
        private const string cLoadMagazinePLCEvents = "instLoadMagazineEvents";
        private const string cUpperConveyorAlmostEmptyInstance = "instUpperConveyorAlmostEmpty";
        private const string cUpperConveyorEmptyInstance = "instUpperConveyorEmpty";
        private const string cMagazineDidNotReachPickupPositionInstance = "instMagazineDidNotReachPickupPosition";       
        private const string cFailedToClampMagazineInGripperInstance = "instFailedToClampMagazineInGripper";
        private const string cMagazineLiftDidNotReachSafePositionYInstance = "instMagazineLiftDidNotReachSafePositionY";
        private const string cMagazineMissingInLiftInstance = "instMagazineMissingInLift";
        private const string cMagazineUnknownInLiftInstance = "instMagazineUnknownInLift";
        private const string cMagazineDoorIsNotLockedAssistInstance = "instMagazineDoorIsNotLocked";
        
        //Provide
        private const string cProvideMaterialPLCEvents = "instProvideMaterialEvents";
        private const string cMagazineLiftDidNotReachProvidePositionInstance = "instMagazineLiftDidNotReachProvidePositionZ";        
        private const string cMagazineCanNotMoveBecauseMaterialInTransferInstanceProvide = "instMagazineCanNotMoveBecauseMaterialInTransfer";
        private const string cFailedToProvideMaterialInstance = "instFailedToProvideMaterial";
        private const string cMaterialDidNotReachOutfeedAssistInstance = "instMaterialDidNotReachOutfeedAssist";

        //Unload       
        private const string cUnloadMagazinePLCEvents = "instUnloadMagazineEvents";
        private const string cMagazineCanNotMoveBecauseMaterialInTransferUnloadInstance = "instMagazineCanNotMoveBecauseMaterialInTransfer";
        private const string cMagazineLiftDidNotReachUnloadPositionZInstance = "instMagazineLiftDidNotReachUnloadPositionZ";
        private const string cLowerConveyorAlmostFullInstance = "instLowerConveyorAlmostFull";
        private const string cLowerConveyorFullInstance = "instLowerConveyorFull";
        private const string cLowerConveyorFullAssistInstance = "instLowerConveyorFullAssist";
        private const string cFailedToReachUnloadPositionYInstance = "instFailedToReachUnloadPositionY";
        private const string cFailedToUnclampMagazineInGripperInstance = "instFailedToUnclampMagazineInGripper";
        private const string cFailedToReachClearUnloadPositionZInstance = "instFailedToReachClearUnloadPositionZ";
        private const string cMagazineLiftDidNotReachUnloadSafePositionYInstance = "instMagazineLiftDidNotReachSafePositionY";
        private const string cMagazineLoaderUnloadDoorNotLockedAssistInstance = "instMagazineLoaderMainDoorNotLockedAssist"; 

        //Safety
        private const string cSafetyPLCEvents = "instMagazineLoaderSafetyEvents";
        private const string cMagazineLoaderMainDoorUnlockedInstance = "instMagazineMainLoaderDoorUnlocked";
        private const string cMagazineLoaderMainDoorLockErrorInstance = "instMagazineMainLoaderDoorLockError";
        private const string cMagazineLoaderProvideDoorOpenErrorInstance = "instMagazineLoaderProvideDoorOpen";
        private const string cMagazineLoaderEmergencyStopActiveInstance = "instMagazineLoaderEmergencyStopActive";
        

        #region Resources
        // Upper hatch
        private const string cUpperHatchPLCEvents = "instUpperHatchEvents";
        private const string cUpperHatchFailedToOpenInstance = "instUpperHatchFailedToOpen";
        private const string cUpperHatchFailedToCloseInstance = "instUpperHatchFailedToClose";

        // Loader lift
        private const string cMagazineLoaderLiftPLCEvents = "instMagazineLoaderLiftEvents";
        private const string cGripperFailedToReachCloseSensorInstance = "instGripperFailedToReachCloseSensor";
        private const string cGripperFailedToReachOpenSensorInstance = "instGripperFailedToReachOpenSensor";
        private const string cLoaderLiftNotInPositionInstance = "instLoaderLiftNotInPosition";
        private const string cLiftZDriveErrorInstance = "instLiftZDriveError";
        private const string cLiftZCommunicationErrorInstance = "instLiftZCommunicationError";
        private const string cLiftZPositionErrorInstance = "instLiftZPositionError";

        //LeadframePusher
        private const string cLeadFramePusherPLCEvents = "instMagazineLeadFramePusherEvents";
        private const string cLeadFramePusherFailedToReachProvidePositionInstance = "instLeadframePusherFailedToReachProvidePosition";
        private const string cLeadFramePusherFailedToReachRetractPositionInstance = "instLeadframePusherFailedToReachRetractPosition";

        // Lower hatch
        private const string cLowerHatchPLCEvents = "instLowerHatchEvents";
        private const string cLowerHatchFailedToReachClosePositionInstance = "instLowerHatchFailedToReachClosePosition";
        private const string cLowerHatchFailedToReachOpenPositionInstance = "instLowerHatchFailedToReachOpenPosition";
        #endregion

        public static Event UpperConveyorAlmostEmpty = new(100, "Upper conveyor almost empty", EventType.Info, cLoadMagazineProcessName, cModuleName, cLoadMagazinePLCEvents, cUpperConveyorAlmostEmptyInstance, $"Events/{cModuleName}/UpperConveyorAlmostEmpty");
        public static Event UpperConveyorEmpty = new(101, "Upper conveyor empty", EventType.Assist, cLoadMagazineProcessName, cModuleName, cLoadMagazinePLCEvents, cUpperConveyorEmptyInstance, $"Events/{cModuleName}/UpperConveyorEmptyAssist");
        public static Event MagazineDidNotReachPickupPosition = new(103, "Magazine did not reach pickup position", EventType.Error, cLoadMagazineProcessName, cModuleName, cLoadMagazinePLCEvents, cMagazineDidNotReachPickupPositionInstance, $"Events/{cModuleName}/MagazineDidNotReachPickUpPosition.");        
        public static Event FailedToClampMagazineInGripper = new(106, "Failed to clamp magazine in gripper", EventType.Error, cLoadMagazineProcessName, cModuleName, cLoadMagazinePLCEvents, cFailedToClampMagazineInGripperInstance, $"Events/{cModuleName}/FailedToClampMagazineInGripper");
        public static Event MagazineLiftDidNotReachSafePositionY = new(107, "Magazine lift did not reach safe position Y", EventType.Error, cLoadMagazineProcessName, cModuleName, cLoadMagazinePLCEvents, cMagazineLiftDidNotReachSafePositionYInstance, $"Events/{cModuleName}/MagazineLiftDidNotReachSafePositionY");

        public static Event MagazineMissingInLiftAssist = new(108, "Magazine loader lift is missing a magazine", EventType.Assist, cLiftResourceName, cModuleName, cLoadMagazinePLCEvents, cMagazineMissingInLiftInstance, $"Events/{cModuleName}/{nameof(MagazineMissingInLiftAssist)}");
        public static Event MagazineUnknownInLiftAssist = new(109, "Magazine loader lift has an unknown magazine", EventType.Assist, cLiftResourceName, cModuleName, cLoadMagazinePLCEvents, cMagazineUnknownInLiftInstance, $"Events/{cModuleName}/{nameof(MagazineUnknownInLiftAssist)}");
        public static Event MagazineLoaderLoadMainDoorNotLockedAssist = new(110, "New magazine can not be loaded magazine door is not locked. ", EventType.Assist, cLiftResourceName, cModuleName, cLoadMagazinePLCEvents, cMagazineDoorIsNotLockedAssistInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderLoadMainDoorNotLockedAssist)}");

        public static Event MagazineLiftDidNotReachProvidePosition = new(111, "Magazine lift did not reach provide position Z", EventType.Error, cProvideMaterialProcessName, cModuleName, cProvideMaterialPLCEvents, cMagazineLiftDidNotReachProvidePositionInstance, $"Events/{cModuleName}/MagazineLiftDidNotReachProvidePosition");
        public static Event MagazineCanNotMoveBecauseMaterialInTransferProvide = new(112, "Magazine can not move because material in transfer", EventType.Error, cProvideMaterialProcessName, cModuleName, cProvideMaterialPLCEvents, cMagazineCanNotMoveBecauseMaterialInTransferInstanceProvide, $"Events/{cModuleName}/MagazineCanNotMoveBecauseMaterialInTransfer");
        public static Event FailedToProvideMaterial = new(113, "Magazine lift failed to provide material", EventType.Error, cProvideMaterialProcessName, cModuleName, cProvideMaterialPLCEvents, cFailedToProvideMaterialInstance, $"Events/{cModuleName}/{nameof(FailedToProvideMaterial)}");
        public static Event MaterialDidNotReachOutfeed = new(114, "Material did not reach outfeed", EventType.Assist, cProvideMaterialProcessName, cModuleName, cProvideMaterialPLCEvents, cMaterialDidNotReachOutfeedAssistInstance, $"Events/{cModuleName}/{nameof(MaterialDidNotReachOutfeed)}");

        public static Event MagazineCanNotMoveBecauseMaterialInTransferUnload = new(115, "Magazine can not move because material in transfer", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cMagazineCanNotMoveBecauseMaterialInTransferUnloadInstance, $"Events/{cModuleName}/MagazineCanNotMoveBecauseMaterialInTransfer");
        public static Event MagazineLiftDidNotReachUnloadPositionZ = new(116, "Magazine lift did not reach unload position Z", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cMagazineLiftDidNotReachUnloadPositionZInstance, $"Events/{cModuleName}/MagazineLiftDidNotReachUnloadPositionZ");
        public static Event LowerConveyorAlmostFull = new(117, "Lower conveyor almost full", EventType.Warning, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cLowerConveyorAlmostFullInstance, $"Events/{cModuleName}/MagazineCanNotMoveBecauseMaterialInTransfer");
        public static Event LowerConveyorFull = new(118, "Lower conveyor full", EventType.Warning, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cLowerConveyorFullInstance, $"Events/{cModuleName}/LowerConveyorFull");
        public static Event LowerConveyorFullAssist = new(119, "Lower conveyor full", EventType.Assist, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cLowerConveyorFullAssistInstance, $"Events/{cModuleName}/{nameof(LowerConveyorFullAssist)}");
        public static Event FailedToReachUnloadPositionY = new(120, "Failed to reach unload position Y", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cFailedToReachUnloadPositionYInstance, $"Events/{cModuleName}/FailedToReachUnloadPositionY");
        public static Event FailedToUnclampMagazineInGripper = new(121, "Failed to unclamp magazine in gripper", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cFailedToUnclampMagazineInGripperInstance, $"Events/{cModuleName}/FailedToUnclampMagazineInGripper");
        public static Event FailedToReachClearUnloadPositionZ = new(122, "Failed to reach clear unload position Z", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cFailedToReachClearUnloadPositionZInstance, $"Events/{cModuleName}/FailedToReachClearUnloadPositionZ");
        public static Event MagazineLiftDidNotReachUnloadSafePositionY = new(123, "Magazine lift did not reach safe position Y", EventType.Error, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cMagazineLiftDidNotReachUnloadSafePositionYInstance, $"Events/{cModuleName}/MagazineLiftDidNotReachUnloadSafePositionY");

        public static Event MagazineLoaderUnloadMainDoorNotLockedAssist = new(125, "Magazine loader main door not locked", EventType.Assist, cUnloadMagazineProcessName, cModuleName, cUnloadMagazinePLCEvents, cMagazineLoaderUnloadDoorNotLockedAssistInstance, $"Events/{cModuleName}/MagazineLoaderUnloadMainDoorNotLockedAssist");     
        public static Event MagazineLoaderMainDoorUnlocked = new(126, "Magazine loader main door unlocked", EventType.Info, cSafetyName, cModuleName, cSafetyPLCEvents, cMagazineLoaderMainDoorUnlockedInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderMainDoorUnlocked)}"); 
        public static Event MagazineLoaderMainDoorLockError = new(127, "Magazine main loader door lock error", EventType.Error, cSafetyName, cModuleName, cSafetyPLCEvents, cMagazineLoaderMainDoorLockErrorInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderMainDoorLockError)}");
        public static Event MagazineLoaderEmergencyStopActive = new(128, "Magazine loader door emergency stop is active", EventType.Error, cSafetyName, cModuleName, cSafetyPLCEvents, cMagazineLoaderEmergencyStopActiveInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderEmergencyStopActive)}");
        public static Event MagazineLoaderProvideDoorOpenError = new(129, "Magazine loader provide door open error", EventType.Error, cSafetyName, cModuleName, cSafetyPLCEvents, cMagazineLoaderProvideDoorOpenErrorInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderProvideDoorOpenError)}");
        
        public static Event UpperHatchFailedToOpen = new(130, "Upper hatch failed to open", EventType.Error, cLoadMagazineProcessName, cModuleName, cUpperHatchPLCEvents, cUpperHatchFailedToOpenInstance, $"Events/{cModuleName}/UpperHatchFailedToOpen.");
        public static Event UpperHatchFailedToClose = new(131, "Upper hatch failed to close", EventType.Error, cLoadMagazineProcessName, cModuleName, cUpperHatchPLCEvents, cUpperHatchFailedToCloseInstance, $"Events/{cModuleName}/UpperHatchFailedToClose.");

        public static Event GripperFailedToReachCloseSensor = new(132, "Lift gripper failed to reach close sensor", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cGripperFailedToReachCloseSensorInstance, $"Events/{cModuleName}/{nameof(GripperFailedToReachCloseSensor)}");
        public static Event GripperFailedToReachOpenSensor = new(133, "Lift gripper failed to reach open sensor", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cGripperFailedToReachOpenSensorInstance, $"Events/{cModuleName}/{nameof(GripperFailedToReachOpenSensor)}");
        public static Event LoaderLiftNotInPosition = new(134, "Magazine lift did not reach position in time", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cLoaderLiftNotInPositionInstance, $"Events/{cModuleName}/{nameof(LoaderLiftNotInPosition)}");
        public static Event LiftZDriveError = new(135, "Lift drive Z is in error", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cLiftZDriveErrorInstance, $"Events/{cModuleName}/{nameof(LiftZDriveError)}");
        public static Event LiftZCommunicationError = new(136, "Lift drive Z has a communication error", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cLiftZCommunicationErrorInstance, $"Events/{cModuleName}/{nameof(LiftZCommunicationError)}");
        public static Event LiftZPositionError = new(137, "Lift drive Z has a position error", EventType.Error, cLiftResourceName, cModuleName, cMagazineLoaderLiftPLCEvents, cLiftZPositionErrorInstance, $"Events/{cModuleName}/{nameof(LiftZPositionError)}");

        public static Event LeadFramePusherFailedToReachProvidePosition = new(140, "Leadframe pusher failed to reach provide position", EventType.Error, cProvideMaterialProcessName, cModuleName, cLeadFramePusherPLCEvents, cLeadFramePusherFailedToReachProvidePositionInstance, $"Events/{cModuleName}/LeadFramePusherFailedToReachProvidePosition");
        public static Event LeadFramePusherFailedToReachRetractPosition = new(141, "Leadframe pusher failed to reach retract position", EventType.Error, cProvideMaterialProcessName, cModuleName, cLeadFramePusherPLCEvents, cLeadFramePusherFailedToReachRetractPositionInstance, $"Events/{cModuleName}/LeadFramePusherFailedToReachRetractPosition");

        public static Event LowerHatchFailedToReachOpenPosition = new(142, "Lower hatch failed to reach open position", EventType.Error, cLowerHatchResourceName, cModuleName, cLowerHatchPLCEvents, cLowerHatchFailedToReachOpenPositionInstance, $"Events/{cModuleName}/{nameof(LowerHatchFailedToReachOpenPosition)}");
        public static Event LowerHatchFailedToReachClosePosition = new(143, "Lower hatch failed to reach close position", EventType.Error, cLowerHatchResourceName, cModuleName, cLowerHatchPLCEvents, cLowerHatchFailedToReachClosePositionInstance, $"Events/{cModuleName}/{nameof(LowerHatchFailedToReachClosePosition)}");

        public static Event MagazineLoaderNotAllCoversLocked = new(145, "Not all covers locked during initializing magazine loader module", EventType.Warning, cSafetyName, cModuleName, cMagazineLoaderMainPLCEvents, cMagazineLoaderNotallCoversLockedInstance, $"Events/{cModuleName}/{nameof(MagazineLoaderNotAllCoversLocked)}");

        public static IEnumerable<Event> Events = new List<Event>
        {
            UpperConveyorAlmostEmpty,
            UpperConveyorEmpty,
            MagazineDidNotReachPickupPosition,
            UpperHatchFailedToOpen,
            UpperHatchFailedToClose,
            FailedToClampMagazineInGripper,
            MagazineLiftDidNotReachSafePositionY,
            MagazineLiftDidNotReachProvidePosition,
            LeadFramePusherFailedToReachProvidePosition,
            LeadFramePusherFailedToReachRetractPosition,
            MagazineCanNotMoveBecauseMaterialInTransferProvide,
            FailedToProvideMaterial,
            MaterialDidNotReachOutfeed,
            MagazineLiftDidNotReachUnloadPositionZ,
            MagazineLoaderUnloadMainDoorNotLockedAssist,
            LowerConveyorAlmostFull,
            LowerConveyorFull,
            LowerConveyorFullAssist,
            FailedToReachUnloadPositionY,
            FailedToUnclampMagazineInGripper,
            FailedToReachClearUnloadPositionZ,
            MagazineLiftDidNotReachUnloadSafePositionY,
            MagazineLoaderMainDoorUnlocked,
            MagazineLoaderMainDoorLockError,
            MagazineLoaderProvideDoorOpenError,
            MagazineLoaderEmergencyStopActive,
            GripperFailedToReachCloseSensor,
            GripperFailedToReachOpenSensor,
            LoaderLiftNotInPosition,
            LiftZDriveError,
            LiftZCommunicationError,
            MagazineUnknownInLiftAssist,
            MagazineMissingInLiftAssist,
            LowerHatchFailedToReachOpenPosition,
            LowerHatchFailedToReachClosePosition,
            MagazineLoaderLoadMainDoorNotLockedAssist,
            LoaderLiftNotInPosition,
            MagazineLoaderNotAllCoversLocked
        };
    }
}
