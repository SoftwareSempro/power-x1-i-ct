﻿using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.LeadFramePusher;
using MagazineLoader.PLCInterfaces.Lift;
using MagazineLoader.PLCInterfaces.LoadMagazine;
using MagazineLoader.PLCInterfaces.LowerConveyor;
using MagazineLoader.PLCInterfaces.LowerHatch;
using MagazineLoader.PLCInterfaces.Main;
using MagazineLoader.PLCInterfaces.Outfeed;
using MagazineLoader.PLCInterfaces.ProvideMaterial;
using MagazineLoader.PLCInterfaces.UnloadMagazine;
using MagazineLoader.PLCInterfaces.UpperConveyor;
using MagazineLoader.PLCInterfaces.UpperHatch;

namespace MagazineLoader.PLCInterfaces
{
    public class MagazineLoader
    {
        [Name("magazineLoaderModuleInterface")]
        public MagazineLoaderModuleInterface MagazineLoaderModuleInterface { get; set; }

        [Name("magazineLoaderLoadMagazineInterface")]
        public MagazineLoaderLoadMagazineInterface MagazineLoaderLoadMagazineInterface { get; set; }

        [Name("magazineLoaderProvideMaterialInterface")]
        public MagazineLoaderProvideMaterialInterface MagazineLoaderProvideMaterialInterface { get; set; }

        [Name("magazineLoaderUnloadMagazineInterface")]
        public MagazineLoaderUnloadMagazineInterface MagazineLoaderUnloadMagazineInterface { get; set; }

        [Name("magazineLoaderUpperConveyorInterface")]
        public MagazineLoaderUpperConveyorInterface MagazineLoaderUpperConveyorInterface { get; set; }

        [Name("magazineLoaderUpperHatchInterface")]
        public MagazineLoaderUpperHatchInterface MagazineLoaderUpperHatchInterface { get; set; }

        [Name("magazineLoaderLiftInterface")]
        public MagazineLoaderLiftInterface MagazineLoaderLiftInterface { get; set; }

        [Name("magazineLoaderLeadFramePusherInterface")]
        public MagazineLoaderLeadFramePusherInterface MagazineLoaderLeadFramePusherInterface { get; set; }

        [Name("magazineLoaderLowerConveyorInterface")]
        public MagazineLoaderLowerConveyorInterface MagazineLoaderLowerConveyorInterface { get; set; }

        [Name("magazineLoaderLowerHatchInterface")]
        public MagazineLoaderLowerHatchInterface MagazineLoaderLowerHatchInterface { get; set; }

        [Name("magazineLoaderOutfeedInterface")]
        public MagazineLoaderOutfeedInterface MagazineLoaderOutfeedInterface { get; set; }

        [Name("magazineLoaderMaterialTrackingInterface")]
        public MagazineLoaderMaterialTracking MaterialTracking { get; set; }
    }
}
