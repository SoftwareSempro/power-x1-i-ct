﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerConveyor
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("magazinePresentSensor")]
        public bool MagazinePresentSensor { get; set; }

        [Name("conveyorFull")]
        public bool ConveyorFull { get; set; }
    }
}
