﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerHatch
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("isOpen")]
        public bool IsOpen { get; set; }

        [Name("isClosed")]
        public bool IsClosed { get; set; }
    }
}
