﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperConveyor
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("magazinePresentSensor")]
        public bool MagazinePresentSensor { get; set; }

        [Name("conveyorAlmostEmpty")]
        public bool conveyorAlmostEmpty { get; set; }

    }
}
