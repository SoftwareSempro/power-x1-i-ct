﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperConveyor
{
    public class ToPlc
    {
        [Name("startCommand")]
        public bool StartCommand { get; set; }

        [Name("stopCommand")]
        public bool StopCommand { get; set; }
    }
}
