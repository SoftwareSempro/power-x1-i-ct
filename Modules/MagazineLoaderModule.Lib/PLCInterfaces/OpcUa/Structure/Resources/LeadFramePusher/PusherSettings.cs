﻿
using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LeadFramePusher
{
   public class PusherSettings
    {
        [Name("push")]
        public bool Push { get; set; }

        [Name("retreive")]
        public bool Retreive { get; set; }

    }
}
