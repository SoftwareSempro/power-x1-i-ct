﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LeadFramePusher
{
    public class MagazineLoaderLeadFramePusherInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
