﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LeadFramePusher
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("primairPusherIn")]
        public bool PrimairPusherIn { get; set; }

        [Name("primairPusherMiddle")]
        public bool PrimairPusherMiddle { get; set; }

        [Name("primairPusherOut")]
        public bool PrimairPusherOut { get; set; }

        [Name("secondairPusherIn")]
        public bool SecondairPusherIn { get; set; }

        [Name("secondairPusherOut")]
        public bool SecondairPusherOut { get; set; }
    }
}
