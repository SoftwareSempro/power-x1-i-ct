﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Lift
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }

        [Name("positionY")]
        public double PositionY { get; set; }

        [Name("positionZ")]
        public double PositionZ { get; set; }

        [Name("gripperOpen")]
        public double GripperOpen { get; set; }

        [Name("gripperClosed")]
        public double GripperClosed { get; set; }
    }
}
