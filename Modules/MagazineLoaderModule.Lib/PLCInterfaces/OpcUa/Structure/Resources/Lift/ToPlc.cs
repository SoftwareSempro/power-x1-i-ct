﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Lift
{
    public class ToPlc
    {
        [Name("goToCommand")]
        public bool GoToCommand { get; set; }

        [Name("setPointY")]
        public double SetPointY { get; set; }

        [Name("setPointZ")]
        public double SetPointZ { get; set; }
        [Name("velocity")]
        public short Velocity { get; set; }

        [Name("gripperOpenCommand")]
        public bool GripperOpenCommand { get; set; }   
        
        [Name("gripperCloseCommand")]
        public bool GripperCloseCommand { get; set; }
    }
}
