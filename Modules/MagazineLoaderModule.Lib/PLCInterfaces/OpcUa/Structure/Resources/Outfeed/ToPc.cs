﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Outfeed
{
   public class ToPc
    {
        [Name("productAtTransfer")]
        public bool ProductAtTransfer { get; set; }
    }
}
