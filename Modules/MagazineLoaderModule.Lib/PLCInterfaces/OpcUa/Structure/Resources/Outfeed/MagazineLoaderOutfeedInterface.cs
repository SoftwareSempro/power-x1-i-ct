﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Outfeed
{
   public class MagazineLoaderOutfeedInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
