﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.ProvideMaterial
{
    public class ToPlc
    {
        [Name("settings")]
        public Provide Provide { get; set; }

        [Name("removeMaterialCommand")]
        public bool RemoveMaterialCommand { get; set; }

        [Name("retryCommand")]
        public bool RetryCommand { get; set; }
    }
}