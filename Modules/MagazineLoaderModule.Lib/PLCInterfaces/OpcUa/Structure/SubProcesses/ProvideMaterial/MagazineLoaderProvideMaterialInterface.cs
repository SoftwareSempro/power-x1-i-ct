﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.ProvideMaterial
{
    public class MagazineLoaderProvideMaterialInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}