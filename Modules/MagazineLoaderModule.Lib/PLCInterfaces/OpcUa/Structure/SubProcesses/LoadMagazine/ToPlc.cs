﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LoadMagazine
{
    public class ToPlc
    {
        [Name("settings")]
        public Load Load { get; set; }

        [Name("retryCommand")]
        public bool RetryCommand { get; set; }

        [Name("continueCommand")]
        public bool ContinueCommand { get; set; }
    }
}