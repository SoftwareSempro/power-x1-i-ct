﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UnloadMagazine
{
    public class MagazineLoaderUnloadMagazineInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
