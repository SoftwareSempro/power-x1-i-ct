﻿
using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UnloadMagazine
{
    public class ToPlc
    {
        [Name("settings")]
        public Unload Unload { get; set; }
        [Name("continueCommand")]
        public bool ContinueCommand { get; set; }
        [Name("retryCommand")]
        public bool RetryCommand { get; set; }

    }
}