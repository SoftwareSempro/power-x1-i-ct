﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UnloadMagazine
{
    public class Unload
    {
        [Name("lowerConveyorZ")]
        public double LowerConveyorZ { get; set; }

        [Name("startPlaceOffsetZ")]
        public double StartPlaceOffsetZ { get; set; }

        public double PlaceY { get; set; }

        [Name("magazinePositionLiftSafePosition")]
        public double LiftSafePositionY { get; set; }

        [Name("velocityY")]
        public int VelocityY { get; set; }
        [Name("velocityZ")]
        public int VelocityZ { get; set; }
    }
}