﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UnloadMagazine
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}