﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class LowerConveyorMagazinePosition
    {
        [Name("hasMaterial")]
        public bool HasMaterial { get; set; }
    }
}