﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class MagazineLoaderMaterialTracking
    {
        [Name("lowerConveyorMagazinePosition")]
        public LowerConveyorMagazinePosition LowerConveyorMagazinePosition { get; set;}

        [Name("upperConveyorMagazinePosition")]
        public UpperConveyorMagazinePosition UpperConveyorMagazinePosition { get; set; }

        [Name("liftMagazinePosition")]
        public LiftMagazinePosition LiftMagazinePosition { get; set; }

        [Name("outfeedLeadframePosition")]
        public LeadframeLocation OutfeedLeadframePosition { get; set; }
    }
}