﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class Magazine
    {
        public int ID { get; set; }

        [Name("isEmpty")]
        public bool IsEmpty { get; set; }

        [Name("slotCount")]
        public int SlotCount { get; set; }

        [Name("slot")]
        public Slot Slot { get; set; }
    }
}