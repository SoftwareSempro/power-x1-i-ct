﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class MagazineLoaderModuleInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set;}

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}